package com.carsa.carrito;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.carsa.carrito"} )
public class CarritoWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarritoWsApplication.class, args);
	}

}
