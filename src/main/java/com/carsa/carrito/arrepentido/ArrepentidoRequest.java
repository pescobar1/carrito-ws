package com.carsa.carrito.arrepentido;

public class ArrepentidoRequest {

	private String webmot;
	private String webcom;
	private String webnr;
	private String webtfa;
	private String webtdo;
	private String webpto;
	private String webnro;
	private String webgnr;
	private String webtel;
	private String webndo;
	private String webema;
	/**
	 * @return the webmot
	 */
	public String getWebmot() {
		return webmot;
	}
	/**
	 * @param webmot the webmot to set
	 */
	public void setWebmot(String webmot) {
		this.webmot = webmot;
	}
	/**
	 * @return the webcom
	 */
	public String getWebcom() {
		return webcom;
	}
	/**
	 * @param webcom the webcom to set
	 */
	public void setWebcom(String webcom) {
		this.webcom = webcom;
	}
	/**
	 * @return the webnr
	 */
	public String getWebnr() {
		return webnr;
	}
	/**
	 * @param webnr the webnr to set
	 */
	public void setWebnr(String webnr) {
		this.webnr = webnr;
	}
	/**
	 * @return the webtfa
	 */
	public String getWebtfa() {
		return webtfa;
	}
	/**
	 * @param webtfa the webtfa to set
	 */
	public void setWebtfa(String webtfa) {
		this.webtfa = webtfa;
	}
	/**
	 * @return the webtdo
	 */
	public String getWebtdo() {
		return webtdo;
	}
	/**
	 * @param webtdo the webtdo to set
	 */
	public void setWebtdo(String webtdo) {
		this.webtdo = webtdo;
	}
	/**
	 * @return the webpto
	 */
	public String getWebpto() {
		return webpto;
	}
	/**
	 * @param webpto the webpto to set
	 */
	public void setWebpto(String webpto) {
		this.webpto = webpto;
	}
	/**
	 * @return the webnro
	 */
	public String getWebnro() {
		return webnro;
	}
	/**
	 * @param webnro the webnro to set
	 */
	public void setWebnro(String webnro) {
		this.webnro = webnro;
	}
	/**
	 * @return the webgnr
	 */
	public String getWebgnr() {
		return webgnr;
	}
	/**
	 * @param webgnr the webgnr to set
	 */
	public void setWebgnr(String webgnr) {
		this.webgnr = webgnr;
	}
	/**
	 * @return the webtel
	 */
	public String getWebtel() {
		return webtel;
	}
	/**
	 * @param webtel the webtel to set
	 */
	public void setWebtel(String webtel) {
		this.webtel = webtel;
	}
	/**
	 * @return the webndo
	 */
	public String getWebndo() {
		return webndo;
	}
	/**
	 * @param webndo the webndo to set
	 */
	public void setWebndo(String webndo) {
		this.webndo = webndo;
	}
	/**
	 * @return the webema
	 */
	public String getWebema() {
		return webema;
	}
	/**
	 * @param webema the webema to set
	 */
	public void setWebema(String webema) {
		this.webema = webema;
	}
}
