package com.carsa.carrito.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

/** clase de configuracion necesaria para usar dos bases de datos sql **/

@Configuration
@PropertySource({ "classpath:application-${spring.profiles.active}.yml" })
@EnableJpaRepositories(
    basePackages = "com.carsa.carrito.repository", 
    entityManagerFactoryRef = "mcomEntityManager", 
    transactionManagerRef = "mcomTransactionManager")
public class PersistenceMcomAutoConfiguration {
		
		@Autowired
	    private Environment env;

		@Bean
		@ConfigurationProperties(prefix = "spring.mcom-datasource")
		public DataSource mcomDataSource() {
			return DataSourceBuilder.create().build();
		}
		
		/** Define el paquete donde estan las clases mapeadas de la bd correspondiente */
		
	    @Bean
	    public LocalContainerEntityManagerFactoryBean mcomEntityManager() {
	        LocalContainerEntityManagerFactoryBean em
	          = new LocalContainerEntityManagerFactoryBean();
	        em.setDataSource(mcomDataSource());
	        em.setPackagesToScan(
	          new String[] { "com.carsa.carrito.domain" });

	        HibernateJpaVendorAdapter vendorAdapter
	          = new HibernateJpaVendorAdapter();
	        em.setJpaVendorAdapter(vendorAdapter);

	        HashMap<String, Object> properties = new HashMap<>();
	        properties.put("hibernate.dialect",
	          env.getProperty("spring.hibernate.dialect"));
	        em.setJpaPropertyMap(properties);

	        return em;
	    }

	    @Bean
	    public PlatformTransactionManager mcomTransactionManager() {
	 
	        JpaTransactionManager transactionManager = new JpaTransactionManager();
	        transactionManager.setEntityManagerFactory(
	          mcomEntityManager().getObject());
	        return transactionManager;
	    }	
}
