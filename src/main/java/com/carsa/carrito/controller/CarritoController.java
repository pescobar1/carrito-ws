package com.carsa.carrito.controller;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.carsa.carrito.arrepentido.ArrepentidoDTO;
import com.carsa.carrito.arrepentido.ArrepentidoRequest;
import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.oportunidad.OportunidadCarro;
import com.carsa.carrito.service.CarritoService;
import com.carsa.carrito.service.MailService;
import com.carsa.carrito.service.WebHService;
import com.carsa.carrito.tools.JmsProducer;
import com.carsa.carrito.tools.RequestGenerator;
import com.carsa.carrito.tools.Soporte;
import com.carsa.carrito.ws.CarritoRequest;
import com.carsa.carrito.ws.CarritoWS;
import com.carsa.exceptions.ExceptionHandler;
import com.carsa.ext.service.dto.ClientePublicMCOM;
import com.google.gson.Gson;

@RestController
@RequestMapping("/mcom/")
public class CarritoController {

	@Value("${test}")
	private String test;

	@Autowired
	CarritoWS carritoWs;

	@Autowired
	JmsProducer jmsProducer;

	@Value("${queueCarritos4db}")
	private String carritos4db;

	@Value("${jmsUrl}")
	private String BROKER_URL;

	@Value("${oportunidades.url}")
	private String oportunidadesUrl;
	

	@Autowired
	CarritoService carritoService;

	@Autowired
	WebHService webHService;

	@Autowired
	RabbitTemplate rabbitTemplate;

	@Autowired
	RequestGenerator requestGenerator;
	
	@Autowired
	MailService mailService;

	private static final Logger LOG = LogManager.getLogger(CarritoController.class);
	
	@RequestMapping(value = "carrito", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> generarCarrito(@RequestBody CarritoRequest carrito) throws Exception {
		String carritoJson = new Gson().toJson(carrito);
		LOG.info("Mensaje original: "+ carritoJson);
		try{
			carritoWs.saveCarrito(carrito);
			//Evento asincrono
			carritoService.persistirCarrito(carrito);
			return new ResponseEntity<CarritoRequest> (carrito,  HttpStatus.OK);
		}catch(Exception e){	
			/*
			 * String carritoXml = carritoWs.carritoRequestToXML(carrito);
			 * System.out.println("Voy a enviar a la cola el carrito" + carritoXml);
			 * jmsProducer.sendMessage(carritoXml); return carritoXml;
			 */
			JSONObject exception = new JSONObject();
			exception.put("message", "bad request");
			exception.put("cause", ExceptionHandler.obtenerMensajeError(e));
			
			
			
			return new ResponseEntity<String> (exception.toString(),  HttpStatus.BAD_REQUEST );
		}
	}

	@RequestMapping(value = "publicCarrito/impactar/comprobante/error", method = RequestMethod.GET)
	public String refacturarCarritoError(@RequestParam("nroCarrito") String nroCarrito) throws Exception {
		String result = "";
		if (nroCarrito != null && carritoWs.carritoEnEstado(nroCarrito, "ERROR") == true) {
			WebH carrito = webHService.getByNroCarrito(nroCarrito);
			ClientePublicMCOM cliente = carritoService.crearClienteGenerarXMLSAP(carrito).get();
			if (cliente != null && cliente.getIdClienteLegacy() != null) {
				if (Soporte.esRetiroEnTienda(carrito) == true) {
					carritoWs.actualizarEstadoCarritoPendienteOrdenCrm(nroCarrito);
					String eventoOrdenCrm = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <novFacturaRT><nroCarrito>"
							+ nroCarrito + "</nroCarrito></novFacturaRT>";
					try {
						LOG.info("Voy a enviar a CRM el carrito: " + nroCarrito);
						//jmsProducer.sendMessageOrdenCrm(eventoOrdenCrm);
						//Voy a reemplazar por llamada a funcion asincrona
						carritoService.generarFacturaAutomatica(nroCarrito);
						
						
						result = "Carrito enviado correctamente";
					} catch (Exception e) {
						e.printStackTrace();
						result = e.toString();
					}
				} else {
					LOG.info("El carrito " + nroCarrito
							+ ". no es retiroTienda. No se puede facturar Automaticamente con esta url.");
				}
			} else {
				LOG.info("error consultando cliente para carrito " + nroCarrito
						+ ". No se puede facturar Automaticamente. Solucionar problema de cliente y reimpactar.");

			}
		}
		return result;
	}

	@RequestMapping(value = "/publicCarrito/crear/cliente", method = RequestMethod.GET)
	public String crearClienteCarrito(@RequestParam("nroCarrito") String nroCarrito) throws Exception {
		String result = "";
		if (nroCarrito != null) {
			WebH carrito = webHService.getByNroCarrito(nroCarrito);
			if (carrito != null) {
				if (carrito.getEstado().equals("ERROR_CLIENTE")) {
					ClientePublicMCOM cliente = carritoService.crearClienteGenerarXMLSAP(carrito).get();
					if (cliente != null && cliente.getIdClienteLegacy() != null) {
						if (Soporte.isRetiroTienda(carrito) == true) {
							carritoWs.actualizarEstadoCarritoPendienteOrdenCrm(nroCarrito);

						} else {
							carritoWs.actualizarEstadoCarritoPendiente(nroCarrito);
						}
						result = "creando cliente para carrito" + nroCarrito + ","
								+ (Soporte.esEntregaSap(carrito) == true ? ", retiroTienda->mandando  EntregaSapXML"
										: "");
					} else {
						LOG.info("error al crear cliente para carrito " + nroCarrito
								+ ", verifique los campos del cliente");

					}
				} else {
					LOG.info("estado ERROR_CLIENTE es necesario para carrito " + nroCarrito);
				}
			} else {
				LOG.info("carrito" + nroCarrito + " no encontrado");
			}
		} else {

			LOG.info("carrito para crear cliente no pasado como parametro");
		}
		return result;
	}

	@RequestMapping(value = "/publicCarrito/oportunidad", method = RequestMethod.GET)
	public String verificarOportunidad(@RequestParam("nroCarrito") String nroCarrito) throws Exception {
		String result = "";
		if (nroCarrito != null) {
			WebH carrito = webHService.getByNroCarrito(nroCarrito);
			if (carrito != null) {
				String oportunidadJson = new Gson().toJson(carritoWs.getOportunidadRequest(carrito));
				String res = requestGenerator.post(oportunidadesUrl, "/setSdoOpoWeb", oportunidadJson);
				if (!res.contains("error") || !res.contains("not found")) {
					LOG.info("200 - ok - cliente guardado exitosamente");
					OportunidadCarro oportunidad = new Gson().fromJson(res, OportunidadCarro.class);
					carritoWs.updateOportunidad(carrito, oportunidad);
					result = "verifique oportunidad en la bd. Json de oportunidad";
				}

			} else {
				LOG.info("carrito" + nroCarrito + " no encontrado");
			}
		} else {

			LOG.info("carrito para crear cliente no pasado como parametro");
		}
		return result;
	}
	
	@RequestMapping(value = "/publicCarrito/enviarEntregaSapRT", method = RequestMethod.GET)
	public String regenerarYEnviarEntrega(@RequestParam("nroCarrito") String nroCarrito) throws Exception {
		String result = "";
		if (nroCarrito != null) {
			WebH carrito = webHService.getByNroCarrito(nroCarrito);
			if (carrito != null) {
				if (carrito.getEstado().equals("Facturado")) {
					ClientePublicMCOM cliente = carritoService.chequearCliente(carrito);
					if (cliente != null && cliente.getIdClienteLegacy() != null) {
						carritoService.XmlSAP(carrito, cliente.getIdClienteLegacy());
					} else {
						LOG.info("cliente no valido, no se genero la entregaSap " + nroCarrito);

					}
				} else {
					LOG.info("estado Facturado es necesario para carrito " + nroCarrito);
				}

			} else {
				LOG.info("carrito" + nroCarrito + " no encontrado");
			}
		} else {

			LOG.info("carrito para crear cliente no pasado como parametro");
		}
		return result;
	}
	
	@RequestMapping(value = "carrito/validar", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> validarCarrito(@RequestBody CarritoRequest carrito) throws Exception {
		String carritoJson = new Gson().toJson(carrito);
		try{
			carritoWs.validarCarrito(carrito);
			LOG.info("Validacion correcta carrito "+ carrito.getCarrito().getNumeroCarro());
			return new ResponseEntity<CarritoRequest> (carrito,  HttpStatus.OK);
		}catch(Exception e){	
			JSONObject exception = new JSONObject();
			exception.put("message", "bad request");
			exception.put("cause", ExceptionHandler.obtenerMensajeError(e));
					
			return new ResponseEntity<String> (exception.toString(),  HttpStatus.BAD_REQUEST );
		}
	}
	
	@GetMapping(value = "/desbloquearOrdenCrm/{factura}")
	public ResponseEntity<String> desbloquearOrdenCrm(@PathVariable("factura") String factura) {
		LOG.info("Factura entrante: "+ factura);
		String nroOrdenCrm = carritoWs.obtenerNroOrdenCrmCarrito(factura);

		final String orden = "<actOrden>" + 
        "   <nroOrden>" + nroOrdenCrm +
        "   </nroOrden>" + 
        "   <estado>" +  
        "   </estado>" +
        "   <datosAdic>" +  
        "   </datosAdic>" + 
        "</actOrden>";
		
		jmsProducer.sendMessageDesbloquearOrden(orden);
		
		return new ResponseEntity<String> ("true",  HttpStatus.OK);
	}
	
	@RequestMapping(value = "carrito", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> carritoArrepentido(@RequestBody ArrepentidoRequest carritoArrepentido) throws Exception {
		String carritoJson = new Gson().toJson(carritoArrepentido);
		try{
			LOG.info("Mensaje Arrepentido: " + carritoJson);
			ArrepentidoDTO arrepentidoDto = carritoWs.obtenerArrepentidoDTO(carritoArrepentido);
			
			final String orden = "<actOrden>" + 
			        "   <nroOrden>" + arrepentidoDto.getNroOrden() +
			        "   </nroOrden>" + 
			        "   <estado>" +  arrepentidoDto.getCodigoMotivo() +
			        "   </estado>" +
			        "   <datosAdic>" +  arrepentidoDto.getObservaciones() +
			        "   </datosAdic>" + 
			        "</actOrden>";
					
					jmsProducer.sendMessageDesbloquearOrden(orden);
					
					String content = "<h3>Se registra un Arrepentimiento para</h3><br> Carrito/Factura: " + arrepentidoDto.getNroOrden() + " <br> Dni: " + arrepentidoDto.getDni() + 
							"<br>Email: " + arrepentidoDto.getEmail() + "<br>Motivo: " + arrepentidoDto.getMotivo();					
					mailService.enviarMailGenerico("",content);
					
					JSONObject response = new JSONObject();
					response.put("Status", true);
					response.put("Message", "Ok");
							
					return new ResponseEntity<String> (response.toString(), HttpStatus.OK);
		}catch(Exception e){	
			JSONObject exception = new JSONObject();
			exception.put("message", "bad request");
			exception.put("cause", ExceptionHandler.obtenerMensajeError(e));
					
			return new ResponseEntity<String> (exception.toString(),  HttpStatus.BAD_REQUEST );
		}
	}
	
	

}
