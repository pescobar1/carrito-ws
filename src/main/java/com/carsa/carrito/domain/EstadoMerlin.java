package com.carsa.carrito.domain;

public enum EstadoMerlin {
	SIN_VALIDAR(0),
	AUT_OK(1),
	AUT_ERROR(2),
	MAN_OK(3),
	MAN_ERROR(4),
	RETIRO_TIENDA(5);
	
	private Integer estado;
	
	

	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	
	EstadoMerlin(Integer estado)
	{
		this.estado=estado;
	}
	public static EstadoMerlin getEstadoMerlin(int a)
	{
		switch(a)
		{
			case 0:
				return SIN_VALIDAR;
			case 1:
				return AUT_OK;
			case 2:
				return AUT_ERROR;
			case 3:
				return MAN_OK;
			case 4:
				return MAN_ERROR;
			case 5:
				return RETIRO_TIENDA;
		}
	 
		
		return null;
	}
}
