package com.carsa.carrito.domain;

public enum MerlinNormalizadoMcom {
	NO(0),SI(1),CONTINGENCIA(2);
	
	private Integer estado;
	
	

	public Integer getEstado() {
		return estado;
	}
	public void setEstado(Integer estado) {
		this.estado = estado;
	}
	
	MerlinNormalizadoMcom(Integer estado)
	{
		this.estado=estado;
	}
	public static MerlinNormalizadoMcom getEstadoMerlin(int a)
	{
		switch(a)
		{
			case 0:
				return NO;
			case 1:
				return SI;
			case 2:
				return CONTINGENCIA;
		}	
		return CONTINGENCIA;
	}
}
