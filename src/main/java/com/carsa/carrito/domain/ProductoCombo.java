package com.carsa.carrito.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "PRODUCTO_COMBO")
public class ProductoCombo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID_PRODUCTO_COMBO")
	private Long idProductoCombo;
	
	@Column(name = "CODIGO_COMBO")
	private String codigoCombo;
	
	@Column(name = "DESCRIPCION_COMBO")
	private String descripcionCombo;
	
	@Column(name = "CODIGO_PRODUCTO")
	private String codigoProducto;
	
	@Column(name = "DESCRIPCION_PRODUCTO")
	private String descripcionProducto;
	
	@Column(name = "PRECIO_PRODUCTO")
	private Double precioProducto;
	
	@Column(name = "COSTO_PRODUCTO")
	private Double costoProducto;
	
	@Column(name = "COSTO_COMBO")
	private Double costoCombo;
	
	@Column(name = "PRECIO_COMBO")
	private Double precioCombo;
	
	public String getDescripcionCombo() {
		return descripcionCombo;
	}

	public void setDescripcionCombo(String descripcionCombo) {
		this.descripcionCombo = descripcionCombo;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public Double getPrecioProducto() {
		return precioProducto;
	}

	public void setPrecioProducto(Double precioProducto) {
		this.precioProducto = precioProducto;
	}

	public Double getCostoProducto() {
		return costoProducto;
	}

	public void setCostoProducto(Double costoProducto) {
		this.costoProducto = costoProducto;
	}

	public Double getCostoCombo() {
		return costoCombo;
	}

	public void setCostoCombo(Double costoCombo) {
		this.costoCombo = costoCombo;
	}

	public Double getPrecioCombo() {
		return precioCombo;
	}

	public void setPrecioCombo(Double precioCombo) {
		this.precioCombo = precioCombo;
	}

	public Long getIdProductoCombo() {
		return idProductoCombo;
	}

	public void setIdProductoCombo(Long idProductoCombo) {
		this.idProductoCombo = idProductoCombo;
	}

	public String getCodigoCombo() {
		return codigoCombo;
	}

	public void setCodigoCombo(String codigoCombo) {
		this.codigoCombo = codigoCombo;
	}
	
}
