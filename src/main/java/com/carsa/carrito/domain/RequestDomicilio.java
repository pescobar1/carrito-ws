package com.carsa.carrito.domain;

import com.carsa.domain.merlin.RequestDomicilioNormalizado;

public class RequestDomicilio{
	
	public RequestDomicilioNormalizado RequestDomicilioNormalizado;
	
	public RequestDomicilio() {
		super();
	}
	
	public RequestDomicilio(RequestDomicilioNormalizado requestD) {
		super();
		this.RequestDomicilioNormalizado = requestD;
	}

	public RequestDomicilioNormalizado getRequest() {
		return RequestDomicilioNormalizado;
	}

	public void setRequest(RequestDomicilioNormalizado request) {
		this.RequestDomicilioNormalizado = request;
	}


	
}