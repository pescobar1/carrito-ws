package com.carsa.carrito.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WEBCLI")
public class WebCli implements Serializable {

	private static final long serialVersionUID = 1023974542075004897L;

	@Id
	@Column(name = "WEBNR")
	private String nroCarrito;

	@Column(name = "WEBCDO")
	private String docCod;

	@Column(name = "WEBNDO")
	private Integer nroDocumento;

	@Column(name = "WEBNAC")
	private Date fechaNacimiento;

	@Column(name = "WEBSEX")
	private String sexo;

	@Column(name = "WEBAPE")
	private String apellido;

	@Column(name = "WEBNOM")
	private String nombre;

	@Column(name = "WEBCAR")
	private String calle;

	@Column(name = "WEBNUR")
	private String altura;

	@Column(name = "WEBPIR")
	private String piso;

	@Column(name = "WEBDER")
	private String departamento;

	@Column(name = "WEBBAR")
	private String localidad;

	@Column(name = "WEBPRR")
	private String provincia;

	@Column(name = "WEBCPR")
	private String codPostal;

	@Column(name = "WEBCXR")
	private String CPA;

	@Column(name = "WEBEMR")
	private String mail;

	@Column(name = "WEBTEL")
	private String telefono;

	@Column(name = "WEBCAF")
	private String calleFac;

	@Column(name = "WEBNUF")
	private String alturaFac;

	@Column(name = "WEBTELF")
	private String telefonoFac;

	@Column(name = "WEBPIF")
	private String pisoFac;

	@Column(name = "WEBDEF")
	private String departamentoFac;

	@Column(name = "WEBBAF")
	private String localidadFac;

	@Column(name = "WEBPRF")
	private String provinciaFac;

	@Column(name = "WEBCPF")
	private String codPostalFac;

	@Column(name = "WEBCXF")
	private String CPAFac;

	@Column(name = "FWEBCAF")
	private String calleEnvio;

	@Column(name = "FWEBNUF")
	private String alturaEnvio;

	@Column(name = "FWEBPIF")
	private String pisoEnvio;

	@Column(name = "FWEBDEF")
	private String departamentoEnvio;

	@Column(name = "FWEBBAF")
	private String localidadEnvio;

	@Column(name = "FWEBPRF")
	private String provinciaEnvio;

	@Column(name = "FWEBCPF")
	private String codPostalEnvio;

	@Column(name = "FWEBCXF")
	private String CPAEnvio;

	@Column(name = "LATITUD")
	private String latitud;

	@Column(name = "LONGITUD")
	private String longitud;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "merlin_normalizado")
	private MerlinNormalizadoMcom merlinNormalizado;

	@Column(name = "merlin_id")
	private String merlinId;

	@Column(name = "DOMICILIO_ENVIO_FRAUDULENTO")
	private Integer domicilioEnvioFraudulento;

	@Column(name = "DOMICILIO_FACTURACION_FRAUDULENTO")
	private Integer domicilioFacturacionFraudulento;
	
	/*
	 * @OneToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinTable(name = "WEBH", joinColumns = { @JoinColumn(name = "WEBNR") },
	 * inverseJoinColumns = {@JoinColumn(name = "WEBNR", updatable = false,
	 * insertable = false) }) private Set<WebH> webHs;
	 */

	public WebCli() {
		super();
	}

	public WebCli(String nroCarrito, String calleEnvio, String alturaEnvio, String localidadEnvio,
			String codPostalEnvio) {
		super();
		this.nroCarrito = nroCarrito;
		this.calleEnvio = calleEnvio;
		this.alturaEnvio = alturaEnvio;
		this.localidadEnvio = localidadEnvio;
		this.codPostalEnvio = codPostalEnvio;
	}

	public String getNroCarrito() {
		return nroCarrito;
	}

	public void setNroCarrito(String nroCarrito) {
		this.nroCarrito = nroCarrito;
	}

	public String getDocCod() {
		return docCod;
	}

	public String getTelefonoFac() {
		return telefonoFac;
	}

	public void setTelefonoFac(String telefonoFac) {
		this.telefonoFac = telefonoFac;
	}

	public void setDocCod(String docCod) {
		this.docCod = docCod;
	}

	public Integer getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(Integer nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCodPostal() {
		return codPostal;
	}

	public void setCodPostal(String codPostal) {
		this.codPostal = codPostal;
	}

	public String getCPA() {
		return CPA;
	}

	public void setCPA(String cPA) {
		CPA = cPA;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCalleFac() {
		return calleFac;
	}

	public void setCalleFac(String calleFac) {
		this.calleFac = calleFac;
	}

	public String getAlturaFac() {
		return alturaFac;
	}

	public void setAlturaFac(String alturaFac) {
		this.alturaFac = alturaFac;
	}

	public String getPisoFac() {
		return pisoFac;
	}

	public void setPisoFac(String pisoFac) {
		this.pisoFac = pisoFac;
	}

	public String getDepartamentoFac() {
		return departamentoFac;
	}

	public void setDepartamentoFac(String departamentoFac) {
		this.departamentoFac = departamentoFac;
	}

	public String getLocalidadFac() {
		return localidadFac;
	}

	public void setLocalidadFac(String localidadFac) {
		this.localidadFac = localidadFac;
	}

	public String getProvinciaFac() {
		return provinciaFac;
	}

	public void setProvinciaFac(String provinciaFac) {
		this.provinciaFac = provinciaFac;
	}

	public String getCodPostalFac() {
		return codPostalFac;
	}

	public void setCodPostalFac(String codPostalFac) {
		this.codPostalFac = codPostalFac;
	}

	public String getCPAFac() {
		return CPAFac;
	}

	public void setCPAFac(String cPAFac) {
		CPAFac = cPAFac;
	}

	public String getCalleEnvio() {
		return calleEnvio;
	}

	public void setCalleEnvio(String calleEnvio) {
		this.calleEnvio = calleEnvio;
	}

	public String getAlturaEnvio() {
		return alturaEnvio;
	}

	public void setAlturaEnvio(String alturaEnvio) {
		this.alturaEnvio = alturaEnvio;
	}

	public String getPisoEnvio() {
		return pisoEnvio;
	}

	public void setPisoEnvio(String pisoEnvio) {
		this.pisoEnvio = pisoEnvio;
	}

	public String getDepartamentoEnvio() {
		return departamentoEnvio;
	}

	public void setDepartamentoEnvio(String departamentoEnvio) {
		this.departamentoEnvio = departamentoEnvio;
	}

	public String getLocalidadEnvio() {
		return localidadEnvio;
	}

	public void setLocalidadEnvio(String localidadEnvio) {
		this.localidadEnvio = localidadEnvio;
	}

	public String getProvinciaEnvio() {
		return provinciaEnvio;
	}

	public void setProvinciaEnvio(String provinciaEnvio) {
		this.provinciaEnvio = provinciaEnvio;
	}

	public String getCodPostalEnvio() {
		return codPostalEnvio;
	}

	public void setCodPostalEnvio(String codPostalEnvio) {
		this.codPostalEnvio = codPostalEnvio;
	}

	public String getCPAEnvio() {
		return CPAEnvio;
	}

	public void setCPAEnvio(String cPAEnvio) {
		CPAEnvio = cPAEnvio;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public MerlinNormalizadoMcom getMerlinNormalizado() {
		return merlinNormalizado;
	}

	public void setMerlinNormalizado(MerlinNormalizadoMcom merlinNormalizado) {
		this.merlinNormalizado = merlinNormalizado;
	}

	public String getMerlinId() {
		return merlinId;
	}

	public void setMerlinId(String merlinId) {
		this.merlinId = merlinId;
	}

	public Integer getDomicilioEnvioFraudulento() {
		return domicilioEnvioFraudulento;
	}

	public void setDomicilioEnvioFraudulento(Integer domicilioEnvioFraudulento) {
		this.domicilioEnvioFraudulento = domicilioEnvioFraudulento;
	}

	public Integer getDomicilioFacturacionFraudulento() {
		return domicilioFacturacionFraudulento;
	}

	public void setDomicilioFacturacionFraudulento(Integer domicilioFacturacionFraudulento) {
		this.domicilioFacturacionFraudulento = domicilioFacturacionFraudulento;
	}

}
