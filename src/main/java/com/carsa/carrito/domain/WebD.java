package com.carsa.carrito.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@IdClass(WebDId.class)
@Entity
@Table(name = "WEBD")
public class WebD implements Serializable {

	private static final long serialVersionUID = -2962409051151867200L;

	/*
	 * @EmbeddedId private WebDId webId;
	 */

	//@Column(name = "WEBNR")
	@Id
	private String nroCarrito;

	//@Column(name = "WEBITEM")
	@Id
	private Short nroItem;

	@Column(name = "WEBCPT")
	private String concepto;

	@Column(name = "WEBIMU")
	private String codigoItemEdsa;

	@Column(name = "WEBICA")
	private Integer codigoItemCarsa;

	@Column(name = "WEBIEM")
	private String codigoItemEmsa;

	@Column(name = "WEBQTY")
	private Integer cantidad;

	@Column(name = "WEBPRE")
	private Double precioVenta;

	@Column(name = "WEBTOT")
	private Double importeTotalLinea;

	@Column(name = "WEBDSC")
	private String descripcion;

	@Column(name = "TIPO_ENTREGA")
	private String tipoEntrega;

	@Column(name = "CODIGO_ALMACEN")
	private Integer codigoAlmacen;

	// webscar
	@Column(name = "WEBSCAR")
	private String codigoRetiroCarsa;

	// websem
	@Column(name = "WEBSEM")
	private String codigoRetiroEmsa;

	// sucfcom
	@Column(name = "SUCFCOM")
	private Date fechaEntregaComprometida;

	@Column(name = "FECHA_CONSULTA_OPORTUNIDAD")
	private Date fechaConsultaPrecio;

	@Column(name = "PRECIO_COSTO")
	private Double precioCostoInterno;

	@Column(name = "PRECIO_VENTA")
	private Double precioVentaInterno;

	@Column(name = "ES_OPORTUNIDAD")
	private Boolean esOportunidad;

	@Column(name = "PRODUCTO_COMBO")
	private String codigoProductoCombo;

	/*
	 * public WebH getWebH() { return webH; }
	 * 
	 * public void setWebH(WebH webH) { this.webH = webH; }
	 */

	/*
	 * @ManyToOne
	 * 
	 * @JoinTable(name = "WEBH", joinColumns = { @JoinColumn(name = "WEBNR") },
	 * inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "WEBNR", updatable = false, insertable = false) }) private
	 * WebH webH;
	 */

	public String getTipoEntrega() {
		return tipoEntrega;
	}

	public void setTipoEntrega(String tipoEntrega) {
		this.tipoEntrega = tipoEntrega;
	}

	public Integer getCodigoAlmacen() {
		return codigoAlmacen;
	}

	public void setCodigoAlmacen(Integer codigoAlmacen) {
		this.codigoAlmacen = codigoAlmacen;
	}

	public Short getNroItem() {
		return nroItem;
	}

	public void setNroItem(Short nroItem) {
		this.nroItem = nroItem;
	}

	public String getNroCarrito() {
		return nroCarrito;
	}

	public void setNroCarrito(String nroCarrito) {
		this.nroCarrito = nroCarrito;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getCodigoItemEdsa() {
		return codigoItemEdsa;
	}

	public void setCodigoItemEdsa(String codigoItemEdsa) {
		this.codigoItemEdsa = codigoItemEdsa;
	}

	public Integer getCodigoItemCarsa() {
		return codigoItemCarsa;
	}

	public void setCodigoItemCarsa(Integer codigoItemCarsa) {
		this.codigoItemCarsa = codigoItemCarsa;
	}

	public String getCodigoItemEmsa() {
		return codigoItemEmsa;
	}

	public void setCodigoItemEmsa(String codigoItemEmsa) {
		this.codigoItemEmsa = codigoItemEmsa;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Double getImporteTotalLinea() {
		return importeTotalLinea;
	}

	public void setImporteTotalLinea(Double importeTotalLinea) {
		this.importeTotalLinea = importeTotalLinea;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigoRetiroCarsa() {
		return codigoRetiroCarsa;
	}

	public void setCodigoRetiroCarsa(String codigoRetiroCarsa) {
		this.codigoRetiroCarsa = codigoRetiroCarsa;
	}

	public String getCodigoRetiroEmsa() {
		return codigoRetiroEmsa;
	}

	public void setCodigoRetiroEmsa(String codigoRetiroEmsa) {
		this.codigoRetiroEmsa = codigoRetiroEmsa;
	}

	public Date getFechaEntregaComprometida() {
		return fechaEntregaComprometida;
	}

	public void setFechaEntregaComprometida(Date fechaEntregaComprometida) {
		this.fechaEntregaComprometida = fechaEntregaComprometida;
	}

	public Date getFechaConsultaPrecio() {
		return fechaConsultaPrecio;
	}

	public void setFechaConsultaPrecio(Date fechaConsultaPrecio) {
		this.fechaConsultaPrecio = fechaConsultaPrecio;
	}

	public Double getPrecioCostoInterno() {
		return precioCostoInterno;
	}

	public void setPrecioCostoInterno(Double precioCostoInterno) {
		this.precioCostoInterno = precioCostoInterno;
	}

	public Double getPrecioVentaInterno() {
		return precioVentaInterno;
	}

	public void setPrecioVentaInterno(Double precioVentaInterno) {
		this.precioVentaInterno = precioVentaInterno;
	}

	public Boolean getEsOportunidad() {
		return esOportunidad;
	}

	public void setEsOportunidad(Boolean esOportunidad) {
		this.esOportunidad = esOportunidad;
	}

	@Override
	public WebD clone() throws CloneNotSupportedException {
		WebD webD = new WebD();
		webD.setCantidad(this.cantidad);
		webD.setCodigoItemCarsa(this.codigoItemCarsa);
		webD.setCodigoItemEdsa(this.codigoItemEdsa);
		webD.setCodigoItemEmsa(this.codigoItemEmsa);
		webD.setConcepto(this.concepto);
		webD.setDescripcion(this.descripcion);
		webD.setImporteTotalLinea(this.importeTotalLinea);
		webD.setNroCarrito(this.nroCarrito);
		webD.setNroItem(this.nroItem);
		webD.setPrecioVenta(this.precioVenta);
		webD.setCodigoRetiroCarsa(this.codigoRetiroCarsa);
		webD.setCodigoRetiroEmsa(this.codigoRetiroEmsa);
		webD.setFechaEntregaComprometida(this.fechaEntregaComprometida);

		return webD;
	}

	public String getCodigoProductoCombo() {
		return codigoProductoCombo;
	}

	public void setCodigoProductoCombo(String codigoProductoCombo) {
		this.codigoProductoCombo = codigoProductoCombo;
	}

}
