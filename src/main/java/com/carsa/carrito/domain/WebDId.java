package com.carsa.carrito.domain;

import java.io.Serializable;

import javax.persistence.Column;

//@Embeddable
public class WebDId implements Serializable {

	private static final long serialVersionUID = 8170410767502849129L;

	@Column(name = "WEBNR")
	private String nroCarrito;
	@Column(name = "WEBITEM")
	private Short nroItem;
	
	public WebDId() {

	}
	
	public WebDId(String nroCarrito, Short nroItem) {
		this.nroCarrito = nroCarrito;
		this.nroItem = nroItem;

	}

	public String getNroCarrito() {
		return nroCarrito;
	}

	public void setNroCarrito(String nroCarrito) {
		this.nroCarrito = nroCarrito;
	}

	public Short getNroItem() {
		return nroItem;
	}

	public void setNroItem(Short nroItem) {
		this.nroItem = nroItem;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nroCarrito == null) ? 0 : nroCarrito.hashCode());
		result = prime * result + ((nroItem == null) ? 0 : nroItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final WebDId other = (WebDId) obj;
		if (nroCarrito == null) {
			if (other.nroCarrito != null)
				return false;
		} else if (!nroCarrito.equals(other.nroCarrito))
			return false;
		if (nroItem == null) {
			if (other.nroItem != null)
				return false;
		} else if (!nroItem.equals(other.nroItem))
			return false;
		return true;
	}
}