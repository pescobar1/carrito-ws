package com.carsa.carrito.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "WEBH")
public class WebH implements Serializable {

	private static final long serialVersionUID = -5647264724184649585L;

	public static final String ESTADO_PENDIENTE = "Pendiente";
	public static final String ESTADO_PENDIENTE_ORDEN_CRM = "PendienteOrdenCRM";
	public static final String EN_REVISION = "EnRevision";
	public static final String ESTADO_PENDIENTE_AUTOMATICO = "PendienteAutomatico";
	public static final String ESTADO_FACTURADO = "Facturado";
	public static final String ESTADO_ANULADO = "Anulado";
	public static final String ESTADO_ERROR = "ERROR";
	public static final String ESTADO_ERROR_CLIENTE = "ERROR_CLIENTE";
	public static final String ESTADO_ARREPENTIMIENTO = "ArrepentimientoCompra";

	@Id
	@Column(name = "WEBNR")
	private String nroCarrito;

	@Column(name = "WEBINT")
	private Integer nroInternoCarrito;

	@Column(name = "WEBEMP")
	private String empresa;

	@Column(name = "WEBFEC")
	private Date fechaPedido;

	@Column(name = "WEBHS")
	private Date horaPedido;

	@Column(name = "WEBMDA")
	private String codigoMoneda;

	@Column(name = "ESTADO")
	private String estado;

	@Column(name = "ID_PEDIDO_VENTA")
	private Integer idPedidoVenta;

	@Column(name = "WEBTOT")
	private Double total;

	@Column(name = "WEBPTONRO")
	private String puntoNumeroFacturacion;

	@Column(name = "WEBFACFEC")
	private Date fechaFacturacion;

	@Column(name = "WEBIMPFEC")
	private Date fechaImpacto;

	// VerEst: Define el estado de la transacci�n ante la validaci�n de Veraz
	@Column(name = "VEREST")
	private String verazEstado;

	// VerSco: Define el valor de score que retorno la validaci�n de Veraz
	@Column(name = "VERSCO")
	private String verazScore;

	// WebNrE: Define el n�mero de Identificador �nico en el sistema de Eventos
	@Column(name = "WEBNRE")
	private String nroEventoEspecial;

	@Column(name = "NRO_ORDEN_CRM")
	private String nroOrdenCrm;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ESTADO_MERLIN")
	private EstadoMerlin estadoMerlin;

	@Column(name = "PROVIDER_HIBRIS")
	private Boolean providerHybris;

	@Column(name = "VENTA_VERDE")
	private Boolean ventaVerde = false;

	@OneToMany(cascade = CascadeType.ALL)
	@OrderBy("WEBITEM asc")
	@JoinColumn(name = "WEBNR", updatable = false, insertable = false)
	private Set<WebD> detalles;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("WEBNR asc")
	@JoinColumn(name = "WEBNR", updatable = false, insertable = false)
	private Set<WebP> pagos;

//	@JoinTable(name = "WEBCLI", joinColumns = { @JoinColumn(name = "WEBNR") },
//	  inverseJoinColumns = {@JoinColumn(name = "WEBNR", updatable=false,
//	  insertable=false)})

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "WEBNR", updatable = false, insertable = false)
	private WebCli webCli;
//	@JoinTable(name = "WEBNPS", joinColumns = { @JoinColumn(name = "WEBNR") },
//			  inverseJoinColumns = {@JoinColumn(name = "WEBNR", updatable=false,
//			  insertable=false)})

	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "WEBNR", nullable=true, updatable = false, insertable = false)
	private WebNps webNps;

	public String getPuntoNumeroFacturacion() {
		return puntoNumeroFacturacion;
	}

	public void setPuntoNumeroFacturacion(String puntoNumeroFacturacion) {
		this.puntoNumeroFacturacion = puntoNumeroFacturacion;
	}

	public Set<WebD> getDetalles() {
		return detalles;
	}

	public void setDetalles(Set<WebD> detalles) {
		this.detalles = detalles;
	}

	public Set<WebP> getPagos() {
		return pagos;
	}

	public void setPagos(Set<WebP> pagos) {
		this.pagos = pagos;
	}

	public WebCli getWebCli() {
		return webCli;
	}

	public void setWebCli(WebCli webCli) {
		this.webCli = webCli;
	}

	public WebNps getWebNps() {
		return webNps;
	}

	public void setWebNps(WebNps webNps) {
		this.webNps = webNps;
	}

	public String getNroCarrito() {
		return nroCarrito;
	}

	public void setNroCarrito(String nroCarrito) {
		this.nroCarrito = nroCarrito;
	}

	public Integer getNroInternoCarrito() {
		return nroInternoCarrito;
	}

	public void setNroInternoCarrito(Integer nroInternoCarrito) {
		this.nroInternoCarrito = nroInternoCarrito;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Date getFechaPedido() {
		return fechaPedido;
	}

	public void setFechaPedido(Date fechaPedido) {
		this.fechaPedido = fechaPedido;
	}

	public Date getHoraPedido() {
		return horaPedido;
	}

	public void setHoraPedido(Date horaPedido) {
		this.horaPedido = horaPedido;
	}

	public String getCodigoMoneda() {
		return codigoMoneda;
	}

	public void setCodigoMoneda(String codigoMoneda) {
		this.codigoMoneda = codigoMoneda;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Integer getIdPedidoVenta() {
		return idPedidoVenta;
	}

	public void setIdPedidoVenta(Integer idPedidoVenta) {
		this.idPedidoVenta = idPedidoVenta;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaFacturacion() {
		return fechaFacturacion;
	}

	public void setFechaFacturacion(Date fechaFacturacion) {
		this.fechaFacturacion = fechaFacturacion;
	}

	public Date getFechaImpacto() {
		return fechaImpacto;
	}

	public void setFechaImpacto(Date fechaImpacto) {
		this.fechaImpacto = fechaImpacto;
	}

	public String getVerazEstado() {
		return verazEstado;
	}

	public void setVerazEstado(String verazEstado) {
		this.verazEstado = verazEstado;
	}

	public String getVerazScore() {
		return verazScore;
	}

	public void setVerazScore(String verazScore) {
		this.verazScore = verazScore;
	}

	public String getNroEventoEspecial() {
		return nroEventoEspecial;
	}

	public void setNroEventoEspecial(String nroEventoEspecial) {
		this.nroEventoEspecial = nroEventoEspecial;
	}

	public EstadoMerlin getEstadoMerlin() {
		return estadoMerlin;
	}

	public void setEstadoMerlin(EstadoMerlin estadoMerlin) {
		this.estadoMerlin = estadoMerlin;
	}

	public Boolean pagosAprobados() {
		Iterator<WebP> iter = this.getPagos().iterator();
		while (iter.hasNext()) {
			WebP webP = iter.next();
			if (!WebP.ESTADO_APROBADO.equals(webP.getEstado())) {
				return false;
			}
		}
		return true;
	}

	public Boolean esRetiroEnTienda() {
		Iterator<WebD> iter = this.getDetalles().iterator();
		while (iter.hasNext()) {
			WebD webD = iter.next();
			if (webD.getCodigoRetiroCarsa() != null && !webD.getCodigoRetiroCarsa().trim().isEmpty())
				return true;
		}
		return false;
	}

	public String getNroOrdenCrm() {
		return nroOrdenCrm;
	}

	public void setNroOrdenCrm(String nroOrdenCrm) {
		this.nroOrdenCrm = nroOrdenCrm;
	}

	public Boolean getProviderHybris() {
		return providerHybris;
	}

	public void setProviderHybris(Boolean providerHybris) {
		this.providerHybris = providerHybris;
	}

	public Boolean getVentaVerde() {
		return ventaVerde;
	}

	public void setVentaVerde(Boolean ventaVerde) {
		this.ventaVerde = ventaVerde;
	}

}
