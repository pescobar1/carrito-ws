package com.carsa.carrito.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WEBNPS")
public class WebNps implements Serializable {

	private static final long serialVersionUID = -378513887914871647L;

	@Id
	@Column(name = "WEBNR")
	private String nroCarrito;

	@Column(name = "WEBCDO")
	private String docCod;

	@Column(name = "WEBNDO")
	private String nroDocumento;

	@Column(name = "WEBNAC")
	private Date fechaNacimiento;

	@Column(name = "WEBCAR")
	private String calle;

	@Column(name = "WEBNUR")
	private String altura;

	@Column(name = "WEBVALD")
	private String validacionDireccion;

	@Column(name = "WEBVALTD")
	private String validacionTipoDocumento;

	@Column(name = "WEBVALND")
	private String validacionNumeroDocumento;

	@Column(name = "WEBVALFN")
	private String validacionFechaNacimiento;
	
	/*
	 * @OneToMany(cascade = CascadeType.ALL)
	 * 
	 * @JoinTable(name = "WEBH", joinColumns = { @JoinColumn(name = "WEBNR") },
	 * inverseJoinColumns = {@JoinColumn(name = "WEBNR", updatable = false,
	 * insertable = false) }) private Set<WebH> webHs;
	 */

	public String getNroCarrito() {
		return nroCarrito;
	}

	public void setNroCarrito(String nroCarrito) {
		this.nroCarrito = nroCarrito;
	}

	public String getDocCod() {
		return docCod;
	}

	public void setDocCod(String docCod) {
		this.docCod = docCod;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getValidacionDireccion() {
		return validacionDireccion;
	}

	public void setValidacionDireccion(String validacionDireccion) {
		this.validacionDireccion = validacionDireccion;
	}

	public String getValidacionTipoDocumento() {
		return validacionTipoDocumento;
	}

	public void setValidacionTipoDocumento(String validacionTipoDocumento) {
		this.validacionTipoDocumento = validacionTipoDocumento;
	}

	public String getValidacionNumeroDocumento() {
		return validacionNumeroDocumento;
	}

	public void setValidacionNumeroDocumento(String validacionNumeroDocumento) {
		this.validacionNumeroDocumento = validacionNumeroDocumento;
	}

	public String getValidacionFechaNacimiento() {
		return validacionFechaNacimiento;
	}

	public void setValidacionFechaNacimiento(String validacionFechaNacimiento) {
		this.validacionFechaNacimiento = validacionFechaNacimiento;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

}
