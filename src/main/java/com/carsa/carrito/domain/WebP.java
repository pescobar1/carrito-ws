package com.carsa.carrito.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WEBP")
public class WebP implements Serializable{

	private static final long serialVersionUID = 6931993802552526692L;
	
	public static final String ESTADO_PENDIENTE = "Pendiente";
	public static final String ESTADO_APROBADO = "Aprobado";
	public static final String ESTADO_CANCELADO = "Cancelado";
	
	@Id
	@Column(name = "WEBNR")
	private String nroCarrito;
	
	@Column(name = "WEBPRO")
	private String producto;
	
	@Column(name = "WEBTC")
	private String codigoTarjeta;
	
	@Column(name = "WEBCUO")
	private Double cantCuotas;
	
	@Column(name = "WEBMDA")
	private String codMoneda;
	
	@Column(name = "WEBTOT")
	private Double importeCupon;
	
	@Column(name = "WEBCUP")
	private String voucherId;
	
	@Column(name = "WEBNTC")
	private String nroTarjeta;
	
	@Column(name = "WEBAUT")
	private String codAutorizacion;
	
	@Column(name = "WEBMID")
	private String merchantId;
	
	@Column(name = "WEBLOT")
	private String nroLote;
	
	@Column(name = "WEBPR")
	private String promocion;
	
	@Column(name = "WEBDTO")
	private String descuentoProm;
	
	//WebEst: Define el estado del pago de la venta informada a la empresa
	@Column(name = "WEBEST")
	private String estado;
	
	@Column(name = "ESTADO_NPS")
	private String npsstatus;
		
	//WebApeT: Apellido del titular
	@Column(name = "WEBAPET")
	private String apellidoTitular;
	
    //WebNomT: Nombre del titular
	@Column(name = "WEBNOMT")
	private String nombreTitular;
	
    //WebNdoT: numero de documento del titular
	@Column(name = "WEBNDOT")
	private String numeroDocumentoTitular;
	
	@Column(name = "WEBTDOT")
	private String tipoDocumentoTitular;
	
	/*
	 * @ManyToOne
	 * 
	 * @JoinTable(name = "WEBH", joinColumns = { @JoinColumn(name = "WEBNR") },
	 * inverseJoinColumns = {
	 * 
	 * @JoinColumn(name = "WEBNR", updatable = false, insertable = false) }) private
	 * WebH webH;
	 */
	
	public String getNroCarrito() {
		return nroCarrito;
	}

	public void setNroCarrito(String nroCarrito) {
		this.nroCarrito = nroCarrito;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getCodigoTarjeta() {
		return codigoTarjeta;
	}

	public void setCodigoTarjeta(String codigoTarjeta) {
		this.codigoTarjeta = codigoTarjeta;
	}

	public Double getCantCuotas() {
		return cantCuotas;
	}

	public void setCantCuotas(Double cantCuotas) {
		this.cantCuotas = cantCuotas;
	}

	public String getCodMoneda() {
		return codMoneda;
	}

	public void setCodMoneda(String codMoneda) {
		this.codMoneda = codMoneda;
	}

	public Double getImporteCupon() {
		return importeCupon;
	}

	public void setImporteCupon(Double importeCupon) {
		this.importeCupon = importeCupon;
	}

	public String getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}

	public String getNroTarjeta() {
		return nroTarjeta;
	}

	public void setNroTarjeta(String nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}

	public String getCodAutorizacion() {
		return codAutorizacion;
	}

	public void setCodAutorizacion(String codAutorizacion) {
		this.codAutorizacion = codAutorizacion;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getNroLote() {
		return nroLote;
	}

	public void setNroLote(String nroLote) {
		this.nroLote = nroLote;
	}

	public String getPromocion() {
		return promocion;
	}

	public void setPromocion(String promocion) {
		this.promocion = promocion;
	}

	public String getDescuentoProm() {
		return descuentoProm;
	}

	public void setDescuentoProm(String descuentoProm) {
		this.descuentoProm = descuentoProm;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getApellidoTitular() {
		return apellidoTitular;
	}

	public void setApellidoTitular(String apellidoTitular) {
		this.apellidoTitular = apellidoTitular;
	}

	public String getNombreTitular() {
		return nombreTitular;
	}

	public void setNombreTitular(String nombreTitular) {
		this.nombreTitular = nombreTitular;
	}

	public String getNumeroDocumentoTitular() {
		return numeroDocumentoTitular;
	}

	public void setNumeroDocumentoTitular(String numeroDocumentoTitular) {
		this.numeroDocumentoTitular = numeroDocumentoTitular;
	}

	public String getTipoDocumentoTitular() {
		return tipoDocumentoTitular;
	}

	public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
		this.tipoDocumentoTitular = tipoDocumentoTitular;
	}

	public String getNpsstatus() {
		return npsstatus;
	}

	public void setNpsstatus(String npsstatus) {
		this.npsstatus = npsstatus;
	}
}
