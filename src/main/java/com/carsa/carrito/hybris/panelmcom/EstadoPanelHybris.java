package com.carsa.carrito.hybris.panelmcom;

public class EstadoPanelHybris {
	public static final String EN_PROCESO = "INPROGRESS";
	public static final String EN_REVISION = "CARRITO_EN_REVISION";
	public static final String FACTURADO = "FACTURADO";
	public static final String RETIRO_EN_SUCURSAL_FACTURADO = "RET_EN_SUC_FACTURADO";
	public static final String REFACTURAR = "ON_VALIDATION";
	public static final String PENDIENTE_AUTOMATICO = "PENDIENTE_AUTOMATICO";

	private String numeroCarro;
	private String estado;

	
	
	public EstadoPanelHybris() {
		super();
	}

	public String getNumeroCarro() {
		return numeroCarro;
	}

	public void setNumeroCarro(String numeroCarro) {
		this.numeroCarro = numeroCarro;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "EstadoPanelHybris [numeroCarro=" + numeroCarro + ", estado=" + estado + "]";
	}

	
	
}