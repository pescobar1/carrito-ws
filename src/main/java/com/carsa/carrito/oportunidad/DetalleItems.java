package com.carsa.carrito.oportunidad;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DetalleItems {
	
	@JsonIgnore
	public DateFormat timestampFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
	
	private Integer articulo;
	private Integer cantidad;
	private Double costo;
	private Double precioVta;
	private Boolean esOpoWeb=false;
	private String timestamp;
	
    public Date getTimestampDate() {
		Date result=null;
		if(timestamp!=null && !timestamp.isEmpty())
		try {
			result = timestampFormat.parse(this.timestamp);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}
    
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp){
		this.timestamp = timestamp;
	}
	
	public Boolean getEsOpoWeb() {
		return esOpoWeb;
	}
	public void setEsOpoWeb(Boolean esOpoWeb) {
		this.esOpoWeb = esOpoWeb;
	}
	public Integer getArticulo() {
		return articulo;
	}
	public void setArticulo(Integer articulo) {
		this.articulo = articulo;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Double getCosto() {
		return costo;
	}
	public void setCosto(Double costo) {
		this.costo = costo;
	}
	public Double getPrecioVta() {
		return precioVta;
	}
	public void setPrecioVta(Double precioVta) {
		this.precioVta = precioVta;
	}
}
