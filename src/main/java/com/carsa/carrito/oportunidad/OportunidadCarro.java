package com.carsa.carrito.oportunidad;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class OportunidadCarro {
	@JsonIgnore
	public static final String OPERACION_MENOS = "-";
	@JsonIgnore
	public static final String OPERACION_MAS = "+";
	
	@JsonIgnore
	public DateFormat fechaFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	private String numeroCarro;
	private String fecha;
	private String operacion;
    private List<DetalleItems> detalleItems = new ArrayList<>();

	public String getNumeroCarro() {
		return numeroCarro;
	}

	public void setNumeroCarro(String numeroCarro) {
		this.numeroCarro = numeroCarro;
	}

	public String getFecha() {
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public void setFechaDate(Date fecha) {
		this.fecha = fechaFormat.format(fecha);
	}
	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public List<DetalleItems> getDetalleItems() {
		return detalleItems;
	}

	public void setDetalleItems(List<DetalleItems> detalleItems) {
		this.detalleItems = detalleItems;
	}

	

}
