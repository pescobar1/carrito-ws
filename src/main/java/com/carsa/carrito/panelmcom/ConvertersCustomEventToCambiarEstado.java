package com.carsa.carrito.panelmcom;

import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.TransformerException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.transformer.AbstractTransformer;
import org.springframework.messaging.Message;

import com.carsa.carrito.panelmcom.ws.cambiarEstado.org.tempuri.CambiarEstado;
import com.carsa.carrito.panelmcom.ws.cambiarEstado.org.tempuri.ObjectFactory;
import com.carsa.carrito.service.WebHService;
import com.carsa.customer.tracer.dto.BusinessEventType;
import com.carsa.customer.tracer.dto.CustomerEventDto;

public class ConvertersCustomEventToCambiarEstado{



	private static final Logger LOG = LogManager.getLogger(ConvertersCustomEventToCambiarEstado.class);
	
	@Autowired
	private WebHService webHService;
	
	public CambiarEstado transform(CustomerEventDto customerEvent) {
		
		CambiarEstado cambiarEstado=null;
		try {
			return createCambiarEstado(customerEvent);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return cambiarEstado;
		
	}
	
	
	private CambiarEstado createCambiarEstado(CustomerEventDto customerEventDto) throws TransformerException {
		CambiarEstado cambioEstado=new CambiarEstado();
		ObjectFactory of=new ObjectFactory();
		cambioEstado.setCodigoEmpresa(of.createCambiarEstadoCodigoEmpresa("CARSA"));
		cambioEstado.setPlataformaOrigen(of.createCambiarEstadoPlataformaOrigen("G"));
		//fecha
		GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(customerEventDto.getEventDate());
		XMLGregorianCalendar novedad;
		try {
			novedad = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		} catch (DatatypeConfigurationException e) {
			LOG.error("Error conviertiendo fecha cambioEstado mcom "+customerEventDto.getReferenceIdentifierValue());
			throw new TransformerException(new Exception(
					new StringBuilder("error en la conversion de fecha ").append(customerEventDto.getEventType()).append("nro Carro ").append(customerEventDto.getReferenceIdentifierValue()).toString()));
		}
		cambioEstado.setNovedad(novedad);
		Integer nroCarro=null;
		
		//convierte un evento ingreso carrito
		if(BusinessEventType.INGRESO_CARRITO.equals(customerEventDto.getEventType())){
			//nroCarro
			try{
				if (customerEventDto.getIdentifierValue().startsWith("C")) {
					nroCarro = Integer.valueOf(customerEventDto.getIdentifierValue().substring(1));
					Boolean isPendiente_Automatico = this.webHService.isPendienteOrdenCrm(customerEventDto.getIdentifierValue());
					// codeStatus
					if (isPendiente_Automatico) 
					{
						cambioEstado.setInvoiceId(nroCarro.longValue());
						cambioEstado.setCodeStatus(of.createCambiarEstadoCodeStatus(EstadoPanel.PENDIENTE_AUTOMATICO));
					} else {
						cambioEstado = null;
					}
				} else {
					cambioEstado = null;
					LOG.info("cambioEstadoPanelMcom INGRESO_CARRITO, carrito"+ customerEventDto.getIdentifierValue()+ "not mcom ndw");
				}
			}catch(NumberFormatException numberFormat){
				LOG.info("cambioEstadoPanelMcom INGRESO_CARRITO, carrito"+customerEventDto.getIdentifierValue()+"not mcom ndw");
				return null;
			}
			return cambioEstado;
		}
		
		//convierte un evento facturado
		if(BusinessEventType.CARRITO_FACTURADO.equals(customerEventDto.getEventType())){
			//nroCarro
			try{
				if (customerEventDto.getReferenceIdentifierValue().startsWith("C")) {
					nroCarro = Integer.valueOf(customerEventDto.getReferenceIdentifierValue().substring(1));
					cambioEstado.setInvoiceId(nroCarro.longValue());
					Boolean isRetiroTienda = this.webHService.getByNroCarrito(customerEventDto.getReferenceIdentifierValue()).esRetiroEnTienda();
					// codeStatus
					if (isRetiroTienda) {
						cambioEstado.setCodeStatus(of.createCambiarEstadoCodeStatus(EstadoPanel.RETIRO_EN_SUCURSAL_FACTURADO));
					} else {
						cambioEstado.setCodeStatus(of.createCambiarEstadoCodeStatus(EstadoPanel.FACTURADO));
					}
				} else {
					cambioEstado = null;
					LOG.info("cambioEstadoPanelMcom CARRITO_FACTURADO, carrito nro: "
							+ customerEventDto.getReferenceIdentifierValue()
							+ "not mcom ndw");
				}
			}catch(NumberFormatException numberFormat){
				LOG.info("cambioEstadoPanelMcom CARRITO_FACTURADO, carrito nro: "+customerEventDto.getReferenceIdentifierValue()+"not mcom ndw");
				return null;
			}
			
			return cambioEstado;
		}

		//en caso de no cumplir con ningun evento retorno null
		return null;
	}
	

	
}