package com.carsa.carrito.panelmcom;

import java.util.Iterator;
import java.util.List;

import javax.xml.transform.TransformerException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.transformer.AbstractTransformer;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import com.carsa.carrito.domain.WebD;
import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.hybris.panelmcom.EstadoPanelHybris;
import com.carsa.carrito.service.WebHService;
import com.carsa.customer.tracer.dto.BusinessEventType;
import com.carsa.customer.tracer.dto.CustomerEventDto;

//@Service
public class ConvertersCustomEventToEstadoHybris {

	@Autowired
	private WebHService webHService;

	private static final Logger LOG = LogManager.getLogger(ConvertersCustomEventToEstadoHybris.class);
	
	@Value("#{'${venta.consignacion.rango.producto}'.split(',')}")
	private List<Integer> rangoConsignacion;
	
	public EstadoPanelHybris transform(CustomerEventDto customerEventDto){
		try {
			return createCambiarEstado(customerEventDto);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return null;
	}

	private EstadoPanelHybris createCambiarEstado(CustomerEventDto customerEventDto) throws TransformerException {
		EstadoPanelHybris resultEstado = null;
		if (BusinessEventType.INGRESO_CARRITO.equals(customerEventDto.getEventType())) {
			String numeroCarro = customerEventDto.getIdentifierValue();
			Boolean isPendiente_Automatico = this.webHService.isPendienteOrdenCrm(numeroCarro);
			List<WebH> carros = this.webHService.findByNumeroCarro(numeroCarro);
			
			// codeStatus
			if (isPendiente_Automatico) {
				resultEstado = new EstadoPanelHybris();
				resultEstado.setEstado(EstadoPanelHybris.PENDIENTE_AUTOMATICO);
				resultEstado.setNumeroCarro(numeroCarro);
				return resultEstado;
			}else{
				if (carros != null && carros.size()>0){
					for(WebH carro:carros){
						if (carro.getNroCarrito().equals(numeroCarro)){
							if(esConsignacion(carro)){
								resultEstado = new EstadoPanelHybris();
								resultEstado.setEstado(EstadoPanelHybris.EN_REVISION);
								resultEstado.setNumeroCarro(numeroCarro);
							}
							
						}
					}
				}
				
				return resultEstado;
			}
		}
		if (BusinessEventType.CARRITO_FACTURADO.equals(customerEventDto.getEventType())) {
			String numeroCarro = customerEventDto.getReferenceIdentifierValue();
			Boolean isRetiroTienda = this.webHService.getByNroCarrito(numeroCarro).esRetiroEnTienda();
			resultEstado = new EstadoPanelHybris();
			resultEstado.setNumeroCarro(numeroCarro);
			if (isRetiroTienda) {
				resultEstado.setEstado(EstadoPanelHybris.RETIRO_EN_SUCURSAL_FACTURADO);
				return resultEstado;
			} else {
				resultEstado.setEstado(EstadoPanelHybris.FACTURADO);
				return resultEstado;
			}
		}

		// en caso de no cumplir con ningun evento retorno null
		return null;
	}
	
	private Boolean esConsignacion( WebH webH){
		Iterator<WebD> iter = webH.getDetalles().iterator();
		while (iter.hasNext()) {	
			WebD webD=iter.next();
			if (webD.getCodigoItemCarsa() != null && webD.getCodigoItemCarsa()>0)
			{
				try{
				int codigoProducto=Integer.valueOf(webD.getCodigoItemCarsa());
				if(codigoProducto>=this.rangoConsignacion.get(0) 
						&& codigoProducto<=this.rangoConsignacion.get(1))
				{
					return true;
				}else{
					return false;
				}
				}catch(NumberFormatException exception){
					return false;
				}
			}
		}
		return false;
	}


}