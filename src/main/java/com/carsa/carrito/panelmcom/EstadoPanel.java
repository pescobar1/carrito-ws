package com.carsa.carrito.panelmcom;

public class EstadoPanel {
	 public static final String EN_PROCESO = "00102";
	 public static final String FACTURADO =  "00103";
	 public static final String RETIRO_EN_SUCURSAL_FACTURADO = "00113";
	 public static final String REFACTURAR = "00104";
	 public static final String PENDIENTE_AUTOMATICO = "00126";
}