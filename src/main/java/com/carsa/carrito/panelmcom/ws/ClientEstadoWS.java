package com.carsa.carrito.panelmcom.ws;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.xml.transform.StringResult;
import org.springframework.ws.soap.SoapMessage;

import com.carsa.carrito.panelmcom.ws.cambiarEstado.org.tempuri.CambiarEstado;
import com.carsa.carrito.panelmcom.ws.cambiarEstado.org.tempuri.CambiarEstadoResponse;

@Service(value = "clienteEstadoWS")
public class ClientEstadoWS {

	
	private WebServiceTemplate webServiceTemplateEntrega;
	@Value("${ws.mcom.defaultUri}")
	private String defaultUri;
	@Value("${ws.mcom.defaultUriCM}")
	private String defaultUriCM;
	@Value("${ws.mcom.urlActionSetEstado}")
	private String urlActionSetEstado;

	public CambiarEstadoResponse cambioEstado(CambiarEstado cambiarEstado) throws Exception {
		CambiarEstadoResponse response = null;
		String urlCambioEstadoWs = defaultUri;
		// para carros mayores a 10000000 es de credito musimundo
		if (cambiarEstado.getInvoiceId() > 10000000) {
			urlCambioEstadoWs = defaultUriCM;
		}
		try {
			response = (CambiarEstadoResponse) webServiceTemplateEntrega.marshalSendAndReceive(urlCambioEstadoWs,
					cambiarEstado, new WebServiceMessageCallback() {
						public void doWithMessage(WebServiceMessage message) {
							((SoapMessage) message).setSoapAction(urlActionSetEstado);
							StringResult result = new StringResult();
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return response;
	}

	public WebServiceTemplate getWebServiceTemplateEntrega() {
		return webServiceTemplateEntrega;
	}

	public void setWebServiceTemplateEntrega(WebServiceTemplate webServiceTemplateEntrega) {
		this.webServiceTemplateEntrega = webServiceTemplateEntrega;
	}

	public String getUrlActionSetEstado() {
		return urlActionSetEstado;
	}

	public String getDefaultUri() {
		return defaultUri;
	}

	public void setDefaultUri(String defaultUri) {
		this.defaultUri = defaultUri;
	}

	public void setUrlActionSetEstado(String urlActionSetEstado) {
		this.urlActionSetEstado = urlActionSetEstado;
	}

}