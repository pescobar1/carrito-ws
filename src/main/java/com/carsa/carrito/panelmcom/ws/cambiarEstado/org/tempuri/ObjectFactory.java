
package com.carsa.carrito.panelmcom.ws.cambiarEstado.org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CambiarEstadoPlataformaOrigen_QNAME = new QName("http://tempuri.org/", "plataformaOrigen");
    private final static QName _CambiarEstadoCodigoEmpresa_QNAME = new QName("http://tempuri.org/", "codigoEmpresa");
    private final static QName _CambiarEstadoCodeStatus_QNAME = new QName("http://tempuri.org/", "codeStatus");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CambiarEstado }
     * 
     */
    public CambiarEstado createCambiarEstado() {
        return new CambiarEstado();
    }

    /**
     * Create an instance of {@link CambiarEstadoResponse }
     * 
     */
    public CambiarEstadoResponse createCambiarEstadoResponse() {
        return new CambiarEstadoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "plataformaOrigen", scope = CambiarEstado.class)
    public JAXBElement<String> createCambiarEstadoPlataformaOrigen(String value) {
        return new JAXBElement<String>(_CambiarEstadoPlataformaOrigen_QNAME, String.class, CambiarEstado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "codigoEmpresa", scope = CambiarEstado.class)
    public JAXBElement<String> createCambiarEstadoCodigoEmpresa(String value) {
        return new JAXBElement<String>(_CambiarEstadoCodigoEmpresa_QNAME, String.class, CambiarEstado.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "codeStatus", scope = CambiarEstado.class)
    public JAXBElement<String> createCambiarEstadoCodeStatus(String value) {
        return new JAXBElement<String>(_CambiarEstadoCodeStatus_QNAME, String.class, CambiarEstado.class, value);
    }

}
