package com.carsa.carrito.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.carsa.carrito.domain.WebCli;
import com.carsa.carrito.domain.WebH;
@Repository
public interface WebHRepository extends JpaRepository<WebH, String> {

	WebH findByNroCarrito(String nroCarrito);

	// WebH findByNroPedidoAndEstadoNoFacturado(Integer nroPedido);

	//List<WebH> findAll(String nroCarrito, Integer nroPagina, Integer size);
	
	Page <WebH> findAll(Pageable pageable);
	
	@Query(value="SELECT new com.carsa.carrito.domain.WebCli(wh.webCli.nroCarrito, wh.webCli.calleEnvio, wh.webCli.alturaEnvio,wh.webCli.localidadEnvio, wh.webCli.codPostalEnvio) FROM WebH as wh join wh.pagos as wp WHERE wh.estado='Pendiente' AND wh.estadoMerlin=0 AND wp.estado='Aprobado' order by wh.nroCarrito desc")
	List<WebCli> findCarritosSinProcesarMerlin();

	//Boolean actualizarEstadoMerlin(String nroCarrito, EstadoMerlin estado);

	//WebH findCarritoByNroFactura(String puntoNro);

	//Boolean isPendienteOrdenCrm(String nroCarrito);

	// List<WebH> findByNumeroCarro(String nroCarrito);

	//Boolean actualizarEstadoCarrito(String nroCarrito, String estado);

	//Boolean actualizarNroOrdenCrmCarrito(String nroCarrito, String nroOrdenCrm);

	//Boolean actualizarCarritoRt(String nroCarrito, String nroOrdenCrm, String estado);

	//List<WebH> findCarritosByCliente(Integer dni);

	WebH findByIdPedidoVentaAndEstado(Integer nroPedido, String estado);
	
	List<WebH> findByNroCarritoAndEstado(String nroCarrito,String estado);
	
	List<WebH> findByWebCli_NroDocumento(Integer dni);
	
	WebH findByPuntoNumeroFacturacionAndPuntoNumeroFacturacionIsNotNull(String puntoNro);
	
}
