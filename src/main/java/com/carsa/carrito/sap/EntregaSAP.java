package com.carsa.carrito.sap;


import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
 

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
 "cabeceraEntregaSAP",
 "conceptosFacturados",
 "detallesEntregas"
})
@XmlRootElement(name = "entrega")
public class EntregaSAP {

 @XmlElement(required = true, name="cabeceraEntrega")
 protected EntregaSAP.CabeceraEntregaSAP cabeceraEntregaSAP;
 protected EntregaSAP.ConceptosFacturados conceptosFacturados;
 @XmlElement(required = true, name="detallesEntregas")
 protected EntregaSAP.Entregas detallesEntregas;

 /**
  * Gets the value of the cabeceraEntregaSAP property.
  * 
  * @return
  *     possible object is
  *     {@link EntregaSAP.CabeceraEntregaSAP }
  *     
  */
 public EntregaSAP.CabeceraEntregaSAP getCabeceraEntregaSAP() {
     return cabeceraEntregaSAP;
 }

 /**
  * Sets the value of the cabeceraEntregaSAP property.
  * 
  * @param value
  *     allowed object is
  *     {@link EntregaSAP.CabeceraEntregaSAP }
  *     
  */
 public void setCabeceraEntregaSAP(EntregaSAP.CabeceraEntregaSAP value) {
     this.cabeceraEntregaSAP = value;
 }

 /**
  * Gets the value of the conceptosFacturados property.
  * 
  * @return
  *     possible object is
  *     {@link EntregaSAP.ConceptosFacturados }
  *     
  */
 public EntregaSAP.ConceptosFacturados getConceptosFacturados() {
     return conceptosFacturados;
 }

 /**
  * Sets the value of the conceptosFacturados property.
  * 
  * @param value
  *     allowed object is
  *     {@link EntregaSAP.ConceptosFacturados }
  *     
  */
 public void setConceptosFacturados(EntregaSAP.ConceptosFacturados value) {
     this.conceptosFacturados = value;
 }

 /**
  * Gets the value of the entregas property.
  * 
  * @return
  *     possible object is
  *     {@link EntregaSAP.Entregas }
  *     
  */
 public EntregaSAP.Entregas getEntregas() {
     return detallesEntregas;
 }

 /**
  * Sets the value of the entregas property.
  * 
  * @param value
  *     allowed object is
  *     {@link EntregaSAP.Entregas }
  *     
  */
 public void setEntregas(EntregaSAP.Entregas value) {
     this.detallesEntregas = value;
 }


 /**
  * <p>Java class for anonymous complex type.
  * 
  * <p>The following schema fragment specifies the expected content contained within this class.
  * 
  * <pre>
  * &lt;complexType>
  *   &lt;complexContent>
  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *       &lt;sequence>
  *         &lt;element name="centro" type="{http://www.w3.org/2001/XMLSchema}string"/>
  *         &lt;element name="idCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
  *         &lt;element name="fechaVenta" type="{http://www.w3.org/2001/XMLSchema}date"/>
  *         &lt;element name="horaVenta" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
  *         &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
  *         &lt;element name="cajero" type="{http://www.w3.org/2001/XMLSchema}string"/>
  *       &lt;/sequence>
  *       &lt;attribute name="referencia" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
  *     &lt;/restriction>
  *   &lt;/complexContent>
  * &lt;/complexType>
  * </pre>
  * 
  * 
  */
 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(name = "", propOrder = {
 	"sociedad",	
     "centro",
     "idCliente",
     "fechaVenta",
     "horaVenta",
     "signo",
     "cajero"
 })
 public static class CabeceraEntregaSAP {

 	@XmlElement(required = true)
     protected String sociedad;	
     @XmlElement(required = true)
     protected String centro;
     @XmlElement(required = true)
     protected String idCliente;
     @XmlElement(required = true)
     @XmlSchemaType(name = "date")
     protected XMLGregorianCalendar fechaVenta;
     @XmlElement(required = true)
     @XmlSchemaType(name = "dateTime")
     protected XMLGregorianCalendar horaVenta;
     @XmlElement(required = true)
     protected String signo;
     @XmlElement(required = true)
     protected String cajero;
     @XmlAttribute(required = true)
     protected String referencia;

     /**
      * Gets the value of the sociedad property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */        
     public String getSociedad() {
     	return sociedad; 
     } 
     /**
      * Sets the value of the sociedad property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */        
     public void setSociedad(String sociedad) { 
     	this.sociedad = sociedad; 
     }        
     /**
      * Gets the value of the centro property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getCentro() {
         return centro;
     }

     /**
      * Sets the value of the centro property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setCentro(String value) {
         this.centro = value;
     }

     /**
      * Gets the value of the idCliente property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getIdCliente() {
         return idCliente;
     }

     /**
      * Sets the value of the idCliente property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setIdCliente(String value) {
         this.idCliente = value;
     }

     /**
      * Gets the value of the fechaVenta property.
      * 
      * @return
      *     possible object is
      *     {@link XMLGregorianCalendar }
      *     
      */
     public XMLGregorianCalendar getFechaVenta() {
         return fechaVenta;
     }

     /**
      * Sets the value of the fechaVenta property.
      * 
      * @param value
      *     allowed object is
      *     {@link XMLGregorianCalendar }
      *     
      */
     public void setFechaVenta(XMLGregorianCalendar value) {
         this.fechaVenta = value;
     }

     /**
      * Gets the value of the horaVenta property.
      * 
      * @return
      *     possible object is
      *     {@link XMLGregorianCalendar }
      *     
      */
     public XMLGregorianCalendar getHoraVenta() {
         return horaVenta;
     }

     /**
      * Sets the value of the horaVenta property.
      * 
      * @param value
      *     allowed object is
      *     {@link XMLGregorianCalendar }
      *     
      */
     public void setHoraVenta(XMLGregorianCalendar value) {
         this.horaVenta = value;
     }

     /**
      * Gets the value of the signo property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getSigno() {
         return signo;
     }

     /**
      * Sets the value of the signo property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setSigno(String value) {
         if (value.equals("8") || value.equals("3")) 
     		this.signo = "+";
     	else 
     		this.signo = "-";
     }

     /**
      * Gets the value of the cajero property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getCajero() {
         return cajero;
     }

     /**
      * Sets the value of the cajero property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setCajero(String value) {
         this.cajero = value;
     }

     /**
      * Gets the value of the referencia property.
      * 
      * @return
      *     possible object is
      *     {@link String }
      *     
      */
     public String getReferencia() {
         return referencia;
     }

     /**
      * Sets the value of the referencia property.
      * 
      * @param value
      *     allowed object is
      *     {@link String }
      *     
      */
     public void setReferencia(String value) {
         this.referencia = value;
     }

 }


 /**
  * <p>Java class for anonymous complex type.
  * 
  * <p>The following schema fragment specifies the expected content contained within this class.
  * 
  * <pre>
  * &lt;complexType>
  *   &lt;complexContent>
  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *       &lt;sequence>
  *         &lt;element name="concepto" maxOccurs="unbounded">
  *           &lt;complexType>
  *             &lt;complexContent>
  *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
  *                 &lt;sequence>
  *                   &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
  *                 &lt;/sequence>
  *                 &lt;attribute name="concepto" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
  *               &lt;/restriction>
  *             &lt;/complexContent>
  *           &lt;/complexType>
  *         &lt;/element>
  *       &lt;/sequence>
  *     &lt;/restriction>
  *   &lt;/complexContent>
  * &lt;/complexType>
  * </pre>
  * 
  * 
  */
 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(name = "", propOrder = {
     "concepto"
 })
 public static class ConceptosFacturados {

     @XmlElement(required = true)
     protected List<EntregaSAP.ConceptosFacturados.Concepto> concepto;

     /**
      * Gets the value of the concepto property.
      * 
      * <p>
      * This accessor method returns a reference to the live list,
      * not a snapshot. Therefore any modification you make to the
      * returned list will be present inside the JAXB object.
      * This is why there is not a <CODE>set</CODE> method for the concepto property.
      * 
      * <p>
      * For example, to add a new item, do as follows:
      * <pre>
      *    getConcepto().add(newItem);
      * </pre>
      * 
      * 
      * <p>
      * Objects of the following type(s) are allowed in the list
      * {@link EntregaSAP.ConceptosFacturados.Concepto }
      * 
      * 
      */
     public List<EntregaSAP.ConceptosFacturados.Concepto> getConcepto() {
         if (concepto == null) {
             concepto = new ArrayList<EntregaSAP.ConceptosFacturados.Concepto>();
         }
         return this.concepto;
     }


     /**
      * <p>Java class for anonymous complex type.
      * 
      * <p>The following schema fragment specifies the expected content contained within this class.
      * 
      * <pre>
      * &lt;complexType>
      *   &lt;complexContent>
      *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *       &lt;sequence>
      *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
      *       &lt;/sequence>
      *       &lt;attribute name="concepto" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
      *     &lt;/restriction>
      *   &lt;/complexContent>
      * &lt;/complexType>
      * </pre>
      * 
      * 
      */
     @XmlAccessorType(XmlAccessType.FIELD)
     @XmlType(name = "", propOrder = {
         "valor"
     })
     public static class Concepto {

         @XmlElement(required = true)
         protected String valor;
         @XmlAttribute(required = true)
         protected String concepto;

         /**
          * Gets the value of the valor property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getValor() {
             return valor;
         }

         /**
          * Sets the value of the valor property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setValor(String value) {
             this.valor = value;
         }

         /**
          * Gets the value of the concepto property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getConcepto() {
             return concepto;
         }

         /**
          * Sets the value of the concepto property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setConcepto(String value) {
             this.concepto = value;
         }

     }

 }


 @XmlAccessorType(XmlAccessType.FIELD)
 @XmlType(name = "", propOrder = {
     "entrega"
 })
 public static class Entregas {

     @XmlElement(required = true, name="detalle")
     protected List<EntregaSAP.Entregas.Entrega> entrega;

     /**
      * Gets the value of the entrega property.
      * 
      * <p>
      * This accessor method returns a reference to the live list,
      * not a snapshot. Therefore any modification you make to the
      * returned list will be present inside the JAXB object.
      * This is why there is not a <CODE>set</CODE> method for the entrega property.
      * 
      * <p>
      * For example, to add a new item, do as follows:
      * <pre>
      *    getEntrega().add(newItem);
      * </pre>
      * 
      * 
      * <p>
      * Objects of the following type(s) are allowed in the list
      * {@link EntregaSAP.Entregas.Entrega }
      * 
      * 
      */
     public List<EntregaSAP.Entregas.Entrega> getEntrega() {
         if (entrega == null) {
             entrega = new ArrayList<EntregaSAP.Entregas.Entrega>();
         }
         return this.entrega;
     }


     /**
      * <p>Java class for anonymous complex type.
      * 
      * <p>The following schema fragment specifies the expected content contained within this class.
      * 
      * <pre>
      * &lt;complexType>
      *   &lt;complexContent>
      *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *       &lt;sequence>
      *         &lt;element name="expedicion" type="{http://www.w3.org/2001/XMLSchema}string"/>
      *         &lt;element name="personaEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="dniEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="fechaEntrega" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
      *         &lt;element name="rangoHorarioDesde" type="{http://www.w3.org/2001/XMLSchema}time" minOccurs="0"/>
      *         &lt;element name="rangoHorarioHasta" type="{http://www.w3.org/2001/XMLSchema}time" minOccurs="0"/>
      *         &lt;element name="direccionEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="barrioEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="cpEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="localidadEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="provinciaEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="personaRecibe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="telefonoRecibe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="entreCalles" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="observaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
      *         &lt;element name="articulosEntregados">
      *           &lt;complexType>
      *             &lt;complexContent>
      *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *                 &lt;sequence>
      *                   &lt;element name="articulo" maxOccurs="unbounded">
      *                     &lt;complexType>
      *                       &lt;complexContent>
      *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *                           &lt;sequence>
      *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}long"/>
      *                             &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
      *                             &lt;element name="expedicion" type="{http://www.w3.org/2001/XMLSchema}string"/>
      *                             &lt;element name="almacen" type="{http://www.w3.org/2001/XMLSchema}string"/>
      *                             &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}short"/>
      *                             &lt;element name="serviciosRelacionados" minOccurs="0">
      *                               &lt;complexType>
      *                                 &lt;complexContent>
      *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *                                     &lt;sequence>
      *                                       &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
      *                                     &lt;/sequence>
      *                                   &lt;/restriction>
      *                                 &lt;/complexContent>
      *                               &lt;/complexType>
      *                             &lt;/element>
      *                             &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
      *                             &lt;element name="conceptosArticulos" maxOccurs="unbounded" minOccurs="0">
      *                               &lt;complexType>
      *                                 &lt;complexContent>
      *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *                                     &lt;sequence>
      *                                       &lt;element name="concepto" maxOccurs="unbounded">
      *                                         &lt;complexType>
      *                                           &lt;complexContent>
      *                                             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
      *                                               &lt;sequence>
      *                                                 &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
      *                                                 &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
      *                                               &lt;/sequence>
      *                                               &lt;attribute name="concepto" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
      *                                             &lt;/restriction>
      *                                           &lt;/complexContent>
      *                                         &lt;/complexType>
      *                                       &lt;/element>
      *                                     &lt;/sequence>
      *                                   &lt;/restriction>
      *                                 &lt;/complexContent>
      *                               &lt;/complexType>
      *                             &lt;/element>
      *                           &lt;/sequence>
      *                         &lt;/restriction>
      *                       &lt;/complexContent>
      *                     &lt;/complexType>
      *                   &lt;/element>
      *                 &lt;/sequence>
      *               &lt;/restriction>
      *             &lt;/complexContent>
      *           &lt;/complexType>
      *         &lt;/element>
      *       &lt;/sequence>
      *       &lt;attribute name="tipoEntrega" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
      *     &lt;/restriction>
      *   &lt;/complexContent>
      * &lt;/complexType>
      * </pre>
      * 
      * 
      */
     @XmlAccessorType(XmlAccessType.FIELD)
     @XmlType(name = "", propOrder = {
         "expedicion",
         "transporte",
         "personaEntrega",
         "dniEntrega",
         "fechaEntrega",
         "rangoHorarioDesde",
         "rangoHorarioHasta",
         "direccionEntrega",
         "calleEntrega",
         "numeroEntrega",
         "pisoEntrega",
         "departamentoEntrega",
         "barrioEntrega",
         "cpEntrega",
         "localidadEntrega",
         "provinciaEntrega",
         "personaRecibe",
         "telefonoRecibe",
         "entreCalles",
         "latitud",
         "longitud",
         "tiempoEspera",
         "observaciones",
         "articulosEntregados"
     })
     public static class Entrega {

         @XmlElement(required = true)
         protected String expedicion;
         protected String transporte;
         protected String personaEntrega;
         protected String dniEntrega;
         @XmlSchemaType(name = "date")
         protected XMLGregorianCalendar fechaEntrega;
         @XmlSchemaType(name = "time")
         protected XMLGregorianCalendar rangoHorarioDesde;
         @XmlSchemaType(name = "time")
         protected XMLGregorianCalendar rangoHorarioHasta;
         protected String direccionEntrega;
         protected String calleEntrega;
         protected String numeroEntrega;
         protected String pisoEntrega;
         protected String departamentoEntrega;
         protected String barrioEntrega;
         protected String cpEntrega;
         protected String localidadEntrega;
         protected String provinciaEntrega;
         protected String personaRecibe;
         protected String telefonoRecibe;
         protected String entreCalles;
         protected String latitud;
         protected String longitud;
         protected String tiempoEspera;
         protected String observaciones;
         @XmlElement(required = true)
         protected EntregaSAP.Entregas.Entrega.ArticulosEntregados articulosEntregados;
         @XmlAttribute(required = true)
         protected String tipoEntrega;

         /**
          * Gets the value of the expedicion property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getExpedicion() {
             return expedicion;
         }

         /**
          * Sets the value of the expedicion property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setExpedicion(String value) {
             this.expedicion = value;
         }

         public String getTransporte() {
				return transporte;
			}

			public void setTransporte(String transporte) {
				this.transporte = transporte;
			}

			/**
          * Gets the value of the personaEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getPersonaEntrega() {
             return personaEntrega;
         }

         /**
          * Sets the value of the personaEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setPersonaEntrega(String value) {
             this.personaEntrega = value;
         }

         /**
          * Gets the value of the dniEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getDniEntrega() {
             return dniEntrega;
         }

         /**
          * Sets the value of the dniEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setDniEntrega(String value) {
             this.dniEntrega = value;
         }

         /**
          * Gets the value of the fechaEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link XMLGregorianCalendar }
          *     
          */
         public XMLGregorianCalendar getFechaEntrega() {
             return fechaEntrega;
         }

         /**
          * Sets the value of the fechaEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link XMLGregorianCalendar }
          *     
          */
         public void setFechaEntrega(XMLGregorianCalendar value) {
             this.fechaEntrega = value;
         }

         /**
          * Gets the value of the rangoHorarioDesde property.
          * 
          * @return
          *     possible object is
          *     {@link XMLGregorianCalendar }
          *     
          */
         public XMLGregorianCalendar getRangoHorarioDesde() {
             return rangoHorarioDesde;
         }

         /**
          * Sets the value of the rangoHorarioDesde property.
          * 
          * @param value
          *     allowed object is
          *     {@link XMLGregorianCalendar }
          *     
          */
         public void setRangoHorarioDesde(XMLGregorianCalendar value) {
             this.rangoHorarioDesde = value;
         }

         /**
          * Gets the value of the rangoHorarioHasta property.
          * 
          * @return
          *     possible object is
          *     {@link XMLGregorianCalendar }
          *     
          */
         public XMLGregorianCalendar getRangoHorarioHasta() {
             return rangoHorarioHasta;
         }

         /**
          * Sets the value of the rangoHorarioHasta property.
          * 
          * @param value
          *     allowed object is
          *     {@link XMLGregorianCalendar }
          *     
          */
         public void setRangoHorarioHasta(XMLGregorianCalendar value) {
             this.rangoHorarioHasta = value;
         }

         /**
          * Gets the value of the direccionEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getDireccionEntrega() {
             return direccionEntrega;
         }

         public String getCalleEntrega() {
			return calleEntrega;
		}

		public void setCalleEntrega(String calleEntrega) {
			this.calleEntrega = calleEntrega;
		}

		public String getNumeroEntrega() {
			return numeroEntrega;
		}

		public void setNumeroEntrega(String numeroEntrega) {
			this.numeroEntrega = numeroEntrega;
		}

		public String getPisoEntrega() {
			return pisoEntrega;
		}

		public void setPisoEntrega(String pisoEntrega) {
			this.pisoEntrega = pisoEntrega;
		}

		public String getDepartamentoEntrega() {
			return departamentoEntrega;
		}

		public void setDepartamentoEntrega(String departamentoEntrega) {
			this.departamentoEntrega = departamentoEntrega;
		}

		/**
          * Sets the value of the direccionEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setDireccionEntrega(String value) {
             this.direccionEntrega = value;
         }

         /**
          * Gets the value of the barrioEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getBarrioEntrega() {
             return barrioEntrega;
         }

         /**
          * Sets the value of the barrioEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setBarrioEntrega(String value) {
             this.barrioEntrega = value;
         }

         /**
          * Gets the value of the cpEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getCpEntrega() {
             return cpEntrega;
         }

         /**
          * Sets the value of the cpEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setCpEntrega(String value) {
             this.cpEntrega = value;
         }

         /**
          * Gets the value of the localidadEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getLocalidadEntrega() {
             return localidadEntrega;
         }

         /**
          * Sets the value of the localidadEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setLocalidadEntrega(String value) {
             this.localidadEntrega = value;
         }

         /**
          * Gets the value of the provinciaEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getProvinciaEntrega() {
             return provinciaEntrega;
         }

         /**
          * Sets the value of the provinciaEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setProvinciaEntrega(String value) {
             this.provinciaEntrega = value;
         }

         /**
          * Gets the value of the personaRecibe property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getPersonaRecibe() {
             return personaRecibe;
         }

         /**
          * Sets the value of the personaRecibe property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setPersonaRecibe(String value) {
             this.personaRecibe = value;
         }

         /**
          * Gets the value of the telefonoRecibe property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getTelefonoRecibe() {
             return telefonoRecibe;
         }

         /**
          * Sets the value of the telefonoRecibe property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setTelefonoRecibe(String value) {
             this.telefonoRecibe = value;
         }

         /**
          * Gets the value of the entreCalles property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getEntreCalles() {
             return entreCalles;
         }

         /**
          * Sets the value of the entreCalles property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setEntreCalles(String value) {
             this.entreCalles = value;
         }

         public String getLatitud() {
				return latitud;
			}

			public void setLatitud(String latitud) {
				this.latitud = latitud;
			}

			public String getLongitud() {
				return longitud;
			}

			public void setLongitud(String longitud) {
				this.longitud = longitud;
			}

			public String getTiempoEspera() {
				return tiempoEspera;
			}

			public void setTiempoEspera(String tiempoEspera) {
				this.tiempoEspera = tiempoEspera;
			}

			/**
          * Gets the value of the observaciones property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getObservaciones() {
             return observaciones;
         }

         /**
          * Sets the value of the observaciones property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setObservaciones(String value) {
             this.observaciones = value;
         }

         /**
          * Gets the value of the articulosEntregados property.
          * 
          * @return
          *     possible object is
          *     {@link EntregaSAP.Entregas.Entrega.ArticulosEntregados }
          *     
          */
         public EntregaSAP.Entregas.Entrega.ArticulosEntregados getArticulosEntregados() {
             return articulosEntregados;
         }

         /**
          * Sets the value of the articulosEntregados property.
          * 
          * @param value
          *     allowed object is
          *     {@link EntregaSAP.Entregas.Entrega.ArticulosEntregados }
          *     
          */
         public void setArticulosEntregados(EntregaSAP.Entregas.Entrega.ArticulosEntregados value) {
             this.articulosEntregados = value;
         }

         /**
          * Gets the value of the tipoEntrega property.
          * 
          * @return
          *     possible object is
          *     {@link String }
          *     
          */
         public String getTipoEntrega() {
             return tipoEntrega;
         }

         /**
          * Sets the value of the tipoEntrega property.
          * 
          * @param value
          *     allowed object is
          *     {@link String }
          *     
          */
         public void setTipoEntrega(String value) {
             this.tipoEntrega = value;
         }

         public void setEmptyEntega() {
         	setTipoEntrega("ZSE");
         	setBarrioEntrega(null);
         	setDireccionEntrega(null);
         	setCpEntrega(null);
         	setDniEntrega(null);
         	setEntreCalles(null);
         	setRangoHorarioDesde(null);
         	setRangoHorarioHasta(null);
         	setLocalidadEntrega(null);
         	setPersonaEntrega(null);
         	setProvinciaEntrega(null);
         	setPersonaRecibe(null);
         	setLongitud(null);
         	setLatitud(null);
         	setTiempoEspera(null);
         	setObservaciones(null);
         }
         
         /**
          * <p>Java class for anonymous complex type.
          * 
          * <p>The following schema fragment specifies the expected content contained within this class.
          * 
          * <pre>
          * &lt;complexType>
          *   &lt;complexContent>
          *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
          *       &lt;sequence>
          *         &lt;element name="articulo" maxOccurs="unbounded">
          *           &lt;complexType>
          *             &lt;complexContent>
          *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
          *                 &lt;sequence>
          *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}long"/>
          *                   &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
          *                   &lt;element name="expedicion" type="{http://www.w3.org/2001/XMLSchema}string"/>
          *                   &lt;element name="almacen" type="{http://www.w3.org/2001/XMLSchema}string"/>
          *                   &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}short"/>
          *                   &lt;element name="serviciosRelacionados" minOccurs="0">
          *                     &lt;complexType>
          *                       &lt;complexContent>
          *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
          *                           &lt;sequence>
          *                             &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
          *                           &lt;/sequence>
          *                         &lt;/restriction>
          *                       &lt;/complexContent>
          *                     &lt;/complexType>
          *                   &lt;/element>
          *                   &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
          *                   &lt;element name="conceptosArticulos" maxOccurs="unbounded" minOccurs="0">
          *                     &lt;complexType>
          *                       &lt;complexContent>
          *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
          *                           &lt;sequence>
          *                             &lt;element name="concepto" maxOccurs="unbounded">
          *                               &lt;complexType>
          *                                 &lt;complexContent>
          *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
          *                                     &lt;sequence>
          *                                       &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
          *                                       &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
          *                                     &lt;/sequence>
          *                                     &lt;attribute name="concepto" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
          *                                   &lt;/restriction>
          *                                 &lt;/complexContent>
          *                               &lt;/complexType>
          *                             &lt;/element>
          *                           &lt;/sequence>
          *                         &lt;/restriction>
          *                       &lt;/complexContent>
          *                     &lt;/complexType>
          *                   &lt;/element>
          *                 &lt;/sequence>
          *               &lt;/restriction>
          *             &lt;/complexContent>
          *           &lt;/complexType>
          *         &lt;/element>
          *       &lt;/sequence>
          *     &lt;/restriction>
          *   &lt;/complexContent>
          * &lt;/complexType>
          * </pre>
          * 
          * 
          */
         @XmlAccessorType(XmlAccessType.FIELD)
         @XmlType(name = "", propOrder = {
             "articulo"
         })
         public static class ArticulosEntregados {

             @XmlElement(required = true)
             protected List<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo> articulo;

             /**
              * Gets the value of the articulo property.
              * 
              * <p>
              * This accessor method returns a reference to the live list,
              * not a snapshot. Therefore any modification you make to the
              * returned list will be present inside the JAXB object.
              * This is why there is not a <CODE>set</CODE> method for the articulo property.
              * 
              * <p>
              * For example, to add a new item, do as follows:
              * <pre>
              *    getArticulo().add(newItem);
              * </pre>
              * 
              * 
              * <p>
              * Objects of the following type(s) are allowed in the list
              * {@link EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo }
              * 
              * 
              */
             public List<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo> getArticulo() {
                 if (articulo == null) {
                     articulo = new ArrayList<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo>();
                 }
                 return this.articulo;
             }


             /**
              * <p>Java class for anonymous complex type.
              * 
              * <p>The following schema fragment specifies the expected content contained within this class.
              * 
              * <pre>
              * &lt;complexType>
              *   &lt;complexContent>
              *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
              *       &lt;sequence>
              *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}long"/>
              *         &lt;element name="tipo" type="{http://www.w3.org/2001/XMLSchema}string"/>
              *         &lt;element name="expedicion" type="{http://www.w3.org/2001/XMLSchema}string"/>
              *         &lt;element name="almacen" type="{http://www.w3.org/2001/XMLSchema}string"/>
              *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}short"/>
              *         &lt;element name="serviciosRelacionados" minOccurs="0">
              *           &lt;complexType>
              *             &lt;complexContent>
              *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
              *                 &lt;sequence>
              *                   &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
              *                 &lt;/sequence>
              *               &lt;/restriction>
              *             &lt;/complexContent>
              *           &lt;/complexType>
              *         &lt;/element>
              *         &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
              *         &lt;element name="conceptosArticulos" maxOccurs="unbounded" minOccurs="0">
              *           &lt;complexType>
              *             &lt;complexContent>
              *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
              *                 &lt;sequence>
              *                   &lt;element name="concepto" maxOccurs="unbounded">
              *                     &lt;complexType>
              *                       &lt;complexContent>
              *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
              *                           &lt;sequence>
              *                             &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
              *                             &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
              *                           &lt;/sequence>
              *                           &lt;attribute name="concepto" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
              *                         &lt;/restriction>
              *                       &lt;/complexContent>
              *                     &lt;/complexType>
              *                   &lt;/element>
              *                 &lt;/sequence>
              *               &lt;/restriction>
              *             &lt;/complexContent>
              *           &lt;/complexType>
              *         &lt;/element>
              *       &lt;/sequence>
              *     &lt;/restriction>
              *   &lt;/complexContent>
              * &lt;/complexType>
              * </pre>
              * 
              * 
              */
             @XmlAccessorType(XmlAccessType.FIELD)
             @XmlType(name = "", propOrder = {
                 "codigo",
                 "tipo",
                 "expedicion",
                 "almacen",
                 "cantidad",
                 "serviciosRelacionados",
                 "signo",
                 "conceptosArticulos"
             })
             public static class Articulo {

                 protected long codigo;
                 @XmlElement(required = true)
                 protected String tipo;
                 @XmlElement(required = true)
                 protected String expedicion;
                 @XmlElement(required = true)
                 protected String almacen = "0001";
                 protected short cantidad;
                 protected EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ServiciosRelacionados serviciosRelacionados;
                 @XmlElement(required = true)
                 protected String signo;
                 protected List<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ConceptosArticulos> conceptosArticulos;

                 /**
                  * Gets the value of the codigo property.
                  * 
                  */
                 public long getCodigo() {
                     return codigo;
                 }

                 /**
                  * Sets the value of the codigo property.
                  * 
                  */
                 public void setCodigo(long value) {
                     this.codigo = value;
                 }

                 /**
                  * Gets the value of the tipo property.
                  * 
                  * @return
                  *     possible object is
                  *     {@link String }
                  *     
                  */
                 public String getTipo() {
                     return tipo;
                 }

                 /**
                  * Sets the value of the tipo property.
                  * 
                  * @param value
                  *     allowed object is
                  *     {@link String }
                  *     
                  */
                 public void setTipo(String value) {
                     this.tipo = value;
                 }

                 /**
                  * Gets the value of the expedicion property.
                  * 
                  * @return
                  *     possible object is
                  *     {@link String }
                  *     
                  */
                 public String getExpedicion() {
                     return expedicion;
                 }

                 /**
                  * Sets the value of the expedicion property.
                  * 
                  * @param value
                  *     allowed object is
                  *     {@link String }
                  *     
                  */
                 public void setExpedicion(String value) {
                     this.expedicion = value;
                 }

                 /**
                  * Gets the value of the almacen property.
                  * 
                  * @return
                  *     possible object is
                  *     {@link String }
                  *     
                  */
                 public String getAlmacen() {
                     return almacen;
                 }

                 /**
                  * Sets the value of the almacen property.
                  * 
                  * @param value
                  *     allowed object is
                  *     {@link String }
                  *     
                  */
                 public void setAlmacen(String value) {
                     this.almacen = value;
                 }

                 /**
                  * Gets the value of the cantidad property.
                  * 
                  */
                 public short getCantidad() {
                     return cantidad;
                 }

                 /**
                  * Sets the value of the cantidad property.
                  * 
                  */
                 public void setCantidad(short value) {
                     this.cantidad = value;
                 }

                 /**
                  * Gets the value of the serviciosRelacionados property.
                  * 
                  * @return
                  *     possible object is
                  *     {@link EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ServiciosRelacionados }
                  *     
                  */
                 public EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ServiciosRelacionados getServiciosRelacionados() {
                     return serviciosRelacionados;
                 }

                 /**
                  * Sets the value of the serviciosRelacionados property.
                  * 
                  * @param value
                  *     allowed object is
                  *     {@link EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ServiciosRelacionados }
                  *     
                  */
                 public void setServiciosRelacionados(EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ServiciosRelacionados value) {
                     this.serviciosRelacionados = value;
                 }

                 /**
                  * Gets the value of the signo property.
                  * 
                  * @return
                  *     possible object is
                  *     {@link String }
                  *     
                  */
                 public String getSigno() {
                     return signo;
                 }

                 /**
                  * Sets the value of the signo property.
                  * 
                  * @param value
                  *     allowed object is
                  *     {@link String }
                  *     
                  */
                 public void setSigno(String value) {
                     this.signo = value;
                 }

                 /**
                  * Gets the value of the conceptosArticulos property.
                  * 
                  * <p>
                  * This accessor method returns a reference to the live list,
                  * not a snapshot. Therefore any modification you make to the
                  * returned list will be present inside the JAXB object.
                  * This is why there is not a <CODE>set</CODE> method for the conceptosArticulos property.
                  * 
                  * <p>
                  * For example, to add a new item, do as follows:
                  * <pre>
                  *    getConceptosArticulos().add(newItem);
                  * </pre>
                  * 
                  * 
                  * <p>
                  * Objects of the following type(s) are allowed in the list
                  * {@link EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ConceptosArticulos }
                  * 
                  * 
                  */
                 public List<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ConceptosArticulos> getConceptosArticulos() {
                     if (conceptosArticulos == null) {
                         conceptosArticulos = new ArrayList<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ConceptosArticulos>();
                     }
                     return this.conceptosArticulos;
                 }


                 /**
                  * <p>Java class for anonymous complex type.
                  * 
                  * <p>The following schema fragment specifies the expected content contained within this class.
                  * 
                  * <pre>
                  * &lt;complexType>
                  *   &lt;complexContent>
                  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                  *       &lt;sequence>
                  *         &lt;element name="concepto" maxOccurs="unbounded">
                  *           &lt;complexType>
                  *             &lt;complexContent>
                  *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                  *                 &lt;sequence>
                  *                   &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
                  *                   &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                  *                 &lt;/sequence>
                  *                 &lt;attribute name="concepto" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                  *               &lt;/restriction>
                  *             &lt;/complexContent>
                  *           &lt;/complexType>
                  *         &lt;/element>
                  *       &lt;/sequence>
                  *     &lt;/restriction>
                  *   &lt;/complexContent>
                  * &lt;/complexType>
                  * </pre>
                  * 
                  * 
                  */
                 @XmlAccessorType(XmlAccessType.FIELD)
                 @XmlType(name = "", propOrder = {
                     "concepto"
                 })
                 public static class ConceptosArticulos {

                     @XmlElement(required = true)
                     protected List<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ConceptosArticulos.Concepto> concepto;

                     /**
                      * Gets the value of the concepto property.
                      * 
                      * <p>
                      * This accessor method returns a reference to the live list,
                      * not a snapshot. Therefore any modification you make to the
                      * returned list will be present inside the JAXB object.
                      * This is why there is not a <CODE>set</CODE> method for the concepto property.
                      * 
                      * <p>
                      * For example, to add a new item, do as follows:
                      * <pre>
                      *    getConcepto().add(newItem);
                      * </pre>
                      * 
                      * 
                      * <p>
                      * Objects of the following type(s) are allowed in the list
                      * {@link EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ConceptosArticulos.Concepto }
                      * 
                      * 
                      */
                     public List<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ConceptosArticulos.Concepto> getConcepto() {
                         if (concepto == null) {
                             concepto = new ArrayList<EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo.ConceptosArticulos.Concepto>();
                         }
                         return this.concepto;
                     }


                     /**
                      * <p>Java class for anonymous complex type.
                      * 
                      * <p>The following schema fragment specifies the expected content contained within this class.
                      * 
                      * <pre>
                      * &lt;complexType>
                      *   &lt;complexContent>
                      *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                      *       &lt;sequence>
                      *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string"/>
                      *         &lt;element name="signo" type="{http://www.w3.org/2001/XMLSchema}string"/>
                      *       &lt;/sequence>
                      *       &lt;attribute name="concepto" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
                      *     &lt;/restriction>
                      *   &lt;/complexContent>
                      * &lt;/complexType>
                      * </pre>
                      * 
                      * 
                      */
                     @XmlAccessorType(XmlAccessType.FIELD)
                     @XmlType(name = "", propOrder = {
                         "valor",
                         "signo"
                     })
                     public static class Concepto {

                         @XmlElement(required = true)
                         protected String valor;
                         @XmlElement(required = true)
                         protected String signo;
                         @XmlAttribute(required = true)
                         protected String concepto;

                         /**
                          * Gets the value of the valor property.
                          * 
                          * @return
                          *     possible object is
                          *     {@link String }
                          *     
                          */
                         public String getValor() {
                             return valor;
                         }

                         /**
                          * Sets the value of the valor property.
                          * 
                          * @param value
                          *     allowed object is
                          *     {@link String }
                          *     
                          */
                         public void setValor(String value) {
                             this.valor = value;
                         }

                         /**
                          * Gets the value of the signo property.
                          * 
                          * @return
                          *     possible object is
                          *     {@link String }
                          *     
                          */
                         public String getSigno() {
                             return signo;
                         }

                         /**
                          * Sets the value of the signo property.
                          * 
                          * @param value
                          *     allowed object is
                          *     {@link String }
                          *     
                          */
                         public void setSigno(String value) {
                             this.signo = value;
                         }

                         /**
                          * Gets the value of the concepto property.
                          * 
                          * @return
                          *     possible object is
                          *     {@link String }
                          *     
                          */
                         public String getConcepto() {
                             return concepto;
                         }

                         /**
                          * Sets the value of the concepto property.
                          * 
                          * @param value
                          *     allowed object is
                          *     {@link String }
                          *     
                          */
                         public void setConcepto(String value) {
                             this.concepto = value;
                         }

                     }

                 }


                 /**
                  * <p>Java class for anonymous complex type.
                  * 
                  * <p>The following schema fragment specifies the expected content contained within this class.
                  * 
                  * <pre>
                  * &lt;complexType>
                  *   &lt;complexContent>
                  *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                  *       &lt;sequence>
                  *         &lt;element name="servicio" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
                  *       &lt;/sequence>
                  *     &lt;/restriction>
                  *   &lt;/complexContent>
                  * &lt;/complexType>
                  * </pre>
                  * 
                  * 
                  */
                 @XmlAccessorType(XmlAccessType.FIELD)
                 @XmlType(name = "", propOrder = {
                     "servicio"
                 })
                 public static class ServiciosRelacionados {

                     @XmlElement(required = true)
                     protected List<String> servicio;

                     /**
                      * Gets the value of the servicio property.
                      * 
                      * <p>
                      * This accessor method returns a reference to the live list,
                      * not a snapshot. Therefore any modification you make to the
                      * returned list will be present inside the JAXB object.
                      * This is why there is not a <CODE>set</CODE> method for the servicio property.
                      * 
                      * <p>
                      * For example, to add a new item, do as follows:
                      * <pre>
                      *    getServicio().add(newItem);
                      * </pre>
                      * 
                      * 
                      * <p>
                      * Objects of the following type(s) are allowed in the list
                      * {@link String }
                      * 
                      * 
                      */
                     public List<String> getServicio() {
                         if (servicio == null) {
                             servicio = new ArrayList<String>();
                         }
                         return this.servicio;
                     }

                 }

             }

         }

     }

 }

}
