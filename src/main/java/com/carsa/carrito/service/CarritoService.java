package com.carsa.carrito.service;

import java.util.concurrent.CompletableFuture;

import javax.jms.JMSException;

import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.ws.CarritoRequest;
import com.carsa.ext.service.dto.ClientePublicMCOM;

public interface CarritoService {
	String persistirCarrito(CarritoRequest carrito) throws Exception;
	public CompletableFuture<ClientePublicMCOM> crearClienteGenerarXMLSAP(WebH carrito) throws InterruptedException;
	public void panelCambioEstadoMCOM(String customerEvent) throws Exception;
	public void generarOportunidad(WebH nroCarrito);
	public ClientePublicMCOM chequearCliente(WebH carrito);
	public void XmlSAP(WebH carrito, Integer idClienteLegacy);
	public void generarFacturaAutomatica(String nroCarrito);
	public boolean actualizarNroOdenCrmCarrito(final String message) throws JMSException;
}
