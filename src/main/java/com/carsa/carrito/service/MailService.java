package com.carsa.carrito.service;

import com.carsa.carrito.domain.Mail;

public interface MailService {
	public void sendEmail(Mail mail);
	public void enviarMail(String e, String nroCarrito);
	public void enviarMailGenerico(String subject, String content);
}
