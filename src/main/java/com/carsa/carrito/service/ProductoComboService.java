package com.carsa.carrito.service;

import java.util.List;

import com.carsa.carrito.domain.ProductoCombo;

public interface ProductoComboService {
	List<ProductoCombo> findAllByCodigoCombo(String idCodigoCombo);
}
