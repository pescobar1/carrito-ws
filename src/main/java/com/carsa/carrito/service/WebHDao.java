package com.carsa.carrito.service;

import java.util.List;

import com.carsa.carrito.domain.EstadoMerlin;
import com.carsa.carrito.domain.WebCli;
import com.carsa.carrito.domain.WebH;


public interface WebHDao {
	
	WebH getByNroCarrito(String nroCarrito);
	
	WebH getByNroPedidoAndEstadoNoFacturado(Integer nroPedido);

	void update(WebH webH);

	void save(WebH webH);
	
	void delete(WebH webH);

	List<WebH> findAll(String nroCarrito,Integer nroPagina,Integer size);

	List<WebCli> findCarritosSinProcesarMerlin();

	Boolean actualizarEstadoMerlin(String nroCarrito, EstadoMerlin estado);

	WebH findCarritoByNroFactura(String puntoNro);

	Boolean isPendienteOrdenCrm(String nroCarrito);

	List<WebH> findByNumeroCarro(String nroCarrito);
	
	Boolean actualizarEstadoCarrito(String nroCarrito, String estado);
	
	Boolean actualizarNroOrdenCrmCarrito(String nroCarrito, String nroOrdenCrm);

	Boolean actualizarCarritoRt(String nroCarrito, String nroOrdenCrm,
			String estado);

	List<WebH> findCarritosByCliente(Integer dni);

}
