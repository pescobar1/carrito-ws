package com.carsa.carrito.service;
import java.util.List;

import com.carsa.carrito.domain.EstadoMerlin;
import com.carsa.carrito.domain.WebCli;
import com.carsa.carrito.domain.WebH;

public interface WebHService {
	
	public void guardar(WebH webH);
	
	public void borrar(WebH webH);
	
	public WebH getByNroCarrito(String nroCarrito);
	
	public WebH getByNroPedidoAndEstadoNoFacturado(Integer nroPedido);
	
	public List<WebH> findAll(Integer nroPagina,Integer size);
	
	public List<WebH> findByNumeroCarro(String nroCarrito);
	
	public List<WebCli> findCarritosSinProcesarMerlin();
	
	public Boolean actualizarEstadoMerlin(String nroCarrito, EstadoMerlin estado);

	public WebH findCarritoByNroFactura(String puntoNro);
	
	public Boolean isPendienteOrdenCrm(String nroCarrito);

	public Boolean actualizarEstadoCarrito(String nroCarrito, String estado);
	
	public Boolean actualizarNroOrdenCrmCarrito(String nroCarrito, String nroOrdenCrm);

	public Boolean actualizarCarritoRt(String nroCarrito, String nroOrdenCrm,
			String estado);

	WebH updateCarro(WebH webH);
	
	public List<WebH> findCarritosByCliente(Integer dni);
	

}
