package com.carsa.carrito.service.impl;

import java.io.StringReader;
import java.util.Date;
import java.util.concurrent.CompletableFuture;

import javax.jms.JMSException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.InputSource;

import com.carsa.carrito.service.CarritoService;
import com.carsa.carrito.service.MailService;
import com.carsa.carrito.service.WebHService;
import com.carsa.carrito.ws.CarritoRequest;
import com.carsa.carrito.ws.CarritoWS;
import com.carsa.customer.tracer.dto.CustomerEventDto;
import com.carsa.domain.merlin.DomicilioNormalizadoMerlin;
import com.carsa.domain.merlin.RequestDomicilioNormalizado;
import com.carsa.domain.utils.FiltroFacturadorAutomatico;
import com.carsa.ext.service.dto.ClientePublicMCOM;
import com.google.gson.Gson;
import com.carsa.carrito.tools.Converter;
import com.carsa.carrito.tools.EventosServices;
import com.carsa.carrito.tools.JmsProducer;
import com.carsa.carrito.tools.MerlinServices;
import com.carsa.carrito.tools.RequestGenerator;
import com.carsa.carrito.tools.Soporte;
import com.carsa.carrito.domain.EstadoMerlin;
import com.carsa.carrito.domain.RequestDomicilio;
import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.hybris.panelmcom.EstadoPanelHybris;
import com.carsa.carrito.oportunidad.OportunidadCarro;
import com.carsa.carrito.panelmcom.ConvertersCustomEventToCambiarEstado;
import com.carsa.carrito.panelmcom.ConvertersCustomEventToEstadoHybris;
import com.carsa.carrito.panelmcom.ws.ClientEstadoWS;
import com.carsa.carrito.panelmcom.ws.cambiarEstado.org.tempuri.CambiarEstado;
import com.carsa.carrito.panelmcom.ws.cambiarEstado.org.tempuri.CambiarEstadoResponse;

@Service
public class CarritoServiceImpl implements CarritoService {

	@Value("${rest.merlin.url}")
	private String merlinUrl;

	@Value("${rest.eventos.url}")
	private String eventosUrl;

	@Value("${rest.cliente.url}")
	private String clientesUrl;

	@Value("${rest.external.url}")
	private String externalUrl;

	@Value("${fraude.activo}")
	private boolean fraudeActivo;

	@Value("${fraude.url}")
	private String fraudeUrl;
	
	@Value("${oportunidades.url}")
	private String oportunidadesUrl;
	
	@Value("${hybris.url}")
	private String hybrisUrl;
	

	@Autowired
	MerlinServices merlinService;

	@Autowired
	EventosServices eventosService;

	@Autowired
	CarritoWS carritoWs;

	@Autowired
	private WebHService webHService;

	@Autowired
	Converter converter;

	@Autowired
	RequestGenerator requestGenerator;

	@Autowired
	MailService mailService;

	@Autowired
	JmsProducer jmsProducer;
	
	@Autowired
	ClientEstadoWS clientEstadoWs;
	
	
	//@Autowired
	//ConvertersCustomEventToEstadoHybris converterEstadoHybris;

	private static final Logger LOG = LogManager.getLogger(CarritoServiceImpl.class);

	@Async("asyncExecutor")
	public String persistirCarrito(CarritoRequest carrito) throws Exception {
		EstadoMerlin estado = null;
		String result = "";
		if (Soporte.carritoRequestEsRetiroEnTienda(carrito) == false) {
			String[] direccionAVerificar = merlinService.obtenerMapDireccion(carrito.getCliente().getDatosEnvio());
			estado = normalizarDomicilio(direccionAVerificar);
		} else {
			estado = EstadoMerlin.RETIRO_TIENDA;
		}
		// Guardo carrito a la bd
		WebH carritoGuardado = carritoWs.saveCarritoRequest(carrito, estado);

		// Envio a fraude en PRD
		if (fraudeActivo == true
				&& ((Soporte.esRetiroEnTienda(carritoGuardado) == false) || Soporte.esMayor(carritoGuardado) == true)) {
			String params = "carrito/" + carritoGuardado.getNroCarrito();
			requestGenerator.post(fraudeUrl, params, "");
		}

		// Envio evento asincrono
		// Hay que ver si es necesario!
		 untilSuccessful(carritoGuardado);

		// Creo cliente o chequeo si ya existe
		crearClienteGenerarXMLSAP(carritoGuardado);
		
		//Falta enviar a la cola de oportunidad (sigue en el flujo generarOportunidad)
		//Tal vez no hace falta el ampq solo llamar al metodo asincronamente y envia  el dato
		
		generarOportunidad(carritoGuardado);

		result = "Carrito encolado correctamente";

		return result;

	}

	public EstadoMerlin normalizarDomicilio(String[] direccionAVerificar) throws Exception {
		ResponseEntity<DomicilioNormalizadoMerlin> resultRest = null;
		EstadoMerlin result = EstadoMerlin.SIN_VALIDAR;
		RequestDomicilioNormalizado domicilioNormalizado = MerlinServices.crearDomicilioRequest(direccionAVerificar[0],
				direccionAVerificar[1], direccionAVerificar[2], direccionAVerificar[3], direccionAVerificar[4]);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		String domicilioNormalizadoJson = new Gson().toJson(new RequestDomicilio(domicilioNormalizado));
		HttpEntity<String> request = new HttpEntity<String>(domicilioNormalizadoJson, headers);

		try {
			resultRest = restTemplate.exchange(merlinUrl, HttpMethod.POST, request, DomicilioNormalizadoMerlin.class);
			if (resultRest.getStatusCode().value() == 200) {
				return merlinService.estaNormalizado(resultRest.getBody());
			}

		} catch (Exception e) {
			LOG.info("Error al normalizar el domicilio " + e.getMessage());
		}

		return result;
	}

	@Async("asyncExecutor")
	public CompletableFuture<String> envioEvento(WebH carrito) throws InterruptedException {
		CompletableFuture<String> result = CompletableFuture.completedFuture(null);
		String eventoJson = new Gson().toJson(eventosService.crearCustomerEventDtoRequest(carrito));
		JSONObject evento = null;
		try {
			evento = new JSONObject(eventoJson);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		Date dateNow = new Date();
		Long epochDate = dateNow.getTime();
		try {
			evento.put("eventDate", epochDate.toString());
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		LOG.info("json evento: " + eventoJson);

		// Envio evento
		ResponseEntity<String> resultRest = null;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(evento.toString(), headers);

		try {
			resultRest = restTemplate.exchange(eventosUrl, HttpMethod.POST, request, String.class);
			if (resultRest.getStatusCode().value() == 200) {
				result = CompletableFuture.completedFuture(resultRest.getBody());
			}

		} catch (Exception e) {
			LOG.info("Error al enviar el evento " + e.getMessage());
			result = null;
		}
		return result;
	}

	public void untilSuccessful(WebH carrito) {
		boolean result = false;
		String evento = "";
		Integer times = 10;
		try {
			while (result != true || times != 0) {
				if (envioEvento(carrito) != null) {
					result = true;
					break;
				} else {
					times = times - 1;
					Thread.sleep(6000);
				}
			}
		} catch (InterruptedException e) {
			LOG.info(e);
		}

	}

	// Crea cliente genera XML para SAP
	@Async("asyncExecutor")
	public CompletableFuture<ClientePublicMCOM> crearClienteGenerarXMLSAP(WebH carrito) throws InterruptedException {
		CompletableFuture<ClientePublicMCOM> result = CompletableFuture.completedFuture(new ClientePublicMCOM());
		ClientePublicMCOM cliente = chequearCliente(carrito);
		if (cliente != null && cliente.getIdClienteLegacy() != null) {
			LOG.info("Cliente encontrado: IdLegacy: " + cliente.getIdClienteLegacy() + " DNI: "
					+ cliente.getNumerDocumento());
			saveOrUpdateEmail(cliente.getIdClienteLegacy(), carrito.getWebCli().getMail());
			if (Soporte.esEntregaSap(carrito) == true) {
				XmlSAP(carrito, cliente.getIdClienteLegacy());
			}
			result = CompletableFuture.completedFuture(cliente);
		} else {
			LOG.info("El cliente no se pudo crear");
			String mailSubject = "Error No encontro el Cliente para carrito: " + carrito.getNroCarrito();
			String mailContent = "Error No encontro el Cliente para carrito: " + carrito.getNroCarrito()
					+ (Soporte.esEntregaSap(carrito) == true ? ", No se creo  EntregaSapXML" : "");
			carritoWs.actualizarEstadoClienteError(carrito.getNroCarrito());
			mailService.enviarMailGenerico(mailSubject, mailContent);
		}

		return result;
	}

	public boolean saveOrUpdateEmail(Integer idClienteLegacy, String mail) {
		String params = "/public/cliente/saveOrUpdateMail/idLegacy/" + idClienteLegacy.toString() + "/mail/" + mail;
		String res = requestGenerator.post(externalUrl, params, "");
		if (!res.contains("error") || !res.contains("not found")) {
			return true;
		} else {
			return false;
		}
	}

	public ClientePublicMCOM chequearCliente(WebH carrito) {
		ClientePublicMCOM cliente = new ClientePublicMCOM();
		ClientePublicMCOM result = cliente;
		Integer sexoCliente = Soporte.getSexoCliente(carrito);
		Integer nroDocumento = Soporte.getNumeroDocumentoCliente(carrito);

		String urlWithParams = clientesUrl + "/" + sexoCliente + "/" + nroDocumento;

		// Consulto cliente
		RestTemplate restTemplate = new RestTemplate();

		try {
			ResponseEntity<String> res = restTemplate.getForEntity(urlWithParams, String.class);
			if (res.getStatusCodeValue() == 200) {
				cliente = new Gson().fromJson(res.getBody(), ClientePublicMCOM.class);
				result = cliente;
				saveOrUpdateMail(cliente.getIdClienteLegacy().toString(), cliente.getEmail());
			} else if (res.getStatusCodeValue() == 404) {
				cliente = converter.carritoWebHToClientePublicMCOM(carrito);
				result = crearCliente(cliente);
			}

		} catch (Exception e) {
			LOG.info("Error al chequear cliente " + e.getMessage() + " se procede a enviar el mail");
			mailService.enviarMail(e.toString(), carrito.getNroCarrito());
		}
		return result;
	}

	public void saveOrUpdateMail(String idClienteLegacy, String mail) {
		String params = "/public/cliente/saveOrUpdateMail/idLegacy/" + idClienteLegacy + "/mail/" + mail;
		String res = requestGenerator.post(externalUrl, params, "");
		if (!res.contains("error") || !res.contains("not found")) {
			LOG.info("200 - ok - update mail");
		} else {
			LOG.info("No se pudo update el mail: " + idClienteLegacy + mail);
		}
	}

	public ClientePublicMCOM crearCliente(ClientePublicMCOM cliente) {
		ClientePublicMCOM result = new ClientePublicMCOM();
		String params = "public/guardarCliente";
		String clienteMcomJson = new Gson().toJson(cliente);
		String res = requestGenerator.post(externalUrl, params, clienteMcomJson);
		if (!res.contains("error") || !res.contains("not found")) {
			LOG.info("200 - ok - cliente guardado exitosamente");
			result = new Gson().fromJson(res, ClientePublicMCOM.class);
		} else {
			LOG.info("No se pudo guardar el cliente: " + cliente.getIdClienteLegacy());
		}
		return result;
	}

	public void XmlSAP(WebH carrito, Integer idClienteLegacy) {
		try {
			String carritoXmlSap = converter.carritoWebHToEntregaSAP(carrito, idClienteLegacy);
			LOG.info("Voy a enviar a SAP el carrito" + carritoXmlSap);
			jmsProducer.sendMessage(carritoXmlSap);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RabbitListener(queues = "${queue.panel.mcom.cambio.estado}")
	public void panelCambioEstadoMCOM(String customerEvent) throws Exception {
		LOG.info("Recieved Message From RabbitMQ: " + customerEvent);
		CustomerEventDto customerEventDto = new Gson().fromJson(customerEvent, CustomerEventDto.class);
		if (customerEventDto != null) {
			if (customerEventDto.getIdentifierValue().startsWith("H")
					|| customerEventDto.getReferenceIdentifierValue().startsWith("H")
					|| customerEventDto.getIdentifierValue().startsWith("Y")
					|| customerEventDto.getReferenceIdentifierValue().startsWith("Y")
					|| customerEventDto.getIdentifierValue().startsWith("E")
					|| customerEventDto.getReferenceIdentifierValue().startsWith("E")) {
				
				ConvertersCustomEventToEstadoHybris converter = new ConvertersCustomEventToEstadoHybris(); 
				EstadoPanelHybris estadoH = converter.transform(customerEventDto);
				LOG.info("cambio de estadoPanel Hybris " + estadoH.toString());
				String params = "panel/status?statusCode=" + estadoH.getEstado() + "&numberCart=" + estadoH.getNumeroCarro();
				String res = requestGenerator.post(externalUrl, params, "");
				LOG.info("cambio de estadoPanel Hybris result:" + res);

			} else if (customerEventDto.getIdentifierValue().startsWith("C")
					|| customerEventDto.getReferenceIdentifierValue().startsWith("C")) {
				
				ConvertersCustomEventToCambiarEstado converter = new ConvertersCustomEventToCambiarEstado();
				CambiarEstadoResponse cambiarEstadoRes = clientEstadoWs.cambioEstado(converter.transform(customerEventDto));
				LOG.info("cambio de estadoPanel NDW " + cambiarEstadoRes.isCambiarEstadoResult());

			} else {
				LOG.info("nro de carro no inia con C ni con H, no se cambia el estado en panel");
			}
		}
	}

	//@RabbitListener(queues = "${queue.oportunidad.carro}")
	@Async("asyncExecutor")
	public void generarOportunidad(WebH carrito) {
		//LOG.info("Recieved Message From RabbitMQ: " + nroCarrito);
		//WebH carrito = webHService.getByNroCarrito(nroCarrito);
		if (carrito.pagosAprobados() == true) {
			String oportunidadJson = new Gson().toJson(carritoWs.getOportunidadRequest(carrito));
			String params = "/setSdoOpoWeb";
			OportunidadCarro result = new Gson().fromJson(requestGenerator.post(oportunidadesUrl,params,oportunidadJson), OportunidadCarro.class);
			carritoWs.updateOportunidad(carrito, result);
		}
	}
	
	public void generarFacturaAutomatica(String nroCarrito) {
		FiltroFacturadorAutomatico filtroFacturadorAutomatico = carritoWs.crearFiltroAutomatico(nroCarrito);
		if(carritoWs.isPendienteOrdenCrm(filtroFacturadorAutomatico.getNroCarrito())){
			//Necesito nroOrden
			String nroOrden = "";
			carritoWs.actualizarEstadoCarritoPendienteAutomatico(filtroFacturadorAutomatico.getNroCarrito(),nroOrden);
			String filtroJson = new Gson().toJson(carritoWs.crearFiltroAutomatico(nroCarrito));
			//Send to AMQ
			jmsProducer.sendMessageFacturador(filtroJson);
		}else {
			
		}
	}
	
	@JmsListener(destination = "${queue.pagare.orden.crm}")
	public boolean actualizarNroOdenCrmCarrito(final String message) throws JMSException {
		
		LOG.info("Mensaje desde la queue de pagare Orden CRM: " + message);
		InputSource inputXML = new InputSource( new StringReader( message ) );
        
        XPath xPath = XPathFactory.newInstance().newXPath();
         
        try {
			String nroCarrito = xPath.evaluate("/novPagare/nroCarrito", inputXML);
			String nroFactura = xPath.evaluate("/novPagare/nroFactura", inputXML);
			String nroOrden = xPath.evaluate("/novPagare/nroOrden", inputXML);
			
			return carritoWs.actualizarNroOrdenCrmCarrito(nroCarrito,nroFactura,nroOrden);
			
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return false;
		
	}

}
