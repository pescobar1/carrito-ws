package com.carsa.carrito.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.carsa.carrito.domain.Mail;
import com.carsa.carrito.service.MailService;

@Service
public class MailServiceImpl implements MailService {
	
	@Value("${spring.mail.from}")
	private String from;
	
	@Autowired
	JavaMailSender mailSender;

	public void sendEmail(Mail mail) {

		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			messageHelper.setFrom(mail.getMailFrom());
			messageHelper.setTo(mail.getMailTo());
			messageHelper.setText(mail.getMailContent());
			messageHelper.setSubject(mail.getMailSubject());

		};
		try {
			mailSender.send(messagePreparator);
		} catch (MailException e) {

		}

		/*
		 * MimeMessage mimeMessage = mailSender.createMimeMessage();
		 * 
		 * try {
		 * 
		 * MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,
		 * true);
		 * 
		 * mimeMessageHelper.setSubject(mail.getMailSubject());
		 * mimeMessageHelper.setFrom(mail.getMailFrom());
		 * mimeMessageHelper.setTo(mail.getMailTo());
		 * mimeMessageHelper.setText(mail.getMailContent());
		 * 
		 * mailSender.send(mimeMessageHelper.getMimeMessage());
		 * 
		 * } catch (MessagingException e) { e.printStackTrace(); }
		 */

	}

	public void enviarMail(String e, String nroCarrito) {

		Mail mail = new Mail();
		mail.setMailFrom(from);
		mail.setMailTo("pablo.escobar@grupocarsa.com");
		mail.setMailSubject("Error Consultando Cliente carrito: " + nroCarrito);
		mail.setMailContent(e);

		sendEmail(mail);
	}
	
	public void enviarMailGenerico(String subject, String content) {

		Mail mail = new Mail();
		mail.setMailFrom(from);
		mail.setMailTo("pablo.escobar@grupocarsa.com");
		mail.setMailSubject(subject);
		mail.setMailContent(content);

		sendEmail(mail);
	}
}
