package com.carsa.carrito.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carsa.carrito.domain.ProductoCombo;
import com.carsa.carrito.repository.ProductoComboRepository;
import com.carsa.carrito.service.ProductoComboService;

@Service
public class ProductoComboServiceImpl implements ProductoComboService{
	
	@Autowired
	ProductoComboRepository productoComboRepository;

	@Override
	public List<ProductoCombo> findAllByCodigoCombo(String codigoCombo) {
		return productoComboRepository.findByCodigoCombo(codigoCombo);
	}
}



