package com.carsa.carrito.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.carsa.carrito.domain.EstadoMerlin;
import com.carsa.carrito.domain.WebCli;
import com.carsa.carrito.domain.WebD;
import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.repository.WebHRepository;
import com.carsa.carrito.service.WebHService;

@Service
public class WebHServiceImpl implements WebHService {

	@Autowired
	WebHRepository webHRepository;

	@Override
	public void guardar(WebH webH) {
		Set<WebD> test =  webH.getDetalles();
		WebH webHAux = this.webHRepository.findByNroCarrito(webH.getNroCarrito());
		Set<WebD> test2 = webHAux.getDetalles();
		if (webHAux != null && (WebH.ESTADO_PENDIENTE.equals(webHAux.getEstado()))) {
			borrar(webHAux);
			// throw new RuntimeException("test hacer rollback");
		}
		webHRepository.save(webH);

	}

	@Override
	public WebH updateCarro(WebH webH) {
		webHRepository.save(webH);
		return webH;
	}

	@Override
	public WebH getByNroCarrito(String nroCarrito) {
		return webHRepository.findByNroCarrito(nroCarrito);
	}

	@Override
	public WebH findCarritoByNroFactura(String puntoNro) {
		return webHRepository.findByPuntoNumeroFacturacionAndPuntoNumeroFacturacionIsNotNull(puntoNro);
	}

	@Override
	public WebH getByNroPedidoAndEstadoNoFacturado(Integer nroPedido) {
		return webHRepository.findByIdPedidoVentaAndEstado(nroPedido, WebH.ESTADO_PENDIENTE);
	}

	public WebHRepository getWebHDao() {
		return webHRepository;
	}

	public void setWebHRepository(WebHRepository webHRepository) {
		this.webHRepository = webHRepository;
	}

	@Override
	public void borrar(WebH webH) {
		webHRepository.delete(webH);
	}

	@Override
	public List<WebH> findByNumeroCarro(String nroCarrito) {
		return (List<WebH>) this.webHRepository.findByNroCarrito(nroCarrito);
	}

	@Override
	public List<WebH> findAll(Integer nroPagina, Integer size) {
		Pageable page = PageRequest.of(nroPagina, size);
		Page<WebH> carritos = this.webHRepository.findAll(page);
		return carritos.getContent(); 
	}

	@Override
	public List<WebCli> findCarritosSinProcesarMerlin() {
		return this.webHRepository.findCarritosSinProcesarMerlin();
	}

	@Override
	public Boolean actualizarEstadoMerlin(String nroCarrito, EstadoMerlin estado) {
		WebH carrito = this.webHRepository.findByNroCarrito(nroCarrito);
		carrito.setEstadoMerlin(estado);
		try {
			this.webHRepository.save(carrito);
			return true;
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public Boolean actualizarEstadoCarrito(String nroCarrito, String estado) {
		WebH carrito = this.webHRepository.findByNroCarrito(nroCarrito);
		carrito.setEstado(estado);
		try {
			this.webHRepository.save(carrito);
			return true;
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public Boolean isPendienteOrdenCrm(String nroCarrito) {
		List<WebH> pendientes = this.webHRepository.findByNroCarritoAndEstado(nroCarrito,
				WebH.ESTADO_PENDIENTE_ORDEN_CRM);
		if (pendientes.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Boolean actualizarNroOrdenCrmCarrito(String nroCarrito, String nroOrdenCrm) {
		WebH carrito = this.webHRepository.findByNroCarrito(nroCarrito);
		carrito.setNroOrdenCrm(nroOrdenCrm);
		try {
			this.webHRepository.save(carrito);
			return true;
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public Boolean actualizarCarritoRt(String nroCarrito, String nroOrdenCrm, String estado) {
		WebH carrito = this.webHRepository.findByNroCarrito(nroCarrito);
		carrito.setNroOrdenCrm(nroOrdenCrm);
		carrito.setEstado(estado);
		try {
			this.webHRepository.save(carrito);
			return true;
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public List<WebH> findCarritosByCliente(Integer dni) {
		return this.webHRepository.findByWebCli_NroDocumento(dni);
	}

}
