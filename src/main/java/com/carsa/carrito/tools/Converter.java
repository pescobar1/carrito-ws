package com.carsa.carrito.tools;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.carsa.carrito.domain.WebD;
import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.domain.WebP;
import com.carsa.carrito.sap.EntregaSAP;
import com.carsa.carrito.sap.EntregaSAP.CabeceraEntregaSAP;
import com.carsa.carrito.sap.EntregaSAP.Entregas;
import com.carsa.carrito.sap.EntregaSAP.Entregas.Entrega;
import com.carsa.carrito.sap.EntregaSAP.Entregas.Entrega.ArticulosEntregados;
import com.carsa.carrito.sap.EntregaSAP.Entregas.Entrega.ArticulosEntregados.Articulo;
import com.carsa.carrito.ws.CarritoRequest;
import com.carsa.domain.merlin.DomicilioNormalizadoMerlin;
import com.carsa.exceptions.ConvertirClienteException;
import com.carsa.exceptions.GeneracionXmlSapException;
import com.carsa.ext.service.dto.ClientePublicMCOM;

@Service
public class Converter {

	private MerlinServices merlinServices;
	private Map<String,String> regionesSap;
	private String codigoTransporteOca;
	private String codigoTransporteAndreani;
	private String codigoTransporteArgentino;
	
	private static final Logger LOG = LogManager.getLogger(Converter.class);
	
	@PostConstruct
	public void init() {
		regionesSap = new HashMap<String, String>();
		regionesSap.put("CAPITAL FEDERAL", "00");
		regionesSap.put("BUENOS AIRES", "01");
		regionesSap.put("CATAMARCA", "02");
		regionesSap.put("C�RDOBA", "03");
		regionesSap.put("CORDOBA", "03");
		regionesSap.put("CORRIENTES", "04");
		regionesSap.put("ENTRE RIOS", "05");
		regionesSap.put("JUJUY", "06");
		regionesSap.put("MENDOZA", "07");
		regionesSap.put("LA RIOJA", "08");
		regionesSap.put("SALTA", "09");
		regionesSap.put("SAN JUAN", "10");
		regionesSap.put("SAN LUIS", "11");
		regionesSap.put("SANTA FE", "12");
		regionesSap.put("SANTIAGO DEL ESTERO", "13");
		regionesSap.put("TUCUMAN", "14");
		regionesSap.put("TUCUM�N", "14");
		regionesSap.put("CHACO", "16");
		regionesSap.put("CHUBUT", "17");
		regionesSap.put("FORMOSA", "18");
		regionesSap.put("MISIONES", "19");
		regionesSap.put("NEUQUEN", "20");
		regionesSap.put("LA PAMPA", "21");
		regionesSap.put("RIO NEGRO", "22");
		regionesSap.put("SANTA CRUZ", "23");
	}

	public String carritoWebHToEntregaSAP(WebH carrito, Integer idClienteLegacy) throws Exception {
		if (idClienteLegacy != null && 0 != idClienteLegacy && -1 != idClienteLegacy)
		{
		try {
			//si llego aca es porque hay que enviar a SAP
			Boolean entregaVentaEnVerdeDomicilio=Soporte.esEnvioDomicilio(carrito); //&& carrito.getVentaVerde();
			Boolean entregaVentaEnVerdeOca=Soporte.esRetiroEnTiendaOca(carrito); //&& carrito.getVentaVerde();
			Boolean entregaVentaEnVerdeAndreani=Soporte.esRetiroEnTiendaAndreani(carrito); //&& carrito.getVentaVerde();
			Boolean entregaVentaEnVerdeArgentino=Soporte.esRetiroEnTiendaArgentino(carrito); //&& carrito.getVentaVerde();
			
			
			
			EntregaSAP entregaSAP = new EntregaSAP();
			LOG.debug("Voy a convertir el carritoRequest a Entrega SAP. NroCarrito:" + carrito.getNroCarrito());
			CabeceraEntregaSAP cab = new CabeceraEntregaSAP();
			String nroCarrito=Soporte.formatearNumeroCarro(carrito.getNroCarrito());		
			cab.setReferencia(nroCarrito);
			cab.setSociedad("1000");
			/*if(entregaVentaEnVerdeDomicilio || entregaVentaEnVerdeOca || entregaVentaEnVerdeAndreani || entregaVentaEnVerdeArgentino){
				cab.setCentro("M222");
			}else{*/
				cab.setCentro("M494");
			//}
			cab.setIdCliente(idClienteLegacy.toString());
			
			XMLGregorianCalendar fechaPedidoXml=toXMLGregorianCalendar(Calendar.getInstance().getTime());
			cab.setFechaVenta(fechaPedidoXml);
			cab.setHoraVenta(fechaPedidoXml);
			entregaSAP.setCabeceraEntregaSAP(cab);
			Entregas entregas = new Entregas();
			Entrega entrega = new Entrega();
			
			List<WebP> pagos=new ArrayList<WebP>(carrito.getPagos());
			List<WebD> articulos=new ArrayList<WebD>(carrito.getDetalles());
			Integer codigoRetiroCarsa =getPrimerCodigoNotNullNotEmpty(articulos);
			
			if(entregaVentaEnVerdeOca || entregaVentaEnVerdeAndreani || entregaVentaEnVerdeArgentino){
				//agregar transportista
				if(entregaVentaEnVerdeOca){
				entrega.setTransporte(codigoTransporteOca);
				}
				if(entregaVentaEnVerdeAndreani){
					entrega.setTransporte(codigoTransporteAndreani);
				}
				if(entregaVentaEnVerdeArgentino){
					entrega.setTransporte(codigoTransporteArgentino);
				}
				entrega.setTipoEntrega("ZMDO");
				codigoRetiroCarsa=222;
			}
			else
			{
				if(entregaVentaEnVerdeDomicilio){
					entrega.setTipoEntrega("ZDO");
					codigoRetiroCarsa=222;
				}else{
					entrega.setTipoEntrega("ZDF");
				}
			}
			
			if (carrito.getPagos() != null
						&& !pagos.isEmpty()) {
					if (CarritoRequest.Pagos.Pago.MEDIO_PAGO_CREDITO_MUSIMUNDO
							.equals(pagos.get(0).getCodigoTarjeta()) ) {
						if(entregaVentaEnVerdeDomicilio){
							entrega.setTipoEntrega("ZTDO");
						}else{
							entrega.setTipoEntrega("ZTDF");				
						}
					}
				}
			
			
			Formatter fmt = new Formatter();
			String codSAP = "M" + fmt.format("%03d", codigoRetiroCarsa).toString();
			entrega.setExpedicion(codSAP);
			entrega.setPersonaEntrega(carrito.getWebCli().getApellido() + "," + carrito.getWebCli().getNombre());
			entrega.setDniEntrega(carrito.getWebCli().getNroDocumento().toString());

			StringBuffer obs = new StringBuffer();
			WebP primerPago=pagos.get(0);
			if(primerPago!=null)
			{
				obs.append("Titular Tarjeta Apellido Nombre: ").append(primerPago.getApellidoTitular())
						.append(" ").append(primerPago.getNombreTitular())
						.append(", Documento:").append(primerPago.getNumeroDocumentoTitular());
				
			if("PC".equals(primerPago.getCodigoTarjeta())){
				obs.append(" Tarjeta Numero: PAGOS MIS CUENTAS");
			}
			else
			{
				if (primerPago.getNroTarjeta().length() >= 8) {
					String nroTarjeta = primerPago.getNroTarjeta().trim();
					obs.append(" Tarjeta Numero: " + nroTarjeta.substring(0, 4) + "***" + nroTarjeta.substring(nroTarjeta.length() - 4));
				}
			}
			}

			entrega.setObservaciones(obs.toString());
			ArticulosEntregados artEntregados = new ArticulosEntregados();

			boolean fechaCargada = false;
			for (WebD articulo : articulos) {
				if (articulo.getCodigoItemCarsa() != null && !articulo.getCodigoItemCarsa().toString().isEmpty() && !"0".equals(articulo.getCodigoItemCarsa().toString())
						&& !"-1".equals(articulo.getCantidad().toString()) && !esServicio(articulo.getCodigoItemEdsa().toString())) {//si es una oferta, no se mapea en el xml de SAP
					if (!fechaCargada && articulo.getFechaEntregaComprometida()!=null) {
						entrega.setFechaEntrega(toXMLGregorianCalendar(articulo.getFechaEntregaComprometida()));
					}
					Articulo art = new Articulo();
					art.setCodigo(Long.valueOf(articulo.getCodigoItemCarsa().toString()));
					/*if(entregaVentaEnVerdeDomicilio){
						art.setTipo("ZDO");
					}else{
						art.setTipo("ZDF");
					}*/
					art.setTipo(entrega.getTipoEntrega());
					fmt = new Formatter();
					codSAP = "M" + fmt.format("%03d", codigoRetiroCarsa).toString();

					art.setExpedicion(codSAP);
					art.setAlmacen("0001");
					art.setCantidad(Short.valueOf(articulo.getCantidad().toString()));
					art.setServiciosRelacionados(null);
					artEntregados.getArticulo().add(art);
				}
			}
			
			if(entregaVentaEnVerdeDomicilio){
				//entrega.setExpedicion("M222");
				entrega.setFechaEntrega(toXMLGregorianCalendar(carrito.getFechaImpacto()));
				armarDomicilioEnvio(entrega,carrito);
				entrega.setCpEntrega(carrito.getWebCli().getCodPostalEnvio());
				entrega.setLocalidadEntrega(carrito.getWebCli().getLocalidadEnvio());
				String provincia=carrito.getWebCli().getProvinciaEnvio().toUpperCase();			
				entrega.setProvinciaEntrega(this.regionesSap.get(provincia));
				entrega.setPersonaRecibe(carrito.getWebCli().getApellido()+", "+carrito.getWebCli().getNombre());
				entrega.setTelefonoRecibe(carrito.getWebCli().getTelefono());
				
				entrega.setTiempoEspera("");
			}
			entrega.setArticulosEntregados(artEntregados);
			entregas.getEntrega().add(entrega);
			entregaSAP.setEntregas(entregas);

			return marshallToXmlSAP(entregaSAP);
						
		} catch (Exception e) {
			LOG.info("Error armando la entrega para SAP en carritoRequestToEntregaSAP. " + e.getMessage());
			throw new GeneracionXmlSapException("Error armando la entrega para SAP del Carrito nro. " + carrito.getNroCarrito(), e);
		}
		}
		else
		{
			LOG.error("El idClienteLegacy es null 0 o -1 para carrito " + carrito.getNroCarrito()+" no se puede generar la entrega sap");
			throw new GeneracionXmlSapException("El idClienteLegacy es null 0 o -1 " + carrito.getNroCarrito()+" no se puede generar la entrega sap");
		}

	}
	
	private  Integer getPrimerCodigoNotNullNotEmpty(List<WebD> articulos) {
		for(WebD articulo:articulos){
			if(articulo.getCodigoRetiroCarsa()!=null && !articulo.getCodigoRetiroCarsa().toString().isEmpty()){
				return Integer.parseInt(articulo.getCodigoRetiroCarsa());
			}
		}
		return null;
	}

	private  XMLGregorianCalendar toXMLGregorianCalendar(Date date) throws DatatypeConfigurationException {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		gregorianCalendar.setTime(date);
	    XMLGregorianCalendar gregorianCalendarXml = DatatypeFactory.newInstance()
	            .newXMLGregorianCalendar(gregorianCalendar);
	    return gregorianCalendarXml;
	}

	/**
	 * Verifica si el articulo es un servicio.
	 * Si es un servicio no se mapea al XML de SAP porque solo se usa para reserva de producto.
	 * y los servicios no se reservan
	 * @param codProd
	 * @return
	 */
	public  boolean esServicio(String codProd){
		if("".equals(codProd)
			|| "900000".equals(codProd)
			|| "9999999931331".equals(codProd)
			|| "9999900003132".equals(codProd)
			|| "9999900090951".equals(codProd)
			|| "9999900031321".equals(codProd)
			|| "9999900001321".equals(codProd)
			|| "9999900090901".equals(codProd)
			|| "9999900090991".equals(codProd)
			|| "9999900000081".equals(codProd)
			//1 es financiacion credito musimundo
			|| "1".equals(codProd)
				)
			return true;
		else
			return false;
	}

	private  String marshallToXmlSAP(EntregaSAP input) throws JAXBException {
		OutputStream xmlSAP = null;
		try {
			JAXBContext jb = JAXBContext.newInstance(EntregaSAP.class);
			Marshaller marshaller = jb.createMarshaller();
			xmlSAP = new ByteArrayOutputStream();
			marshaller.marshal((EntregaSAP) input, xmlSAP);

		} catch (JAXBException e) {
			LOG.debug("Error generando el xml para SAP." + e.getMessage());
		}
		LOG.info("Obtuve exitosamente el xml de la entregaSAP con nro de referencia" + input.getCabeceraEntregaSAP().getReferencia());
		return xmlSAP.toString();

	}

	
	public  ClientePublicMCOM carritoWebHToClientePublicMCOM(WebH webh) throws ConvertirClienteException {
		try {
			ClientePublicMCOM cliente = new ClientePublicMCOM();
			LOG.debug("Voy a armar mi clientePublicMCOM con los datos del carrito. NroCarrito:" + webh.getNroCarrito());
			cliente.setApellido(webh.getWebCli().getApellido());
			cliente.setNombre(webh.getWebCli().getNombre());
			cliente.setTipoDocumento("96");
			cliente.setNumerDocumento(webh.getWebCli().getNroDocumento().toString());
			cliente.setTipoSexo(webh.getWebCli().getSexo());
			cliente.setCalle(webh.getWebCli().getCalle());
			cliente.setNumero(Integer.valueOf(webh.getWebCli().getAltura()));
			cliente.setCodigoPostal(webh.getWebCli().getCodPostal().toString());
			if (webh.getWebCli().getDepartamento() != null)
				cliente.setDepartamento(webh.getWebCli().getDepartamento());
			if (webh.getWebCli().getPiso() != null)
				cliente.setPiso(webh.getWebCli().getPiso());
			cliente.setEmail(webh.getWebCli().getMail());
			cliente.setLocalidad(webh.getWebCli().getLocalidad());
			cliente.setProvincia(webh.getWebCli().getProvincia());
			cliente.setTelefono(webh.getWebCli().getTelefono());
			cliente.setFechaNacimiento(webh.getWebCli().getFechaNacimiento());

			LOG.debug("Arme el cliente exitosamente para NroCarrito:" + webh.getNroCarrito());
			return cliente;
		} catch (Exception e) {
			LOG.debug("Error obteniendo datos del cliente del Carrito nro. " +  webh.getNroCarrito());
			throw new ConvertirClienteException("Error obteniendo datos del cliente del Carrito nro. " +  webh.getNroCarrito(), e);
		}
	}
	
	private Entrega armarDomicilioEnvio(Entrega entrega,WebH carrito)
	{
		DomicilioNormalizadoMerlin domicilioNormalizado=null;
		try {
			domicilioNormalizado=this.merlinServices.normalizarDomicilio(
					carrito.getWebCli().getCalleEnvio(),
					carrito.getWebCli().getAlturaEnvio(),
					carrito.getWebCli().getCodPostalEnvio(),
					carrito.getWebCli().getLocalidadEnvio(),
					carrito.getWebCli().getProvinciaEnvio());
		}catch(Exception exception){
			LOG.error("error al normalizar domicilio carrito "+carrito.getNroCarrito());
		}
		String domicilioEnvio="";
		
		if(domicilioNormalizado!=null && "CO".equals(domicilioNormalizado.getEstado())){
			//merlin tiene calle y numero en calle
			domicilioEnvio +=domicilioNormalizado.getCalle()+" "+domicilioNormalizado.getNumero()+" ";
			entrega.setEntreCalles(domicilioNormalizado.getEntreCalle1()+", "+domicilioNormalizado.getEntreCalle2());
			entrega.setLatitud((domicilioNormalizado.getLatitud()!=null)?domicilioNormalizado.getLatitud().toString():"");
			entrega.setLongitud((domicilioNormalizado.getLogitud()!=null)?domicilioNormalizado.getLogitud().toString():"");
		}else{
			domicilioEnvio +=carrito.getWebCli().getCalleEnvio()==null?"":carrito.getWebCli().getCalleEnvio()+" ";
			domicilioEnvio +=carrito.getWebCli().getAlturaEnvio()==null?"":carrito.getWebCli().getAlturaEnvio()+" ";			
			entrega.setEntreCalles("");
			entrega.setLatitud("");
			entrega.setLongitud("");
		}		
		domicilioEnvio +=carrito.getWebCli().getPisoEnvio()==null?"":carrito.getWebCli().getPisoEnvio()+" ";
		domicilioEnvio +=carrito.getWebCli().getDepartamentoEnvio()==null?"":carrito.getWebCli().getDepartamentoEnvio()+" ";
		
		entrega.setDireccionEntrega(domicilioEnvio);
		entrega.setCalleEntrega(carrito.getWebCli().getCalleEnvio()==null?"":carrito.getWebCli().getCalleEnvio());
		entrega.setNumeroEntrega(carrito.getWebCli().getAlturaEnvio()==null?"":carrito.getWebCli().getAlturaEnvio());
		entrega.setPisoEntrega(carrito.getWebCli().getPisoEnvio()==null?"":carrito.getWebCli().getPisoEnvio());
		entrega.setDepartamentoEntrega(carrito.getWebCli().getDepartamentoEnvio()==null?"":carrito.getWebCli().getDepartamentoEnvio());
		

		
		return entrega;
	}

	public MerlinServices getMerlinServices() {
		return merlinServices;
	}

	public void setMerlinServices(MerlinServices merlinServices) {
		this.merlinServices = merlinServices;
	}

	public String getCodigoTransporteOca() {
		return codigoTransporteOca;
	}

	public void setCodigoTransporteOca(String codigoTransporteOca) {
		this.codigoTransporteOca = codigoTransporteOca;
	}

	public void setCodigoTransporteAndreani(String codigoTransporteAndreani) {
		this.codigoTransporteAndreani = codigoTransporteAndreani;
	}

	public void setCodigoTransporteArgentino(String codigoTransporteArgentino) {
		this.codigoTransporteArgentino = codigoTransporteArgentino;
	}
	
}
