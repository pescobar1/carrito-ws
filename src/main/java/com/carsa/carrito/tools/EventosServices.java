package com.carsa.carrito.tools;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.carsa.carrito.domain.WebH;
import com.carsa.customer.tracer.dto.BusinessEventType;
import com.carsa.customer.tracer.dto.BusinessProcessName;
import com.carsa.customer.tracer.dto.CustomerEventDto;
import com.carsa.customer.tracer.dto.CustomerEventDto.CustomerEventSource;
import com.carsa.customer.tracer.dto.CustomerEventDto.CustomerIdentity;
import com.carsa.customer.tracer.dto.IdentifierType;

@Service
public class EventosServices {
	
	public CustomerEventDto crearCustomerEventDtoRequest(WebH carrito){
		CustomerEventDto customerEventDto=new CustomerEventDto();
		customerEventDto.setEventType(BusinessEventType.INGRESO_CARRITO);
		customerEventDto.setIdentifierType(IdentifierType.NRO_CARRITO);
		customerEventDto.setEventMessage("evento generado");
		String nroCarrito=Soporte.formatearNumeroCarro(carrito.getNroCarrito());
		customerEventDto.setIdentifierValue(nroCarrito);
		customerEventDto.setReferenceIdentifierType(IdentifierType.DOCUMENTO_SEXO);		
		customerEventDto.setReferenceIdentifierValue(carrito.getWebCli().getNroDocumento().toString());
		customerEventDto.setEventDate(new Date());

		CustomerEventSource customerEventSource=new CustomerEventSource();
		customerEventSource.setSystem("ESB");
		customerEventSource.setProcess(BusinessProcessName.VENTA_MCOM);		
		customerEventDto.setCustomerEventSource(customerEventSource);
		
		CustomerEventDto.CustomerIdentity customerIdentity=new CustomerIdentity();
		customerIdentity.setApellido(carrito.getWebCli().getApellido());
		customerIdentity.setNombre(carrito.getWebCli().getNombre());
		customerIdentity.setNumeroDocumento(carrito.getWebCli().getNroDocumento().toString());
		customerIdentity.setSexo(carrito.getWebCli().getSexo());
		customerEventDto.setCustomerIdentity(customerIdentity);

		return customerEventDto;
	}

	

}
