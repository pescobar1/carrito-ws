package com.carsa.carrito.tools;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class JmsProducer {

    @Autowired
    JmsTemplate jmsTemplate;

    @Value("${queue.sap.entrega}")
    private String sap;
    
    @Value("${queue.carrito.orden.crm}")
    private String ordenCrm;
    
    @Value("${queue.carrito.facturador.automatico}")
	private String facturadorAutomatico;
    
    @Value("${queue.sap.desbloquear.orden.crm}")
    private String desbloquearOrdenCrm;

    public void sendMessage(String carritoSAPXml){
        try{
            jmsTemplate.convertAndSend(sap, carritoSAPXml);
        } catch(Exception e){
           System.out.println("Recieved Exception during send Message: " + e);
        }
    }
    
    public void sendMessageOrdenCrm(String evento){
        try{
            jmsTemplate.convertAndSend(ordenCrm, evento);
        } catch(Exception e){
           System.out.println("Recieved Exception during send Message: " + e);
        }
    }
    
    public void sendMessageFacturador(String factura){
        try{
            jmsTemplate.convertAndSend(facturadorAutomatico, factura);
        } catch(Exception e){
           System.out.println("Recieved Exception during send Message: " + e);
        }
    }
    public void sendMessageDesbloquearOrden(String ordenCrm){
    	try{
            jmsTemplate.convertAndSend(desbloquearOrdenCrm, ordenCrm);
        } catch(Exception e){
           System.out.println("Recieved Exception during send Message: " + e);
        }
    }
}
