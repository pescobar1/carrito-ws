package com.carsa.carrito.tools;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.carsa.carrito.ws.CarritoRequest;
import com.carsa.carrito.ws.CarritoRequest.Cliente.DatosEnvio;
import com.carsa.carrito.domain.EstadoMerlin;
import com.carsa.carrito.domain.WebCli;
import com.carsa.carrito.service.WebHService;
import com.carsa.domain.merlin.DomicilioNormalizadoMerlin;
import com.carsa.domain.merlin.RequestDomicilioNormalizado;
import com.carsa.integration.marshal.IntegrationMarshaller;

@Service
public class MerlinServices {
	private IntegrationMarshaller integrationMarshaller;
	
	@Value("${rest.merlin.url}")
	private String merlinUrl;
	
	private RestTemplate restTemplate;
	private WebHService webHService;
	
	public static RequestDomicilioNormalizado crearDomicilioRequest(String calle,String numero,String cp,String localidad,String provincia){
		return new RequestDomicilioNormalizado(calle+" "+numero, cp, localidad, provincia);
	}

	public String domicilioRequestToJson(RequestDomicilioNormalizado domicilio) throws Exception{
		return integrationMarshaller.marshal(domicilio);		
	}
	
	public DomicilioNormalizadoMerlin normalizarDomicilio(String calle,String numero,String cp,String localidad, String provincia) throws Exception{
		String request=integrationMarshaller.marshal(new RequestDomicilioNormalizado(calle+" "+numero, cp, localidad, provincia));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request ,headers);
		String responseS=restTemplate.postForObject(merlinUrl, entity,String.class);
		DomicilioNormalizadoMerlin response=(DomicilioNormalizadoMerlin) integrationMarshaller.unMarshall(responseS);
		return response;
	}
	
	public DomicilioNormalizadoMerlin jsonToDomicilioNormalizadoMerlin(String json) throws Exception{
		return (DomicilioNormalizadoMerlin) integrationMarshaller.unMarshall(json);
	}
	
	public EstadoMerlin estaNormalizado(DomicilioNormalizadoMerlin domicilo){
		if("CO".equals(domicilo.getEstado())){
			return EstadoMerlin.AUT_OK;
		}
		return EstadoMerlin.AUT_ERROR;
	}
	
	public void setIntegrationMarshaller(IntegrationMarshaller integrationMarshaller) {
		this.integrationMarshaller = integrationMarshaller;
	}

	public void setWebHService(WebHService webHService) {
		this.webHService = webHService;
	}
	
	public List<WebCli> consultarCarritosSinProcesarMerlin(){
		List<WebCli> result=webHService.findCarritosSinProcesarMerlin();
		return result;
	}
	
	public Boolean actualizarEstadoMerlin(WebCli clienteCarrito,EstadoMerlin estadoMelin){
		if(estadoMelin.getEstado()!=0){
		 webHService.actualizarEstadoMerlin(clienteCarrito.getNroCarrito(), estadoMelin);
		 return true;
		 }
		return false;
	}
	
	public static String[] obtenerMapDireccion(Object direccionResources){
		String[] resultado=null;
		if(direccionResources instanceof CarritoRequest.Cliente.DatosEnvio){
			CarritoRequest.Cliente.DatosEnvio domicilioEnvio=(DatosEnvio) direccionResources;
			resultado=new String[]{domicilioEnvio.getCalle(),(domicilioEnvio.getNumero()==null)?"":domicilioEnvio.getNumero().toString(),(domicilioEnvio.getCodigoPostal()==null)?"":domicilioEnvio.getCodigoPostal().toString(),domicilioEnvio.getLocalidad(),domicilioEnvio.getProvincia()};
			return resultado;
		}
		if(direccionResources instanceof WebCli){
			WebCli domicilioEnvio=(WebCli) direccionResources;
			resultado=new String[]{domicilioEnvio.getCalleEnvio(),domicilioEnvio.getAlturaEnvio(),domicilioEnvio.getCodPostalEnvio(),domicilioEnvio.getLocalidadEnvio(),domicilioEnvio.getProvinciaEnvio()};
			return resultado;
		}
		
		return resultado;
		
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	
	
}
