package com.carsa.carrito.tools;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class RequestGenerator {
	
	private static final Logger LOG = LogManager.getLogger(RequestGenerator.class);
	private String url;
	private String params;
	private Object body;
	
	public RequestGenerator() {
		
	}
	
	//Constructor for POST
	public RequestGenerator(String url,Object body) {
		this.url = url;
		this.body = body;
	}
	
	//Constructor for GET
	public RequestGenerator(String url,String params) {
		this.url = url;
		this.params = params;
	}
	
	public String get(String url,String params) {
		String result = "";
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> res = restTemplate.getForEntity(url + params, String.class);
			if (res.getStatusCodeValue() == 200) {
				result = res.getBody();
			}else if(res.getStatusCodeValue() == 404) {
				result = "not found";
			}else {
				result = "error";
			}

		} catch (Exception e) {
			LOG.info("Error al enviar request" + e.getMessage());
			result =  e.getMessage();
		}
		return result;
	}
	
	public String post(String url,String params,String body) {
		String result = "";
		ResponseEntity<String> resultRest = null;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<String>(body, headers);
		try {
			resultRest = restTemplate.exchange(url + params, HttpMethod.POST, request, String.class);
			if (resultRest.getStatusCode().value() == 200) {
				LOG.info("Peticion enviada correctamente. \n Resultado: " + resultRest.getBody());
				result = resultRest.getBody();
			}

		} catch (Exception e) {
			LOG.info("Error al enviar la request: " + e.getMessage());
			result =  e.getMessage();
		}
		return result;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getParams() {
		return params;
	}
;
	public void setParams(String params) {
		this.params = params;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}
	
}
