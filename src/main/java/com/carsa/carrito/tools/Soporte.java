package com.carsa.carrito.tools;

import java.util.Formatter;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.carsa.carrito.domain.WebD;
import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.domain.WebP;
import com.carsa.carrito.ws.CarritoRequest;
import com.carsa.carrito.ws.CarritoWSImpl;
import com.carsa.ext.service.dto.ClientePublicDto;
import com.carsa.ext.service.dto.ClientePublicMCOM;


public class Soporte {
	private static final Logger LOG = LogManager.getLogger(CarritoWSImpl.class);
	

	public static Integer getSexoCliente(WebH carrito) {
		LOG.debug("Obteniendo sexo cliente para consultar. NroCarrito:" + carrito.getNroCarrito());
		String sexo = carrito.getWebCli().getSexo();
		if ("F".equals(sexo))
			return 1;
		else
			return 0;
	}

	public static Integer getNumeroDocumentoCliente(WebH carrito) {
		LOG.debug("Obteniendo numero documento de cliente para consultar. NroCarrito:" + carrito.getNroCarrito());
		return carrito.getWebCli().getNroDocumento();
	}

	/**
	 * Cuando el cliente existe, llega un ClientePublicDto Cuando se crea en el
	 * proceso, llega un ClientePublicMCOM
	 * 
	 * @param cliente
	 * @return
	 */
	public static Integer getIdClienteLegacy(Object cliente) {
		if (cliente instanceof ClientePublicDto)
			return ((ClientePublicDto) cliente).getIdClienteLegacy();
		else if (cliente instanceof ClientePublicMCOM)
			return ((ClientePublicMCOM) cliente).getIdClienteLegacy();
		else
			return null;

	}
	
	public static boolean carritoRequestEsRetiroEnTienda(CarritoRequest carrito) {
		List<CarritoRequest.Articulos.Articulo> articulos = carrito.getArticulos().getArticulo();
		for (com.carsa.carrito.ws.CarritoRequest.Articulos.Articulo articulo : articulos) {
			String codigo = articulo.getCodigoRetiroCarsa();
			try{
			if (codigo == null || "".equals(codigo) || Integer.valueOf(codigo)>10000) {
				LOG.debug("No es retiro en tienda. Carrito Nro. " + carrito.getCarrito().getNumeroCarro());
				return false;
			} else {
				break;
			}
			}catch(NumberFormatException e){				
				LOG.error("Error al convertir codigo retiroCarsa, Carrito Nro. " + carrito.getCarrito().getNumeroCarro());
				return true;
			}
		}
		LOG.debug("Es retiro en tienda. Voy a generar XML para SAP. Carrito Nro. " + carrito.getCarrito().getNumeroCarro());
		return true;
	}

	public static boolean esRetiroEnTienda(WebH carrito) {
		Set<WebD>  articulos=carrito.getDetalles();
		for (WebD articulo : articulos) {
			String codigo = articulo.getCodigoRetiroCarsa();
			try{
			if (codigo != null && !codigo.trim().isEmpty()){
				if(Integer.valueOf(codigo)>10000) {
					LOG.info("No es retiro en tienda. Carrito Nro. " + carrito.getNroCarrito());
					return false;
				}else{
					LOG.debug("Es retiro en tienda. Voy a generar XML para SAP. Carrito Nro. " + carrito.getNroCarrito());
					return true;
				}
			}
			}catch(NumberFormatException e){				
				LOG.error("Error al convertir codigo retiroCarsa, Carrito Nro. " + carrito.getNroCarrito());
				return false;
			}
		}
		LOG.debug("Ningun item webd posee codigo valido " + carrito.getNroCarrito());
		return false;
	}
	
	public static boolean esRetiroEnTiendaOca(WebH carrito) {
		Set<WebD>  articulos=carrito.getDetalles();
		for (WebD articulo : articulos) {
			String codigo = articulo.getCodigoRetiroCarsa();
			try{
			if (codigo != null && !codigo.trim().isEmpty()){
				if(Integer.valueOf(codigo)>10000 && Integer.valueOf(codigo)<20000) {
					LOG.info("No es retiro en tienda. Carrito Nro. " + carrito.getNroCarrito());
					return true;
				}
			}
			}catch(NumberFormatException e){				
				LOG.error("Error al convertir codigo retiroOCA, Carrito Nro. " + carrito.getNroCarrito());
				return false;
			}
		}
		return false;
	}
	
	public static boolean esRetiroEnTiendaAndreani(WebH carrito) {
		Set<WebD>  articulos=carrito.getDetalles();
		for (WebD articulo : articulos) {
			String codigo = articulo.getCodigoRetiroCarsa();
			try{
			if (codigo != null && !codigo.trim().isEmpty()){
				if(Integer.valueOf(codigo)>=20000 && Integer.valueOf(codigo)<30000) {
					LOG.info("No es retiro en tienda. Carrito Nro. " + carrito.getNroCarrito());
					return true;
				}
			}
			}catch(NumberFormatException e){				
				LOG.error("Error al convertir codigo retiroAndreani, Carrito Nro. " + carrito.getNroCarrito());
				return false;
			}
		}
		return false;
	}
	
	
	public static boolean esRetiroEnTiendaArgentino(WebH carrito) {
		Set<WebD>  articulos=carrito.getDetalles();
		for (WebD articulo : articulos) {
			String codigo = articulo.getCodigoRetiroCarsa();
			try{
			if (codigo != null && !codigo.trim().isEmpty()){
				if(Integer.valueOf(codigo)>30000) {
					LOG.info("No es retiro en tienda. Carrito Nro. " + carrito.getNroCarrito());
					return true;
				}
			}
			}catch(NumberFormatException e){				
				LOG.error("Error al convertir codigo retiroArgentino, Carrito Nro. " + carrito.getNroCarrito());
				return false;
			}
		}
		return false;
	}
	
	public static boolean esEnvioDomicilio(WebH carrito) {
		Boolean envioDomicilio=true;
		Set<WebD>  articulos=carrito.getDetalles();
		for (WebD articulo : articulos) {
			String codigo = articulo.getCodigoRetiroCarsa();
			if (codigo != null && (codigo != null && !"".equals(codigo.trim()))){
				LOG.info("No Es entrega a domicilio. " + carrito.getNroCarrito());
				return false;
			}
		}
		LOG.info("Es entrega a domicilio. " + carrito.getNroCarrito());
		return envioDomicilio;
	}
	
	/*
	 * Verifica si es posible enviar xml de entrega a sap
	 * debe ser 'retiro en tienda' / 'venta verde' y los pagos deben ser aprobados
	 * 
	 */
	public static boolean esEntregaSap(WebH carrito){
		//si el pago esta aprobado genera xml sap desde mule
		//if(esRetiroEnTienda(carrito) || carrito.getVentaVerde()){
			Set<WebP> pagos=carrito.getPagos();
			for(WebP pago: pagos){
				if(pago.getEstado()!=null && !"Aprobado".equals(pago.getEstado())){
					LOG.info(new StringBuilder("el carrito ").append(carrito.getNroCarrito()).append(" no esta con pago Aprobado, no se envia a sap").toString());
					return false;
				}
			}
			return true;
		//}
		
		//return false;
	}
	
	public static boolean isRetiroTienda(WebH carrito){
		//los retiro tienda oca no son entrega en domicilio ni retiro sucursal-> no genera xml sap desde mule
		if(esRetiroEnTienda(carrito)){
			Set<WebP> pagos=carrito.getPagos();
			for(WebP pago: pagos){
				if(pago.getEstado()!=null && !"Aprobado".equals(pago.getEstado())){
					LOG.info(new StringBuilder("el carrito ").append(carrito.getNroCarrito()).append(" no esta con pago Aprobado, no se envia a sap").toString());
					return false;
				}
			}
			return true;
		}	
		return false;
	}
	
	/*
	 * Se formatea el numero de carro si llega desde ndw a C000000000
	 * En caso de venir de Hybris el carrito ya se encuentra formateado H000000000
	 */
	static public String formatearNumeroCarro(String nroCarrito) {
		char[] chart=nroCarrito.toCharArray();
		if(Character.isAlphabetic(chart[0])){
			if(chart[0]=='H' || chart[0]=='C' || chart[0]=='P' || chart[0]=='E'|| chart[0]=='Y'){
				return nroCarrito;
			}					
				throw new RuntimeException("El carro debe iniciar con H, P, C, E, Y o ser un numero");			
		}
		else
		{	//es un numero de carro puro de ndw
			Integer numero = Integer.parseInt(nroCarrito);
			Formatter fmt = new Formatter();
			fmt.format("%09d", numero);
			return new StringBuilder().append("C").append(fmt.toString()).toString();
		}
	}
	
	
	/*
	 * Se formatea el numero de carro si llega desde ndw con NroEventoEspeciall empezando con EE a E000000000
	 */
	
	static public String formatearNumeroCarroEE(String nroCarrito) {
		//es un numero de carro puro de ndw pero de Evento Especial
			Integer numero = Integer.parseInt(nroCarrito);
			Formatter fmt = new Formatter();
			fmt.format("%09d", numero);
			return new StringBuilder().append("E").append(fmt.toString()).toString();
		
	}
	
	//Evaluo monto para enviar a fraude
	public static boolean esMayor(WebH carrito) {
		LOG.debug("Obteniendo el monto total del NroCarrito:" + carrito.getNroCarrito());
		Double montoFraude = new Double(100000);
		
		if(carrito.getTotal() > montoFraude){
			return true;
		}else{
			return false;
		}
	}
}
