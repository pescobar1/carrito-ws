//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.16 a las 10:55:15 AM ART 
//

package com.carsa.carrito.ws;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import com.carsa.carrito.ws.CarritoRequest.Articulos.Articulo;

/**
 * <p>
 * Clase Java para anonymous complex type.
 * 
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="carrito">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="numerointerno" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                   &lt;element name="fechaPedio" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                 &lt;/sequence>
 *                 &lt;attribute name="numeroCarro" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="cliente">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="datosCliente">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="codigoDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="numeroDocumento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="domicilio">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                                       &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="direccionMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="datosFacturacion">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="datosEnvio">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="articulos">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="articulo">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="conceptoVenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigoEDSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="codigoCARSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="codigoEMSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="precio" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="decripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="pagos">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="pago">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="producto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="importeCupon" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="voucherID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="codigoAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="merchantID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="lote" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *                             &lt;element name="descripcon" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="descuento" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="npsstatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "carrito", "cliente", "datosNPS", "articulos", "pagos" })
@XmlRootElement(name = "carrito-request")
public class CarritoRequest {

	@XmlElement(required = true)
	@Valid
	protected CarritoRequest.Carrito carrito;
	
	@XmlElement(required = true)
	@Valid
	protected CarritoRequest.Cliente cliente;
	
	@XmlElement(required = true)
	@Valid
	protected CarritoRequest.Articulos articulos;
	
	@XmlElement(required = true)
	@Valid
	protected CarritoRequest.Pagos pagos;
	
	@XmlElement(required = false)
	@Valid
	protected CarritoRequest.DatosNps datosNPS;

	/**
	 * Obtiene el valor de la propiedad carrito.
	 * 
	 * @return possible object is {@link CarritoRequest.Carrito }
	 * 
	 */
	public CarritoRequest.Carrito getCarrito() {
		return carrito;
	}

	/**
	 * Define el valor de la propiedad carrito.
	 * 
	 * @param value allowed object is {@link CarritoRequest.Carrito }
	 * 
	 */
	public void setCarrito(CarritoRequest.Carrito value) {
		this.carrito = value;
	}

	/**
	 * Obtiene el valor de la propiedad cliente.
	 * 
	 * @return possible object is {@link CarritoRequest.Cliente }
	 * 
	 */
	public CarritoRequest.Cliente getCliente() {
		return cliente;
	}

	public CarritoRequest.DatosNps getDatosNPS() {
		return datosNPS;
	}

	public void setDatosNPS(CarritoRequest.DatosNps datosNPS) {
		this.datosNPS = datosNPS;
	}

	public boolean esRetiroEnTienda() {
		for (Articulo it : articulos.getArticulo()) {
			if (it.getCodigoRetiroCarsa() != null && !it.getCodigoRetiroCarsa().trim().isEmpty())
				return true;
		}
		return false;
	}
	
	@AssertTrue(message = "El Codigo Postal de los Datos de Envio no es valido. Debe tener 4 digitos o ser Retiro en Tienda.")
	private boolean isCodigoPostalDatosEnvioValido() {
		if (esRetiroEnTienda())
			return true;
		else {
			BigInteger cp = getCliente().getDatosEnvio().getCodigoPostal();
			return (cp != null && cp.intValue() >= 1000 && cp.intValue() <= 9999);
		}

	}

	/**
	 * Define el valor de la propiedad cliente.
	 * 
	 * @param value allowed object is {@link CarritoRequest.Cliente }
	 * 
	 */
	public void setCliente(CarritoRequest.Cliente value) {
		this.cliente = value;
	}

	/**
	 * Obtiene el valor de la propiedad articulos.
	 * 
	 * @return possible object is {@link CarritoRequest.Articulos }
	 * 
	 */
	public CarritoRequest.Articulos getArticulos() {
		return articulos;
	}

	/**
	 * Define el valor de la propiedad articulos.
	 * 
	 * @param value allowed object is {@link CarritoRequest.Articulos }
	 * 
	 */
	public void setArticulos(CarritoRequest.Articulos value) {
		this.articulos = value;
	}

	/**
	 * Obtiene el valor de la propiedad pagos.
	 * 
	 * @return possible object is {@link CarritoRequest.Pagos }
	 * 
	 */
	public CarritoRequest.Pagos getPagos() {
		return pagos;
	}

	/**
	 * Define el valor de la propiedad pagos.
	 * 
	 * @param value allowed object is {@link CarritoRequest.Pagos }
	 * 
	 */
	public void setPagos(CarritoRequest.Pagos value) {
		this.pagos = value;
	}

	/**
	 * <p>
	 * Clase Java para anonymous complex type.
	 * 
	 * <p>
	 * El siguiente fragmento de esquema especifica el contenido que se espera que
	 * haya en esta clase.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence maxOccurs="unbounded">
	 *         &lt;element name="articulo">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="conceptoVenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="codigoEDSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="codigoCARSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="codigoEMSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="precio" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
	 *                   &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
	 *                   &lt;element name="decripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "articulo" })
	public static class Articulos {

		@XmlElement(required = true)
		@NotNull
		@Valid
		protected List<CarritoRequest.Articulos.Articulo> articulo;

		/**
		 * Gets the value of the articulo property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the articulo property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getArticulo().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link CarritoRequest.Articulos.Articulo }
		 * 
		 * 
		 */
		public List<CarritoRequest.Articulos.Articulo> getArticulo() {
			if (articulo == null) {
				articulo = new ArrayList<CarritoRequest.Articulos.Articulo>();
			}
			return this.articulo;
		}

		/**
		 * <p>
		 * Clase Java para anonymous complex type.
		 * 
		 * <p>
		 * El siguiente fragmento de esquema especifica el contenido que se espera que
		 * haya en esta clase.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="conceptoVenta" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="codigoEDSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="codigoCARSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="codigoEMSA" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="precio" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
		 *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
		 *         &lt;element name="decripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "conceptoVenta", "codigoEDSA", "codigoCARSA", "codigoEMSA",
				"codigoRetiroCarsa", "codigoRetiroEmsa", "fechaEntregaComprometida", "cantidad", "precio", "importe",
				"descripcion" })
		public static class Articulo {

			@XmlElement(required = true)
			@Size(max = 3)
			protected String conceptoVenta;
			
			@XmlElement(required = true)
			@Digits(fraction = 0, integer = 13)
			@NotNull
			protected BigInteger codigoEDSA;
			
			@XmlElement(required = true)
			@NotNull
			protected BigInteger codigoCARSA;
			
			@XmlElement(required = true)
			@Digits(fraction = 0, integer = 30)
			protected BigInteger codigoEMSA;// WEBIEM
			
			@XmlElement(required = true)
			protected BigInteger cantidad;// WEBQTY
			
			@XmlElement(required = true)
			@Digits(fraction = 2, integer = 11)
			protected BigDecimal precio;// WEBPRE
			
			@XmlElement(required = true)
			@Digits(fraction = 2, integer = 11)
			protected BigDecimal importe;// WEBTOT
			
			@XmlElement(required = true)
			protected String descripcion;
			
			@Size(max = 10)
			protected String codigoRetiroCarsa;
			
			@Size(max = 10)
			protected String codigoRetiroEmsa;
			
			@XmlSchemaType(name = "date")
			protected XMLGregorianCalendar fechaEntregaComprometida;

			/**
			 * Obtiene el valor de la propiedad conceptoVenta.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getConceptoVenta() {
				return conceptoVenta;
			}

			/**
			 * Define el valor de la propiedad conceptoVenta.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setConceptoVenta(String value) {
				this.conceptoVenta = value;
			}

			/**
			 * Obtiene el valor de la propiedad codigoEDSA.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCodigoEDSA() {
				return codigoEDSA;
			}

			/**
			 * Define el valor de la propiedad codigoEDSA.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setCodigoEDSA(BigInteger value) {
				this.codigoEDSA = value;
			}

			/**
			 * Obtiene el valor de la propiedad codigoCARSA.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCodigoCARSA() {
				return codigoCARSA;
			}

			/**
			 * Define el valor de la propiedad codigoCARSA.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setCodigoCARSA(BigInteger value) {
				this.codigoCARSA = value;
			}

			/**
			 * Obtiene el valor de la propiedad codigoEMSA.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCodigoEMSA() {
				return codigoEMSA;
			}

			/**
			 * Define el valor de la propiedad codigoEMSA.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setCodigoEMSA(BigInteger value) {
				this.codigoEMSA = value;
			}

			/**
			 * Obtiene el valor de la propiedad cantidad.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCantidad() {
				return cantidad;
			}

			/**
			 * Define el valor de la propiedad cantidad.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setCantidad(BigInteger value) {
				this.cantidad = value;
			}

			/**
			 * Obtiene el valor de la propiedad precio.
			 * 
			 * @return possible object is {@link BigDecimal }
			 * 
			 */
			public BigDecimal getPrecio() {
				return precio;
			}

			/**
			 * Define el valor de la propiedad precio.
			 * 
			 * @param value allowed object is {@link BigDecimal }
			 * 
			 */
			public void setPrecio(BigDecimal value) {
				this.precio = value;
			}

			/**
			 * Obtiene el valor de la propiedad importe.
			 * 
			 * @return possible object is {@link BigDecimal }
			 * 
			 */
			public BigDecimal getImporte() {
				return importe;
			}

			/**
			 * Define el valor de la propiedad importe.
			 * 
			 * @param value allowed object is {@link BigDecimal }
			 * 
			 */
			public void setImporte(BigDecimal value) {
				this.importe = value;
			}

			/**
			 * Obtiene el valor de la propiedad decripcion.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getDescripcion() {
				return descripcion;
			}

			/**
			 * Define el valor de la propiedad decripcion.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setDescripcion(String value) {
				this.descripcion = value;
			}

			public String getCodigoRetiroCarsa() {
				return codigoRetiroCarsa;
			}

			public void setCodigoRetiroCarsa(String codigoRetiroCarsa) {
				this.codigoRetiroCarsa = codigoRetiroCarsa;
			}

			public XMLGregorianCalendar getFechaEntregaComprometida() {
				return fechaEntregaComprometida;
			}

			public void setFechaEntregaComprometida(XMLGregorianCalendar fechaEntregaComprometida) {
				this.fechaEntregaComprometida = fechaEntregaComprometida;
			}

		}

	}

	/**
	 * <p>
	 * Clase Java para anonymous complex type.
	 * 
	 * <p>
	 * El siguiente fragmento de esquema especifica el contenido que se espera que
	 * haya en esta clase.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="numerointerno" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *         &lt;element name="fechaPedio" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
	 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *         &lt;element name="importeTotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
	 *       &lt;/sequence>
	 *       &lt;attribute name="numeroCarro" use="required" type="{http://www.w3.org/2001/XMLSchema}integer" />
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "numerointerno", "verazEstado", "verazScore", "nroEventoEspecial", "fechaPedido",
			"moneda", "importeTotal", "numeroCarro", "idReserva" })
	public static class Carrito {

		@XmlElement(required = true)
		@NotNull
		protected Integer numerointerno;

		@XmlElement(required = true)
		protected VerEst verazEstado;

		// VerSco: Define el valor de score que retorno la validación de Veraz
		@XmlElement(required = true)
		protected String verazScore;

		// WebNrE: Define el número de Identificador Único en el sistema de
		// Eventos Especiales
		@XmlElement(required = true)
		protected String nroEventoEspecial;

		@XmlElement(required = true)
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar fechaPedido;
		
		@XmlElement(required = true, defaultValue = "ARS")
		@Size(max = 3)
		protected String moneda;

		@XmlElement(required = true)
		@Digits(fraction = 2, integer = 11)
		protected BigDecimal importeTotal;

		@XmlAttribute(name = "numeroCarro", required = true)
		@Pattern(regexp = "\\d{1,9}|[H|C|P|Y]\\d{9,9}", message = "patron numero de carro no valido")
		protected String numeroCarro;
		
		@XmlElement(required = false)
		protected String idReserva;

		public String getIdReserva() {
			return idReserva;
		}

		public void setIdReserva(String idReserva) {
			this.idReserva = idReserva;
		}

		/**
		 * Define el estado de la transacción ante la validación de Veraz
		 */
		@XmlType(name = "VerEst")
		@XmlEnum
		public enum VerEst {
			Error, Aprobado, Rechazado, NoRequerido, Bloqueado, NoContesta
		}

		/**
		 * Obtiene el valor de la propiedad numerointerno.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public Integer getNumerointerno() {
			return numerointerno;
		}

		/**
		 * Define el valor de la propiedad numerointerno.
		 * 
		 * @param value allowed object is {@link BigInteger }
		 * 
		 */
		public void setNumerointerno(Integer value) {
			this.numerointerno = value;
		}

		/**
		 * Obtiene el valor de la propiedad fechaPedio.
		 * 
		 * @return possible object is {@link XMLGregorianCalendar }
		 * 
		 */
		public XMLGregorianCalendar getFechaPedido() {
			return fechaPedido;
		}

		/**
		 * Define el valor de la propiedad fechaPedio.
		 * 
		 * @param value allowed object is {@link XMLGregorianCalendar }
		 * 
		 */
		public void setFechaPedido(XMLGregorianCalendar value) {
			this.fechaPedido = value;
		}

		/**
		 * Obtiene el valor de la propiedad moneda.
		 * 
		 * @return possible object is {@link String }
		 * 
		 */
		public String getMoneda() {
			return moneda;
		}

		/**
		 * Define el valor de la propiedad moneda.
		 * 
		 * @param value allowed object is {@link String }
		 * 
		 */
		public void setMoneda(String value) {
			this.moneda = value;
		}

		/**
		 * Obtiene el valor de la propiedad importeTotal.
		 * 
		 * @return possible object is {@link BigDecimal }
		 * 
		 */
		public BigDecimal getImporteTotal() {
			return importeTotal;
		}

		/**
		 * Define el valor de la propiedad importeTotal.
		 * 
		 * @param value allowed object is {@link BigDecimal }
		 * 
		 */
		public void setImporteTotal(BigDecimal value) {
			this.importeTotal = value;
		}

		/**
		 * Obtiene el valor de la propiedad numeroCarro.
		 * 
		 * @return possible object is {@link BigInteger }
		 * 
		 */
		public String getNumeroCarro() {
			return numeroCarro;
		}

		/**
		 * Define el valor de la propiedad numeroCarro.
		 * 
		 * @param value allowed object is {@link BigInteger }
		 * 
		 */
		public void setNumeroCarro(String value) {
			this.numeroCarro = value;
		}

		public VerEst getVerazEstado() {
			return verazEstado;
		}

		public void setVerazEstado(VerEst verazEstado) {
			this.verazEstado = verazEstado;
		}

		public String getVerazScore() {
			return verazScore;
		}

		public void setVerazScore(String verazScore) {
			this.verazScore = verazScore;
		}

		public String getNroEventoEspecial() {
			return nroEventoEspecial;
		}

		public void setNroEventoEspecial(String nroEventoEspecial) {
			this.nroEventoEspecial = nroEventoEspecial;
		}

	}

	/**
	 * <p>
	 * Clase Java para anonymous complex type.
	 * 
	 * <p>
	 * El siguiente fragmento de esquema especifica el contenido que se espera que
	 * haya en esta clase.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence>
	 *         &lt;element name="datosCliente">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="codigoDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="numeroDocumento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
	 *                   &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="domicilio">
	 *                     &lt;complexType>
	 *                       &lt;complexContent>
	 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                           &lt;sequence>
	 *                             &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                             &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                             &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                             &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                             &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                           &lt;/sequence>
	 *                         &lt;/restriction>
	 *                       &lt;/complexContent>
	 *                     &lt;/complexType>
	 *                   &lt;/element>
	 *                   &lt;element name="direccionMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="datosFacturacion">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *         &lt;element name="datosEnvio">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "datosCliente", "datosFacturacion", "datosEnvio" })
	public static class Cliente {

		@XmlElement(required = true)
		@Valid
		protected CarritoRequest.Cliente.DatosCliente datosCliente;
		@XmlElement(required = true)
		@Valid
		protected CarritoRequest.Cliente.DatosFacturacion datosFacturacion;
		@XmlElement(required = true)
		@Valid
		protected CarritoRequest.Cliente.DatosEnvio datosEnvio;

		/**
		 * Obtiene el valor de la propiedad datosCliente.
		 * 
		 * @return possible object is {@link CarritoRequest.Cliente.DatosCliente }
		 * 
		 */
		public CarritoRequest.Cliente.DatosCliente getDatosCliente() {
			return datosCliente;
		}

		/**
		 * Define el valor de la propiedad datosCliente.
		 * 
		 * @param value allowed object is {@link CarritoRequest.Cliente.DatosCliente }
		 * 
		 */
		public void setDatosCliente(CarritoRequest.Cliente.DatosCliente value) {
			this.datosCliente = value;
		}

		/**
		 * Obtiene el valor de la propiedad datosFacturacion.
		 * 
		 * @return possible object is {@link CarritoRequest.Cliente.DatosFacturacion }
		 * 
		 */
		public CarritoRequest.Cliente.DatosFacturacion getDatosFacturacion() {
			return datosFacturacion;
		}

		/**
		 * Define el valor de la propiedad datosFacturacion.
		 * 
		 * @param value allowed object is
		 *              {@link CarritoRequest.Cliente.DatosFacturacion }
		 * 
		 */
		public void setDatosFacturacion(CarritoRequest.Cliente.DatosFacturacion value) {
			this.datosFacturacion = value;
		}

		/**
		 * Obtiene el valor de la propiedad datosEnvio.
		 * 
		 * @return possible object is {@link CarritoRequest.Cliente.DatosEnvio }
		 * 
		 */
		public CarritoRequest.Cliente.DatosEnvio getDatosEnvio() {
			return datosEnvio;
		}

		/**
		 * Define el valor de la propiedad datosEnvio.
		 * 
		 * @param value allowed object is {@link CarritoRequest.Cliente.DatosEnvio }
		 * 
		 */
		public void setDatosEnvio(CarritoRequest.Cliente.DatosEnvio value) {
			this.datosEnvio = value;
		}

		/**
		 * <p>
		 * Clase Java para anonymous complex type.
		 * 
		 * <p>
		 * El siguiente fragmento de esquema especifica el contenido que se espera que
		 * haya en esta clase.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="codigoDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="numeroDocumento" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
		 *         &lt;element name="sexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="apellido" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="domicilio">
		 *           &lt;complexType>
		 *             &lt;complexContent>
		 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *                 &lt;sequence>
		 *                   &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *                   &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *                   &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                   &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *                   &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *                 &lt;/sequence>
		 *               &lt;/restriction>
		 *             &lt;/complexContent>
		 *           &lt;/complexType>
		 *         &lt;/element>
		 *         &lt;element name="direccionMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "codigoDocumento", "numeroDocumento", "fechaNacimiento", "sexo", "apellido",
				"nombre", "domicilio", "direccionMail", "telefono" })
		public static class DatosCliente {

			@XmlElement(required = true, defaultValue = "DN")
			@Size(max = 2)
			protected String codigoDocumento;
			
			@XmlElement(required = true)
			@NotNull
			protected BigInteger numeroDocumento;
			
			@XmlElement(required = true)
			@XmlSchemaType(name = "date")
			protected XMLGregorianCalendar fechaNacimiento;
			
			@XmlElement(required = true)
			@Size(max = 1)
			@NotNull
			protected String sexo;
			
			@XmlElement(required = true)
			@Size(max = 50)
			@NotNull
			protected String apellido;
			
			@XmlElement(required = true)
			@Size(max = 50)
			@NotNull
			protected String nombre;
			
			@XmlElement(required = true)
			@Valid
			protected CarritoRequest.Cliente.DatosCliente.Domicilio domicilio;
			
			@XmlElement(required = true)
			@Size(max = 40)
			protected String direccionMail;
			
			@XmlElement(required = true)
			@Size(max = 30)
			protected String telefono;

			/**
			 * Obtiene el valor de la propiedad codigoDocumento.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCodigoDocumento() {
				return codigoDocumento;
			}

			/**
			 * Define el valor de la propiedad codigoDocumento.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setCodigoDocumento(String value) {
				this.codigoDocumento = value;
			}

			/**
			 * Obtiene el valor de la propiedad numeroDocumento.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getNumeroDocumento() {
				return numeroDocumento;
			}

			/**
			 * Define el valor de la propiedad numeroDocumento.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setNumeroDocumento(BigInteger value) {
				this.numeroDocumento = value;
			}

			/**
			 * Obtiene el valor de la propiedad fechaNacimiento.
			 * 
			 * @return possible object is {@link XMLGregorianCalendar }
			 * 
			 */
			public XMLGregorianCalendar getFechaNacimiento() {
				return fechaNacimiento;
			}

			/**
			 * Define el valor de la propiedad fechaNacimiento.
			 * 
			 * @param value allowed object is {@link XMLGregorianCalendar }
			 * 
			 */
			public void setFechaNacimiento(XMLGregorianCalendar value) {
				this.fechaNacimiento = value;
			}

			/**
			 * Obtiene el valor de la propiedad sexo.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getSexo() {
				return sexo;
			}

			/**
			 * Define el valor de la propiedad sexo.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setSexo(String value) {
				this.sexo = value;
			}

			/**
			 * Obtiene el valor de la propiedad apellido.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getApellido() {
				return apellido;
			}

			/**
			 * Define el valor de la propiedad apellido.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setApellido(String value) {
				this.apellido = value;
			}

			/**
			 * Obtiene el valor de la propiedad nombre.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getNombre() {
				return nombre;
			}

			/**
			 * Define el valor de la propiedad nombre.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setNombre(String value) {
				this.nombre = value;
			}

			/**
			 * Obtiene el valor de la propiedad domicilio.
			 * 
			 * @return possible object is
			 *         {@link CarritoRequest.Cliente.DatosCliente.Domicilio }
			 * 
			 */
			public CarritoRequest.Cliente.DatosCliente.Domicilio getDomicilio() {
				return domicilio;
			}

			/**
			 * Define el valor de la propiedad domicilio.
			 * 
			 * @param value allowed object is
			 *              {@link CarritoRequest.Cliente.DatosCliente.Domicilio }
			 * 
			 */
			public void setDomicilio(CarritoRequest.Cliente.DatosCliente.Domicilio value) {
				this.domicilio = value;
			}

			/**
			 * Obtiene el valor de la propiedad direccionMail.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getDireccionMail() {
				return direccionMail;
			}

			/**
			 * Define el valor de la propiedad direccionMail.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setDireccionMail(String value) {
				this.direccionMail = value;
			}

			/**
			 * Obtiene el valor de la propiedad telefono.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getTelefono() {
				return telefono;
			}

			/**
			 * Define el valor de la propiedad telefono.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setTelefono(String value) {
				this.telefono = value;
			}

			/**
			 * <p>
			 * Clase Java para anonymous complex type.
			 * 
			 * <p>
			 * El siguiente fragmento de esquema especifica el contenido que se espera que
			 * haya en esta clase.
			 * 
			 * <pre>
			 * &lt;complexType>
			 *   &lt;complexContent>
			 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
			 *       &lt;sequence>
			 *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
			 *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
			 *         &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
			 *         &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
			 *       &lt;/sequence>
			 *     &lt;/restriction>
			 *   &lt;/complexContent>
			 * &lt;/complexType>
			 * </pre>
			 * 
			 * 
			 */
			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "calle", "numero", "piso", "departamento", "localidad", "provincia",
					"codigoPostal", "cpa" })
			public static class Domicilio {

				@XmlElement(required = true)
				@Size(max = 50)
				protected String calle;// WEBCAR
				
				@XmlElement(required = true)
				@Size(max = 50)
				protected String numero;// WEBNUR
				
				@XmlElement(required = true, nillable = true)
				@Size(max = 2)
				protected String piso;// WEBPIR
				
				@XmlElement(required = true)
				@Size(max = 5)
				protected String departamento;// WEBDER
				
				@XmlElement(required = true)
				@Size(max = 40)
				protected String localidad;// WEBBAR
				
				@XmlElement(required = true)
				@Size(max = 30)
				protected String provincia;// WEBPRR
				
				@XmlElement(required = true)
				@Digits(fraction = 0, integer = 4)
				@Min(1000)
				@Max(9999)
				protected BigInteger codigoPostal;// WEBCPR
				
				@XmlElement(required = true)
				@Size(max = 10)
				protected String cpa;// WEBCXR

				/**
				 * Obtiene el valor de la propiedad calle.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getCalle() {
					return calle;
				}

				/**
				 * Define el valor de la propiedad calle.
				 * 
				 * @param value allowed object is {@link String }
				 * 
				 */
				public void setCalle(String value) {
					this.calle = value;
				}

				/**
				 * Obtiene el valor de la propiedad numero.
				 * 
				 * @return possible object is {@link BigInteger }
				 * 
				 */
				public String getNumero() {
					return numero;
				}

				/**
				 * Define el valor de la propiedad numero.
				 * 
				 * @param value allowed object is {@link BigInteger }
				 * 
				 */
				public void setNumero(String value) {
					this.numero = value;
				}

				/**
				 * Obtiene el valor de la propiedad piso.
				 * 
				 * @return possible object is {@link BigInteger }
				 * 
				 */
				public String getPiso() {
					return piso;
				}

				/**
				 * Define el valor de la propiedad piso.
				 * 
				 * @param value allowed object is {@link BigInteger }
				 * 
				 */
				public void setPiso(String value) {
					this.piso = value;
				}

				/**
				 * Obtiene el valor de la propiedad departamento.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getDepartamento() {
					return departamento;
				}

				/**
				 * Define el valor de la propiedad departamento.
				 * 
				 * @param value allowed object is {@link String }
				 * 
				 */
				public void setDepartamento(String value) {
					this.departamento = value;
				}

				/**
				 * Obtiene el valor de la propiedad localidad.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getLocalidad() {
					return localidad;
				}

				/**
				 * Define el valor de la propiedad localidad.
				 * 
				 * @param value allowed object is {@link String }
				 * 
				 */
				public void setLocalidad(String value) {
					this.localidad = value;
				}

				/**
				 * Obtiene el valor de la propiedad provincia.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getProvincia() {
					return provincia;
				}

				/**
				 * Define el valor de la propiedad provincia.
				 * 
				 * @param value allowed object is {@link String }
				 * 
				 */
				public void setProvincia(String value) {
					this.provincia = value;
				}

				/**
				 * Obtiene el valor de la propiedad codigoPostal.
				 * 
				 * @return possible object is {@link BigInteger }
				 * 
				 */
				public BigInteger getCodigoPostal() {
					return codigoPostal;
				}

				/**
				 * Define el valor de la propiedad codigoPostal.
				 * 
				 * @param value allowed object is {@link BigInteger }
				 * 
				 */
				public void setCodigoPostal(BigInteger value) {
					this.codigoPostal = value;
				}

				/**
				 * Obtiene el valor de la propiedad cpa.
				 * 
				 * @return possible object is {@link String }
				 * 
				 */
				public String getCpa() {
					return cpa;
				}

				/**
				 * Define el valor de la propiedad cpa.
				 * 
				 * @param value allowed object is {@link String }
				 * 
				 */
				public void setCpa(String value) {
					this.cpa = value;
				}

			}

		}

		/**
		 * <p>
		 * Clase Java para anonymous complex type.
		 * 
		 * <p>
		 * El siguiente fragmento de esquema especifica el contenido que se espera que
		 * haya en esta clase.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "calle", "numero", "piso", "departamento", "localidad", "provincia",
				"codigoPostal", "latitud", "longitud", "cpa", "merlinnormalizado", "merlinid", "fraude","observaciones" })
		public static class DatosEnvio {

			@XmlElement(required = true)
			@NotNull
			@Size(max = 50)
			protected String calle;// FWEBCAF

			@XmlElement(required = true)
			@NotEmpty
			@Size(max = 50)
			protected String numero;// FEWEBNUF

			@XmlElement(required = true)
			@Size(max = 2)
			protected String piso;// FWEBPIF

			@XmlElement(required = true)
			@Size(max = 5)
			protected String departamento;// FWEBDEF

			@XmlElement(required = true)
			@Size(max = 40)
			@NotNull
			protected String localidad;// FWEBBAF

			@XmlElement(required = true)
			@Size(max = 30)
			@NotNull
			protected String provincia;// FWEBPRF

			@XmlElement(required = true)
			@Digits(integer = 4, fraction = 0)
			//Se valida también con la función isCodigoPostalDatosEnvioValido()
			protected BigInteger codigoPostal;// FWEBCPF

			@XmlElement(required = true)
			@Size(max = 10)
			protected String cpa;// FWEBCXF
			
			@XmlElement(required = false)
			@Size(max = 30)
			protected String latitud;// latitud
			
			@XmlElement(required = false)
			@Size(max = 30)
			protected String longitud;// longitud
			
			@XmlElement(required = false)
			@Size(max = 30)
			protected String merlinnormalizado;// merlinnormalizado
			
			@XmlElement(required = false)
			@Size(max = 30)
			protected String merlinid;// merlinid
			
			@XmlElement(required = false)
			@Min(0)
			@Max(1)
			protected Integer fraude;
			
			@XmlElement(required = false)
			@Size(max = 200)
			protected String observaciones;

			/**
			 * Obtiene el valor de la propiedad calle.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCalle() {
				return calle;
			}

			/**
			 * Define el valor de la propiedad calle.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setCalle(String value) {
				this.calle = value;
			}

			/**
			 * Obtiene el valor de la propiedad numero.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public String getNumero() {
				return numero;
			}

			/**
			 * Define el valor de la propiedad numero.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setNumero(String value) {
				this.numero = value;
			}

			/**
			 * Obtiene el valor de la propiedad piso.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public String getPiso() {
				return piso;
			}

			/**
			 * Define el valor de la propiedad piso.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setPiso(String value) {
				this.piso = value;
			}

			/**
			 * Obtiene el valor de la propiedad departamento.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getDepartamento() {
				return departamento;
			}

			/**
			 * Define el valor de la propiedad departamento.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setDepartamento(String value) {
				this.departamento = value;
			}

			/**
			 * Obtiene el valor de la propiedad localidad.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getLocalidad() {
				return localidad;
			}

			/**
			 * Define el valor de la propiedad localidad.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setLocalidad(String value) {
				this.localidad = value;
			}

			/**
			 * Obtiene el valor de la propiedad provincia.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getProvincia() {
				return provincia;
			}

			/**
			 * Define el valor de la propiedad provincia.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setProvincia(String value) {
				this.provincia = value;
			}

			/**
			 * Obtiene el valor de la propiedad codigoPostal.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCodigoPostal() {
				return codigoPostal;
			}

			/**
			 * Define el valor de la propiedad codigoPostal.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setCodigoPostal(BigInteger value) {
				this.codigoPostal = value;
			}

			/**
			 * Obtiene el valor de la propiedad cpa.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCpa() {
				return cpa;
			}

			/**
			 * Define el valor de la propiedad cpa.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setCpa(String value) {
				this.cpa = value;
			}

			public Integer getFraude() {
				return fraude;
			}

			public void setFraude(Integer fraude) {
				this.fraude = fraude;
			}

			public String getLatitud() {
				return latitud;
			}

			public void setLatitud(String latitud) {
				this.latitud = latitud;
			}

			public String getLongitud() {
				return longitud;
			}

			public void setLongitud(String longitud) {
				this.longitud = longitud;
			}

			public String getMerlinnormalizado() {
				return merlinnormalizado;
			}

			public void setMerlinnormalizado(String merlinnormalizado) {
				this.merlinnormalizado = merlinnormalizado;
			}

			public String getMerlinid() {
				return merlinid;
			}

			public void setMerlinid(String merlinid) {
				this.merlinid = merlinid;
			}
			
			public String getObservaciones() {
				return observaciones;
			}

			public void setObservaciones(String observaciones) {
				this.observaciones = observaciones;
			}

		}

		/**
		 * <p>
		 * Clase Java para anonymous complex type.
		 * 
		 * <p>
		 * El siguiente fragmento de esquema especifica el contenido que se espera que
		 * haya en esta clase.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="calle" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="piso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="departamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="localidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="provincia" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="codigoPostal" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="cpa" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "calle", "numero", "piso", "departamento", "localidad", "provincia",
				"codigoPostal", "cpa", "fraude", "telefono","observaciones"  })
		public static class DatosFacturacion {

			@XmlElement(required = true)
			@NotNull
			@Size(max = 50)
			protected String calle;// WEBCAF

			@XmlElement(required = true)
			@NotEmpty
			@Size(max = 50)
			protected String numero;// WEBNUF

			@XmlElement(required = true)
			@Size(max = 2)
			protected String piso;// WEBPIF

			@XmlElement(required = true)
			@Size(max = 5)
			protected String departamento;// WEBDEF

			@XmlElement(required = true)
			@Size(max = 40)
			protected String localidad;// WEBBAF

			@XmlElement(required = true)
			@Size(max = 30)
			protected String provincia;// WEBPRF

			@XmlElement(required = true)
			@Digits(fraction = 0, integer = 4)
			@Min(1000)
			@Max(9999)
			protected BigInteger codigoPostal;// WEBCPF

			@XmlElement(required = true)
			@Size(max = 10)
			protected String cpa;// WEBCXF
			
			@XmlElement(required = false)
			@Min(0)
			@Max(1)
			protected Integer fraude;
			
			//@XmlElement(required = true)
			@Size(max = 30)
			protected String telefono;// WEBTELF

			@XmlElement(required = false)
			@Size(max = 200)
			protected String observaciones;

			/**
			 * Obtiene el valor de la propiedad calle.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCalle() {
				return calle;
			}

			/**
			 * Define el valor de la propiedad calle.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setCalle(String value) {
				this.calle = value;
			}

			/**
			 * Obtiene el valor de la propiedad numero.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public String getNumero() {
				return numero;
			}

			/**
			 * Define el valor de la propiedad numero.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setNumero(String value) {
				this.numero = value;
			}

			/**
			 * Obtiene el valor de la propiedad piso.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public String getPiso() {
				return piso;
			}

			/**
			 * Define el valor de la propiedad piso.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setPiso(String value) {
				this.piso = value;
			}

			/**
			 * Obtiene el valor de la propiedad departamento.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getDepartamento() {
				return departamento;
			}

			/**
			 * Define el valor de la propiedad departamento.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setDepartamento(String value) {
				this.departamento = value;
			}

			/**
			 * Obtiene el valor de la propiedad localidad.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getLocalidad() {
				return localidad;
			}

			/**
			 * Define el valor de la propiedad localidad.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setLocalidad(String value) {
				this.localidad = value;
			}

			/**
			 * Obtiene el valor de la propiedad provincia.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getProvincia() {
				return provincia;
			}

			/**
			 * Define el valor de la propiedad provincia.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setProvincia(String value) {
				this.provincia = value;
			}

			/**
			 * Obtiene el valor de la propiedad codigoPostal.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCodigoPostal() {
				return codigoPostal;
			}

			/**
			 * Define el valor de la propiedad codigoPostal.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setCodigoPostal(BigInteger value) {
				this.codigoPostal = value;
			}

			/**
			 * Obtiene el valor de la propiedad cpa.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCpa() {
				return cpa;
			}

			/**
			 * Define el valor de la propiedad cpa.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setCpa(String value) {
				this.cpa = value;
			}

			public String getTelefono() {
				return telefono;
			}

			public void setTelefono(String telefono) {
				this.telefono = telefono;
			}

			public Integer getFraude() {
				return fraude;
			}

			public void setFraude(Integer fraude) {
				this.fraude = fraude;
			}
			
			public String getObservaciones() {
				return observaciones;
			}

			public void setObservaciones(String observaciones) {
				this.observaciones = observaciones;
			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "calle", "numeroCalle", "tipoDocumento", "numeroDocumento", "fechaNacimiento",
			"validacionDireccion", "validacionTipoDocumento", "validacionNumeroDocumento",
			"validacionFechaNacimiento" })
	public static class DatosNps {
		@XmlElement(required = true)
		@NotNull
		@Size(max = 100)
		protected String calle;

		@XmlElement(required = true)
		@NotEmpty
		@Size(max = 5)
		protected String numeroCalle;

		@XmlElement(required = true)
		@Size(max = 5)
		protected String tipoDocumento;

		@XmlElement(required = true)
		@Size(max = 10)
		protected String numeroDocumento;

		@XmlElement(required = true)
		@XmlSchemaType(name = "date")
		protected XMLGregorianCalendar fechaNacimiento;

		@XmlElement(required = true)
		@Size(max = 1)
		protected String validacionDireccion;

		@XmlElement(required = true)
		@Size(max = 1)
		protected String validacionTipoDocumento;

		@XmlElement(required = true)
		@Size(max = 1)
		protected String validacionNumeroDocumento;

		@XmlElement(required = true)
		@Size(max = 1)
		protected String validacionFechaNacimiento;

		public String getCalle() {
			return calle;
		}

		public void setCalle(String calle) {
			this.calle = calle;
		}

		public String getNumeroCalle() {
			return numeroCalle;
		}

		public void setNumeroCalle(String numeroCalle) {
			this.numeroCalle = numeroCalle;
		}

		public String getTipoDocumento() {
			return tipoDocumento;
		}

		public void setTipoDocumento(String tipoDocumento) {
			this.tipoDocumento = tipoDocumento;
		}

		public String getNumeroDocumento() {
			return numeroDocumento;
		}

		public void setNumeroDocumento(String numeroDocumento) {
			this.numeroDocumento = numeroDocumento;
		}

		public XMLGregorianCalendar getFechaNacimiento() {
			return fechaNacimiento;
		}

		public void setFechaNacimiento(XMLGregorianCalendar fechaNacimiento) {
			this.fechaNacimiento = fechaNacimiento;
		}

		public String getValidacionDireccion() {
			return validacionDireccion;
		}

		public void setValidacionDireccion(String validacionDireccion) {
			this.validacionDireccion = validacionDireccion;
		}

		public String getValidacionTipoDocumento() {
			return validacionTipoDocumento;
		}

		public void setValidacionTipoDocumento(String validacionTipoDocumento) {
			this.validacionTipoDocumento = validacionTipoDocumento;
		}

		public String getValidacionNumeroDocumento() {
			return validacionNumeroDocumento;
		}

		public void setValidacionNumeroDocumento(String validacionNumeroDocumento) {
			this.validacionNumeroDocumento = validacionNumeroDocumento;
		}

		public String getValidacionFechaNacimiento() {
			return validacionFechaNacimiento;
		}

		public void setValidacionFechaNacimiento(String validacionFechaNacimiento) {
			this.validacionFechaNacimiento = validacionFechaNacimiento;
		}

	}

	/**
	 * <p>
	 * Clase Java para anonymous complex type.
	 * 
	 * <p>
	 * El siguiente fragmento de esquema especifica el contenido que se espera que
	 * haya en esta clase.
	 * 
	 * <pre>
	 * &lt;complexType>
	 *   &lt;complexContent>
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *       &lt;sequence maxOccurs="unbounded">
	 *         &lt;element name="pago">
	 *           &lt;complexType>
	 *             &lt;complexContent>
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
	 *                 &lt;sequence>
	 *                   &lt;element name="producto" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="cuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="importeCupon" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
	 *                   &lt;element name="voucherID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="codigoAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="merchantID" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="lote" type="{http://www.w3.org/2001/XMLSchema}integer"/>
	 *                   &lt;element name="descripcon" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                   &lt;element name="descuento" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
	 *                   &lt;element name="npsstatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
	 *                 &lt;/sequence>
	 *               &lt;/restriction>
	 *             &lt;/complexContent>
	 *           &lt;/complexType>
	 *         &lt;/element>
	 *       &lt;/sequence>
	 *     &lt;/restriction>
	 *   &lt;/complexContent>
	 * &lt;/complexType>
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "pago" })
	public static class Pagos {

		@XmlElement(required = true)
		@Valid
		protected List<CarritoRequest.Pagos.Pago> pago;

		/**
		 * Gets the value of the pago property.
		 * 
		 * <p>
		 * This accessor method returns a reference to the live list, not a snapshot.
		 * Therefore any modification you make to the returned list will be present
		 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
		 * for the pago property.
		 * 
		 * <p>
		 * For example, to add a new item, do as follows:
		 * 
		 * <pre>
		 * getPago().add(newItem);
		 * </pre>
		 * 
		 * 
		 * <p>
		 * Objects of the following type(s) are allowed in the list
		 * {@link CarritoRequest.Pagos.Pago }
		 * 
		 * 
		 */
		public List<CarritoRequest.Pagos.Pago> getPago() {
			if (pago == null) {
				pago = new ArrayList<CarritoRequest.Pagos.Pago>();
			}
			return this.pago;
		}

		/**
		 * <p>
		 * Clase Java para anonymous complex type.
		 * 
		 * <p>
		 * El siguiente fragmento de esquema especifica el contenido que se espera que
		 * haya en esta clase.
		 * 
		 * <pre>
		 * &lt;complexType>
		 *   &lt;complexContent>
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
		 *       &lt;sequence>
		 *         &lt;element name="producto" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="cuotas" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="moneda" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="importeCupon" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
		 *         &lt;element name="voucherID" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="codigoAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="merchantID" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="lote" type="{http://www.w3.org/2001/XMLSchema}integer"/>
		 *         &lt;element name="descripcon" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *         &lt;element name="descuento" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
		 *         &lt;element name="npsstatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
		 *       &lt;/sequence>
		 *     &lt;/restriction>
		 *   &lt;/complexContent>
		 * &lt;/complexType>
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "producto", "codigo", "estado", "cuotas", "moneda", "importeCupon",
				"voucherID", "numeroTarjeta", "codigoAutorizacion", "merchantID", "lote", "descripcion", "descuento",
				"npsstatus", "apellidoTitular", "nombreTitular", "numeroDocumentoTitular", "tipoDocumentoTitular",
				"modoPago" })
		public static class Pago {

			public static String MEDIO_PAGO_CREDITO_MUSIMUNDO = "CM";

			@XmlElement(required = true)
			@Size(max = 25)
			protected String producto;// WEBPRO

			@XmlElement(required = true)
			@Size(max = 2)
			protected String codigo;// WEBTC

			@XmlElement(required = true)
			protected WebEst estado;// ESTADO
			@XmlElement(required = true)
			protected BigInteger cuotas; // WEBCUO
			@XmlElement(required = true, defaultValue = "ARS")
			@Size(max = 3)
			protected String moneda;// WEBMDA
			@XmlElement(required = true)
			@Digits(fraction = 2, integer = 11)
			protected BigDecimal importeCupon;// WEBTOT
			@XmlElement(required = true)
			@Digits(fraction = 0, integer = 5)
			protected BigInteger voucherID;// WEBCUP
			@XmlElement(required = true)
			@Size(max = 11)
			protected String numeroTarjeta;// WEBNTC
			@Size(max = 256)
			protected String apellidoTitular; // webapet
			@Size(max = 256)
			protected String nombreTitular; // webnomt
			@Size(max = 256)
			protected String numeroDocumentoTitular; // webndot
			@Size(max = 10)
			protected String tipoDocumentoTitular; // webnomt
			@XmlElement(required = true)
			@Size(max = 8)
			protected String codigoAutorizacion;// WEBAUT
			@XmlElement(required = true)
			@Size(max = 20)
			protected String merchantID;// WEBMID
			@XmlElement(required = true)
			@Digits(fraction = 0, integer = 5)
			protected BigInteger lote;// WEBLOT

			@XmlElement(required = true)
			@Size(max = 60)
			protected String descripcion;// WEBPR

			@XmlElement(required = true)
			@Digits(fraction = 2, integer = 6)
			protected BigDecimal descuento;// WEBDTO
			// @XmlElement(required = true)
			@Size(max = 15)
			protected String npsstatus;// ESTADO_NPS
			// @XmlElement(required = true)
			@Size(max = 15)
			protected String modoPago;// no mapeado

			public String getModoPago() {
				return modoPago;
			}

			public void setModoPago(String modoPago) {
				this.modoPago = modoPago;
			}

			/**
			 * Obtiene el valor de la propiedad producto.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getProducto() {
				return producto;
			}

			/**
			 * Define el valor de la propiedad producto.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setProducto(String value) {
				this.producto = value;
			}

			/**
			 * Obtiene el valor de la propiedad codigo.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCodigo() {
				return codigo;
			}

			/**
			 * Define el valor de la propiedad codigo.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setCodigo(String value) {
				this.codigo = value;
			}

			/**
			 * Obtiene el valor de la propiedad cuotas.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getCuotas() {
				return cuotas;
			}

			/**
			 * Define el valor de la propiedad cuotas.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setCuotas(BigInteger value) {
				this.cuotas = value;
			}

			/**
			 * Obtiene el valor de la propiedad moneda.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getMoneda() {
				return moneda;
			}

			/**
			 * Define el valor de la propiedad moneda.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setMoneda(String value) {
				this.moneda = value;
			}

			/**
			 * Obtiene el valor de la propiedad importeCupon.
			 * 
			 * @return possible object is {@link BigDecimal }
			 * 
			 */
			public BigDecimal getImporteCupon() {
				return importeCupon;
			}

			/**
			 * Define el valor de la propiedad importeCupon.
			 * 
			 * @param value allowed object is {@link BigDecimal }
			 * 
			 */
			public void setImporteCupon(BigDecimal value) {
				this.importeCupon = value;
			}

			/**
			 * Obtiene el valor de la propiedad voucherID.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getVoucherID() {
				return voucherID;
			}

			/**
			 * Define el valor de la propiedad voucherID.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setVoucherID(BigInteger value) {
				this.voucherID = value;
			}

			/**
			 * Obtiene el valor de la propiedad numeroTarjeta.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getNumeroTarjeta() {
				return numeroTarjeta;
			}

			/**
			 * Define el valor de la propiedad numeroTarjeta.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setNumeroTarjeta(String value) {
				this.numeroTarjeta = value;
			}

			/**
			 * Obtiene el valor de la propiedad codigoAutorizacion.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getCodigoAutorizacion() {
				return codigoAutorizacion;
			}

			/**
			 * Define el valor de la propiedad codigoAutorizacion.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setCodigoAutorizacion(String value) {
				this.codigoAutorizacion = value;
			}

			/**
			 * Obtiene el valor de la propiedad merchantID.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getMerchantID() {
				return merchantID;
			}

			/**
			 * Define el valor de la propiedad merchantID.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setMerchantID(String value) {
				this.merchantID = value;
			}

			/**
			 * Obtiene el valor de la propiedad lote.
			 * 
			 * @return possible object is {@link BigInteger }
			 * 
			 */
			public BigInteger getLote() {
				return lote;
			}

			/**
			 * Define el valor de la propiedad lote.
			 * 
			 * @param value allowed object is {@link BigInteger }
			 * 
			 */
			public void setLote(BigInteger value) {
				this.lote = value;
			}

			/**
			 * Obtiene el valor de la propiedad descripcon.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getDescripcon() {
				return descripcion;
			}

			/**
			 * Define el valor de la propiedad descripcon.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setDescripcon(String value) {
				this.descripcion = value;
			}

			/**
			 * Obtiene el valor de la propiedad descuento.
			 * 
			 * @return possible object is {@link BigDecimal }
			 * 
			 */
			public BigDecimal getDescuento() {
				return descuento;
			}

			/**
			 * Define el valor de la propiedad descuento.
			 * 
			 * @param value allowed object is {@link BigDecimal }
			 * 
			 */
			public void setDescuento(BigDecimal value) {
				this.descuento = value;
			}

			/**
			 * Obtiene el valor de la propiedad npsstatus.
			 * 
			 * @return possible object is {@link String }
			 * 
			 */
			public String getNpsstatus() {
				return npsstatus;
			}

			/**
			 * Define el valor de la propiedad npsstatus.
			 * 
			 * @param value allowed object is {@link String }
			 * 
			 */
			public void setNpsstatus(String value) {
				this.npsstatus = value;
			}

			public WebEst getEstado() {
				return estado;
			}

			public void setEstado(WebEst estado) {
				this.estado = estado;
			}

			public String getDescripcion() {
				return descripcion;
			}

			public void setDescripcion(String descripcion) {
				this.descripcion = descripcion;
			}

			public String getApellidoTitular() {
				return apellidoTitular;
			}

			public void setApellidoTitular(String apellidoTitular) {
				this.apellidoTitular = apellidoTitular;
			}

			public String getNombreTitular() {
				return nombreTitular;
			}

			public void setNombreTitular(String nombreTitular) {
				this.nombreTitular = nombreTitular;
			}

			public String getNumeroDocumentoTitular() {
				return numeroDocumentoTitular;
			}

			public void setNumeroDocumentoTitular(String numeroDocumentoTitular) {
				this.numeroDocumentoTitular = numeroDocumentoTitular;
			}

			public String getTipoDocumentoTitular() {
				return tipoDocumentoTitular;
			}

			public void setTipoDocumentoTitular(String tipoDocumentoTitular) {
				this.tipoDocumentoTitular = tipoDocumentoTitular;
			}

			@XmlType(name = "WebEst")
			@XmlEnum
			public enum WebEst {
				Pendiente, Aprobado, Cancelado
			}

		}

	}

	public Boolean isHybris() {
		return 'H' == this.getCarrito().getNumeroCarro().toString().charAt(0);
	}

}
