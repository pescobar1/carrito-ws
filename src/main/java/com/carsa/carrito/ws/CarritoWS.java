package com.carsa.carrito.ws;

import javax.jws.WebService;

import com.carsa.carrito.arrepentido.ArrepentidoDTO;
import com.carsa.carrito.arrepentido.ArrepentidoRequest;
import com.carsa.carrito.domain.EstadoMerlin;
import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.oportunidad.OportunidadCarro;
import com.carsa.domain.exception.http.BadRequestException;
import com.carsa.domain.utils.FiltroFacturadorAutomatico;
import com.carsa.exceptions.ConvertirCarritoException;
import com.carsa.exceptions.SaveCarritoException;
import com.carsa.exceptions.ValidacionException;

@WebService
public interface CarritoWS {
	public boolean saveCarrito(CarritoRequest carrito) throws ValidacionException, SaveCarritoException;
	public boolean validarCarrito(CarritoRequest carrito) throws ValidacionException;
	public String carritoRequestToXML(CarritoRequest carrito) throws ConvertirCarritoException;
	public WebH saveCarritoRequest(CarritoRequest carrito,EstadoMerlin estadoMerlin) throws Exception;
	public Boolean actualizarEstadoClienteError(String nroCarrito);
	public Boolean carritoEnEstado(String nroCarrito,String estado);
	public Boolean actualizarEstadoCarritoPendienteOrdenCrm(String nroCarrito);
	public Boolean actualizarEstadoCarritoPendiente(String nroCarrito);
	public OportunidadCarro getOportunidadRequest(WebH webh);
	public WebH updateOportunidad(WebH webh,OportunidadCarro oportunidadCarro);
	public FiltroFacturadorAutomatico crearFiltroAutomatico(String nroCarritoS);
	public Boolean isPendienteOrdenCrm(String nroCarrito);
	public Boolean actualizarEstadoCarritoPendienteAutomatico(String nroCarrito,String nroOrden);
	public Boolean actualizarNroOrdenCrmCarrito(String nroCarritoS, String nroFactura, String nroOrdenCrm);
	public String obtenerNroOrdenCrmCarrito(String nroFactura) throws BadRequestException;
	public ArrepentidoDTO obtenerArrepentidoDTO(ArrepentidoRequest request) throws BadRequestException;
}
