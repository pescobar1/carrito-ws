package com.carsa.carrito.ws;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Formatter;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.carsa.carrito.arrepentido.ArrepentidoDTO;
import com.carsa.carrito.arrepentido.ArrepentidoRequest;
import com.carsa.carrito.oportunidad.DetalleItems;
import com.carsa.carrito.oportunidad.OportunidadCarro;
import com.carsa.carrito.service.ProductoComboService;
import com.carsa.carrito.tools.Converter;
import com.carsa.carrito.tools.Soporte;
import com.carsa.carrito.ws.CarritoRequest.Articulos.Articulo;
import com.carsa.carrito.ws.CarritoRequest.Pagos.Pago;
import com.carsa.domain.exception.http.BadRequestException;
import com.carsa.carrito.domain.EstadoMerlin;
import com.carsa.carrito.domain.MerlinNormalizadoMcom;
import com.carsa.carrito.domain.ProductoCombo;
import com.carsa.carrito.domain.WebCli;
import com.carsa.carrito.domain.WebD;
import com.carsa.carrito.domain.WebH;
import com.carsa.carrito.domain.WebNps;
import com.carsa.carrito.domain.WebP;
import com.carsa.carrito.service.WebHService;
import com.carsa.domain.utils.FiltroFacturadorAutomatico;
import com.carsa.exceptions.ConvertirCarritoException;
import com.carsa.exceptions.SaveCarritoException;
import com.carsa.exceptions.ValidacionException;

@Service
public class CarritoWSImpl implements CarritoWS {
	
	private static final Logger LOG = LogManager.getLogger(CarritoWSImpl.class);
	
	
	private static final String CODIGO_BONIFICACION = "9999900090901";
	private static String QA="qa";

	@Autowired
	private WebHService webHService;
	
	@Autowired
	private ProductoComboService productoComboService;
	
	private Integer puntoVentaAutomatico;
	
	@Autowired
	private Converter converter;
	
	
	@Value("${facturacion.automatica.habilitado}")
	private Boolean habilitarFacturacionAutomatica;
	
	@Value("${facturacion.automatica.score.mayorIgual}")
	private Integer scoreMayorIgual;
	
	@Value("#{'${venta.verde.rango.producto}'.split(',')}")
	private List<Integer> rangoProductoVentaVerde;
	
	@Value("#{'${venta.consignacion.rango.producto}'.split(',')}")
	private List<Integer> rangoProductoVentaConsignacion;
	
	private String enviroment;

	
	public WebHService getWebHService() {
		return webHService;
	}

	public void setWebHService(WebHService webHService) {
		this.webHService = webHService;
	}
	
	private WebH guardarCarrito(CarritoRequest carrito,EstadoMerlin estadoMerlin) throws ConvertirCarritoException {
		LOG.info("Voy a guardar el carrito en la BD. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
		WebH webH=toWebH(carrito);
		webH.setEstadoMerlin(estadoMerlin);
		
		if(this.isPosteriorCambioNroCarro(carrito.getCarrito().getFechaPedido().toGregorianCalendar().getTime())){
			webHService.guardar(webH);
			LOG.info("Carrito guardado exitosamente. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
		}else{
			throw new ConvertirCarritoException("Carrito Antiguo. El carrito "+carrito.getCarrito().getNumeroCarro()+" con fecha "+carrito.getCarrito().getFechaPedido());
		}
	
		return webH;
	}
	
	//por compatibilidad carritos posteriores al 29/3/2017
	//si el carrito es posterior al cambio nro carro int->string no impactar
	private Boolean isPosteriorCambioNroCarro(Date fechaPedido) {
		Calendar date = new GregorianCalendar(2017, Calendar.MARCH, 28);
		date.set(Calendar.HOUR, 23);
	    date.set(Calendar.MINUTE, 59);
	    date.set(Calendar.SECOND, 59);
	    date.set(Calendar.MILLISECOND, 0);
	    Date fechaLimite=date.getTime();
	    //fecha despues de 28/3/2017: 23:59:59
	    return fechaPedido.after(fechaLimite);
	}
	
	public WebH saveCarritoRequest(CarritoRequest carrito,EstadoMerlin estadoMerlin) throws Exception {
		if (carrito == null) {
			LOG.info("El carrito a guardar es nulo." + Calendar.getInstance());
			throw new SaveCarritoException("El carrito a guardar es nulo." + Calendar.getInstance());
		}
		try {
			return guardarCarrito(carrito,estadoMerlin);

		} catch (ConvertirCarritoException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("Error guardando carrito en la BD. NroCarrito: " + carrito.getCarrito().getNumeroCarro() + ". " + e.getClass().getName() + ": "
					+ e.getMessage());
			throw new SaveCarritoException("Error guardando carrito en la BD. NroCarrito: " + carrito.getCarrito().getNumeroCarro(), e);
		}

	}
	
	public CarritoRequest xmlToCarritoRequest(String xml) throws ConvertirCarritoException {
		LOG.info("Voy a convertir el XML de la cola a CarritoRequest y guardar en la bd");
		
		CarritoRequest carrito = null;
		try {
			InputStream input = new ByteArrayInputStream(xml.getBytes("UTF-8"));
			JAXBContext jb = JAXBContext.newInstance(CarritoRequest.class);
			Unmarshaller unmarshaller = jb.createUnmarshaller();
			carrito = (CarritoRequest) unmarshaller.unmarshal(input);
			LOG.info("Carrito convertido a objeto exitosamente. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error convirtiendo xml a CarritoRequest para guardar.  " + e.getMessage());
			throw new ConvertirCarritoException("Error convirtiendo xml a CarritoRequest para guardar.", e);
		}
		return carrito;
	}
	
	
	public String carritoRequestToXML(CarritoRequest carritoRequest) throws ConvertirCarritoException {

		String result=null;
		try {
			ByteArrayOutputStream salida=new ByteArrayOutputStream();
			JAXBContext jb = JAXBContext.newInstance(CarritoRequest.class);
			Marshaller marshaller = jb.createMarshaller();
			marshaller.marshal(carritoRequest,  salida);
			result=salida.toString("UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("Error convirtiendo carritoRequestToXML" + e.getMessage());
			throw new ConvertirCarritoException("Error convirtiendo carritoRequestToXML", e);
		}
		return result;
	}

	/**
	 * Validador del carrito. Se llama "save" porque para el que consume este
	 * servicio, el m�todo guarda el carrito. En realidad, lo valida y encola. y
	 * luego se guarda desde otro flujo que consume dicha cola. Si da un error,
	 * lo devuelve y tambien lo guarda en la bd documental.
	 */
	@Override
	public boolean saveCarrito(CarritoRequest carrito) throws ValidacionException, SaveCarritoException {
		if (carrito != null && carrito.getCarrito() != null && carrito.getCarrito().getNumeroCarro() != null) {
			try {
				LOG.info("Voy a validar el carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
				Set<ConstraintViolation<CarritoRequest>> errores = validar(carrito);

				if (carrito == null || (errores != null && errores.size() > 0)) {
					ArrayList<String> lista = prepararErrores(errores);
					LOG.info("El carrito no supero las validaciones iniciales. Errores:" + lista.toString()
							+ ". No entro al sistema interno de carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
					throw new ValidacionException("ValidationException: " + lista.toString());
				}
				LOG.info("Carrito validado. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
				return true;
			} catch (ValidacionException e) {
				throw e;
			} catch (Exception e) {
				LOG.error("Error recibiendo carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
				throw new ValidacionException("Error desconocido recibiendo carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro() + e.getMessage());
			}
		} else
			throw new ValidacionException("Carrito nulo o sin numero de carrito.");
	}
	
	/**
	 * Validador del carrito.
	 */
	@Override
	public boolean validarCarrito(CarritoRequest carrito) throws ValidacionException  {
		if (carrito != null && carrito.getCarrito() != null && carrito.getCarrito().getNumeroCarro() != null) {
			try {
				LOG.info("Voy a validar el carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
				Set<ConstraintViolation<CarritoRequest>> errores = validar(carrito);

				if (carrito == null || (errores != null && errores.size() > 0)) {
					ArrayList<String> lista = prepararErrores(errores);
					LOG.info("El carrito no supero las validaciones iniciales. Errores:" + lista.toString()
							+ ". No entro al sistema interno de carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
					throw new ValidacionException("ValidationException: " + lista.toString());
				}
				LOG.info("Carrito validado. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
				return true;
			} catch (ValidacionException e) {
				throw e;
			} catch (Exception e) {
				LOG.error("Error recibiendo carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
				throw new ValidacionException("Error desconocido recibiendo carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro() + e.getMessage());
			}
		} else
			throw new ValidacionException("Carrito nulo o sin numero de carrito.");
	}

	private ArrayList<String> prepararErrores(Set<ConstraintViolation<CarritoRequest>> errores) throws SaveCarritoException {
		try {
			ArrayList<String> lista = new ArrayList<String>();
			for (ConstraintViolation<CarritoRequest> error : errores) {
				String msj = "Atributo:" + error.getPropertyPath() + " - Error: " + error.getMessage();
				lista.add(msj);
			}
			return lista;
		} catch (Exception e) {
			LOG.error("Error en SaveCarrito/prepararErrores" + e.getMessage());
			throw new SaveCarritoException("Error en SaveCarrito/prepararErrrores.", e.getCause());
		}
	}

	private Set<ConstraintViolation<CarritoRequest>> validar(CarritoRequest carrito) throws ValidacionException {
		try {
			ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

			Validator validator = validatorFactory.getValidator();
			
			validarTotal(carrito);
			
			return validator.validate(carrito);

		} catch (Exception e) {
			LOG.error("Error en Validacion de datos del carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro() + e.getMessage());
			throw new ValidacionException("Error en Validacion de datos del carrito. NroCarrito: " + carrito.getCarrito().getNumeroCarro() +". "+ e.getMessage(), e.getCause());
		}
	}
	
	private void validarTotal(CarritoRequest carrito) throws ValidacionException
	{
	
		BigDecimal total = carrito.getCarrito().getImporteTotal();
		BigDecimal suma = new BigDecimal(0);
		
		for (CarritoRequest.Articulos.Articulo articulo : carrito.getArticulos().getArticulo()) 
		{
			suma = suma.add(articulo.getImporte());
		}
		
		if(suma.compareTo(total) != 0)
		{
			throw new ValidacionException("El monto total no corresponde a la suma de los totales de los Items.");
		}
		
	}
	
	
	private WebH toWebH(CarritoRequest carrito) throws ConvertirCarritoException {

		try {
			WebH resultWebH = new WebH();
			LOG.info("Voy a convertir el carrito a WebH. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
			String nroCarrito = "";
			//Si es un carro de evento especial
			if(carrito.getCarrito() != null){ 
				if(carrito.getCarrito().getNroEventoEspecial() !=null 
						&& !carrito.getCarrito().getNroEventoEspecial().isEmpty()
							&& carrito.getCarrito().getNroEventoEspecial().contains("EE") ){
					nroCarrito=Soporte.formatearNumeroCarroEE(carrito.getCarrito().getNumeroCarro().toString());
				}else{
					nroCarrito=Soporte.formatearNumeroCarro(carrito.getCarrito().getNumeroCarro().toString());
				}
			}else{
				LOG.error("Error convirtiendo el carrito a WebH para guardar. carrito.getCarrito() es null ");
				throw new ConvertirCarritoException("Error convirtiendo el carrito a WebH para guardar. carrito.getCarrito() es null");
			}
			resultWebH.setNroCarrito(nroCarrito);
			resultWebH.setProviderHybris(carrito.isHybris());
			resultWebH.setNroInternoCarrito(carrito.getCarrito().getNumerointerno());
			resultWebH.setEmpresa("");
			resultWebH.setFechaPedido(carrito.getCarrito().getFechaPedido().toGregorianCalendar().getTime());
			resultWebH.setHoraPedido(carrito.getCarrito().getFechaPedido().toGregorianCalendar().getTime());
			resultWebH.setCodigoMoneda(carrito.getCarrito().getMoneda());
			
			resultWebH.setTotal(carrito.getCarrito().getImporteTotal().doubleValue());
			resultWebH.setIdPedidoVenta(null);
			resultWebH.setPuntoNumeroFacturacion(null);
			resultWebH.setFechaFacturacion(null);
			resultWebH.setFechaImpacto(new Date());
			if(carrito.getCarrito().getVerazEstado()!=null)
				resultWebH.setVerazEstado(carrito.getCarrito().getVerazEstado().toString());
			if(carrito.getCarrito().getVerazScore()!=null)
				resultWebH.setVerazScore(carrito.getCarrito().getVerazScore().toString());
			resultWebH.setNroEventoEspecial(carrito.getCarrito().getNroEventoEspecial());
			
			if(carrito.getDatosNPS()!=null){
				WebNps webNps=new WebNps();
				webNps.setNroCarrito(resultWebH.getNroCarrito());
				webNps.setAltura(carrito.getDatosNPS().getNumeroCalle());
				webNps.setCalle(carrito.getDatosNPS().getCalle());
				webNps.setDocCod(carrito.getDatosNPS().getTipoDocumento());
				webNps.setNroDocumento(carrito.getDatosNPS().getNumeroDocumento());
				webNps.setFechaNacimiento((carrito.getDatosNPS().getFechaNacimiento()!=null)?carrito.getDatosNPS().getFechaNacimiento().toGregorianCalendar().getTime():null);
				webNps.setValidacionDireccion(carrito.getDatosNPS().getValidacionDireccion());
				webNps.setValidacionFechaNacimiento(carrito.getDatosNPS().getValidacionFechaNacimiento());
				webNps.setValidacionNumeroDocumento(carrito.getDatosNPS().getValidacionNumeroDocumento());
				webNps.setValidacionTipoDocumento(carrito.getDatosNPS().getValidacionTipoDocumento());
				resultWebH.setWebNps(webNps);
			}
			

			WebCli webCli = new WebCli();
			webCli.setNroCarrito(resultWebH.getNroCarrito());
			webCli.setDocCod(carrito.getCliente().getDatosCliente().getCodigoDocumento());
			webCli.setNroDocumento(carrito.getCliente().getDatosCliente().getNumeroDocumento().intValue());
			if(carrito.getCliente().getDatosCliente().getFechaNacimiento()!=null)
			{
			webCli.setFechaNacimiento(carrito.getCliente().getDatosCliente().getFechaNacimiento().toGregorianCalendar().getTime());
			}
			webCli.setSexo(carrito.getCliente().getDatosCliente().getSexo());
			webCli.setApellido(carrito.getCliente().getDatosCliente().getApellido());
			webCli.setNombre(carrito.getCliente().getDatosCliente().getNombre());
			
			String calle = carrito.getCliente().getDatosCliente().getDomicilio().getCalle();
			String altura = carrito.getCliente().getDatosCliente().getDomicilio().getNumero();
		
			webCli.setCalle(calleFormateada(altura, calle));
			webCli.setAltura(alturaFormateada(altura));

			webCli.setPiso(carrito.getCliente().getDatosCliente().getDomicilio().getPiso());
			webCli.setDepartamento(carrito.getCliente().getDatosCliente().getDomicilio().getDepartamento());
			webCli.setLocalidad(carrito.getCliente().getDatosCliente().getDomicilio().getLocalidad());
			webCli.setProvincia(carrito.getCliente().getDatosCliente().getDomicilio().getProvincia());
			webCli.setCodPostal(carrito.getCliente().getDatosCliente().getDomicilio().getCodigoPostal().toString());
			webCli.setCPA(carrito.getCliente().getDatosCliente().getDomicilio().getCpa());
			webCli.setMail(carrito.getCliente().getDatosCliente().getDireccionMail());
			webCli.setTelefono(carrito.getCliente().getDatosCliente().getTelefono());

			// envio factura
			String calleFac = carrito.getCliente().getDatosFacturacion().getCalle();
			String alturaFac = carrito.getCliente().getDatosFacturacion().getNumero();
			webCli.setCalleFac(calleFormateada(alturaFac, calleFac));
			webCli.setAlturaFac(alturaFormateada(alturaFac));
			
			webCli.setPisoFac(carrito.getCliente().getDatosFacturacion().getPiso());
			webCli.setDepartamentoFac(carrito.getCliente().getDatosFacturacion().getDepartamento());
			webCli.setLocalidadFac(carrito.getCliente().getDatosFacturacion().getLocalidad());
			webCli.setProvinciaFac(carrito.getCliente().getDatosFacturacion().getProvincia());
			webCli.setTelefonoFac(carrito.getCliente().getDatosFacturacion().getTelefono());
			webCli.setCodPostalFac(carrito.getCliente().getDatosFacturacion().getCodigoPostal().toString());
			webCli.setCPAFac(carrito.getCliente().getDatosFacturacion().getCpa());
			webCli.setDomicilioEnvioFraudulento(carrito.getCliente().getDatosEnvio().getFraude());
			webCli.setDomicilioFacturacionFraudulento(carrito.getCliente().getDatosFacturacion().getFraude());

			// envio del articulo
			String calleEnvio = carrito.getCliente().getDatosEnvio().getCalle();
			String alturaEnvio = carrito.getCliente().getDatosEnvio().getNumero();
			webCli.setCalleEnvio(calleFormateada(alturaEnvio, calleEnvio));
			webCli.setAlturaEnvio(alturaFormateada(alturaEnvio));
			
			if (carrito.getCliente().getDatosEnvio() != null & !carrito.getCliente().getDatosEnvio().getPiso().equals("0")) {
				webCli.setPisoEnvio(carrito.getCliente().getDatosEnvio().getPiso());
			} else {
				webCli.setPisoEnvio(null);
			}
			webCli.setDepartamentoEnvio(carrito.getCliente().getDatosEnvio().getDepartamento());
			webCli.setLocalidadEnvio(carrito.getCliente().getDatosEnvio().getLocalidad());
			webCli.setProvinciaEnvio(carrito.getCliente().getDatosEnvio().getProvincia());
			if (carrito.esRetiroEnTienda())
				webCli.setCodPostalEnvio("0000");
			else
				webCli.setCodPostalEnvio(carrito.getCliente().getDatosEnvio().getCodigoPostal().toString());
			webCli.setCPAEnvio(carrito.getCliente().getDatosEnvio().getCpa());
			//set datos GEO MCOM
			webCli.setLatitud(carrito.getCliente().getDatosEnvio().getLatitud());
			webCli.setLongitud(carrito.getCliente().getDatosEnvio().getLongitud());
			webCli.setMerlinNormalizado(merlinNormalizado(carrito.getCliente().getDatosEnvio().getMerlinnormalizado()));
			webCli.setMerlinId(carrito.getCliente().getDatosEnvio().getMerlinid());

			resultWebH.setWebCli(webCli);


			Short i = 1;
			Boolean existeAnyProductoCombo=false;
			resultWebH.setDetalles(new HashSet<WebD>());
			resultWebH.setVentaVerde(false);
			
			for (Articulo a : carrito.getArticulos().getArticulo()) {	
				//
				List<ProductoCombo> comboProductos = productoComboService.findAllByCodigoCombo(a.getCodigoCARSA().toString());
				if (!converter.esServicio(a.getCodigoEDSA().toString()) &&
						comboProductos != null && !comboProductos.isEmpty()) {
					existeAnyProductoCombo=true;
					for (ProductoCombo productoCombo : comboProductos) {
						WebD webD = new WebD();
						webD.setCodigoProductoCombo(a.getCodigoCARSA().toString());
						webD.setNroCarrito(resultWebH.getNroCarrito());
						webD.setNroItem(i);
						webD.setConcepto(a.getConceptoVenta());
						webD.setCodigoItemEdsa(a.getCodigoEDSA().toString());
						webD.setCodigoItemCarsa(Integer.parseInt(productoCombo.getCodigoProducto()));
						webD.setCodigoItemEmsa((a.getCodigoEMSA() == null) ? "" : a.getCodigoEMSA().toString());
						webD.setCantidad(a.getCantidad().intValue());
						webD.setPrecioVenta(productoCombo.getPrecioProducto());
						Double importeTotalLinea = this
								.setPrecision(a.getCantidad().intValue() * productoCombo.getPrecioProducto(), 2);
						webD.setImporteTotalLinea(importeTotalLinea);
						// ver en la bd el limite es de 20caracteres.
						webD.setDescripcion((productoCombo.getDescripcionProducto() == null) ? ""
								: ((productoCombo.getDescripcionProducto().length() > 20)
										? productoCombo.getDescripcionProducto().substring(0, 19)
										: productoCombo.getDescripcionProducto()));

						webD.setCodigoAlmacen(null);
						webD.setTipoEntrega(null);
						resultWebH.getDetalles().add(webD);

						webD.setCodigoRetiroCarsa(a.getCodigoRetiroCarsa());
						webD.setFechaEntregaComprometida((a.getFechaEntregaComprometida() != null)
								? a.getFechaEntregaComprometida().toGregorianCalendar().getTime() : null);
						// venta en verde
						if (!resultWebH.getVentaVerde() && webD.getCodigoItemCarsa() >= this.rangoProductoVentaVerde.get(0)
								&& webD.getCodigoItemCarsa() <= this.rangoProductoVentaVerde.get(1)) {
							resultWebH.setVentaVerde(true);
						}
						i++;
					}
				}
				else
				{
					WebD webD = new WebD();
					webD.setNroCarrito(resultWebH.getNroCarrito());
					webD.setNroItem(i);
					webD.setConcepto(a.getConceptoVenta());
					webD.setCodigoItemEdsa(a.getCodigoEDSA().toString());
					webD.setCodigoItemCarsa(a.getCodigoCARSA().intValue());
					webD.setCodigoItemEmsa((a.getCodigoEMSA() == null) ? "" : a.getCodigoEMSA().toString());
					webD.setCantidad(a.getCantidad().intValue());
					webD.setPrecioVenta(a.getPrecio().doubleValue());
					webD.setImporteTotalLinea(a.getImporte().doubleValue());
					// ver en la bd el limite es de 20caracteres.
					webD.setDescripcion((a.getDescripcion() == null) ? ""
							: ((a.getDescripcion().length() > 20) ? a.getDescripcion().substring(0, 19)
									: a.getDescripcion()));

					webD.setCodigoAlmacen(null);
					webD.setTipoEntrega(null);
					resultWebH.getDetalles().add(webD);

					webD.setCodigoRetiroCarsa(a.getCodigoRetiroCarsa());
					webD.setFechaEntregaComprometida((a.getFechaEntregaComprometida() != null)
							? a.getFechaEntregaComprometida().toGregorianCalendar().getTime() : null);
					// venta en verde
					if (!resultWebH.getVentaVerde() && webD.getCodigoItemCarsa() >= this.rangoProductoVentaVerde.get(0)
							&& webD.getCodigoItemCarsa() <= this.rangoProductoVentaVerde.get(1)) {
						resultWebH.setVentaVerde(true);
					}
					i++;
				}
			}
			
			if(existeAnyProductoCombo){
				this.subdividirBonificacionComboProducto(resultWebH.getDetalles());
			}

			i = 1;
			resultWebH.setPagos(new HashSet<WebP>());
			LocalDate fechaHoy = LocalDate.now();
			for (Pago p : carrito.getPagos().getPago()) {
				WebP webP = new WebP();
				webP.setEstado(p.getEstado().toString());
				webP.setNroCarrito(resultWebH.getNroCarrito());
				webP.setProducto(p.getProducto());
				//promocion AMEX bancarias y no bancarias
				if(p.getCodigo().equals("AE") && p.getMerchantID().equals("mu_carsa")){
					webP.setCodigoTarjeta("A5");	
				}//diferenciar Naranja de Naranja Plan Z
				else if(p.getCodigo().equals("TN") && p.getDescuento().intValue() == 11){
					webP.setCodigoTarjeta("TZ");
				}else if (p.getCodigo().equals("VD") && p.getNumeroTarjeta().startsWith("499859") && (fechaHoy.toString().equals("2021-08-07") || 
						fechaHoy.toString().equals("2021-08-08") || fechaHoy.toString().equals("2021-08-14")|| fechaHoy.toString().equals("2021-08-15"))){
					webP.setCodigoTarjeta("V8");
				}else{
						webP.setCodigoTarjeta(p.getCodigo());
					}
				webP.setCantCuotas(p.getCuotas().doubleValue());
				webP.setCodMoneda(p.getMoneda());
				webP.setImporteCupon(p.getImporteCupon().doubleValue());
				webP.setVoucherId(p.getVoucherID().toString());
				webP.setNroTarjeta(p.getNumeroTarjeta());
				String codigoAutorizacion=p.getCodigoAutorizacion();
				if(codigoAutorizacion!=null && !codigoAutorizacion.isEmpty()){
					try{
					Long numeroAutorizacion=Long.parseLong(codigoAutorizacion);
					}catch(Exception e){
						LOG.error("Error al ingresar tarjeta codigoAutorizacion, Carrito: "+resultWebH.getNroCarrito());
						LOG.info("remplazo codigoAutorizacion: "+codigoAutorizacion+", por :"+codigoAutorizacion.replaceAll("[^a-zA-Z]", ""));
						codigoAutorizacion=codigoAutorizacion.replaceAll("[^0-9]", "");
					}
					
				}
				webP.setCodAutorizacion(codigoAutorizacion);
				webP.setMerchantId(p.getMerchantID());
				webP.setNroLote((p.getLote().intValue() == 0) ? "" : p.getLote().toString());
				//webP.setNpsstatus(p.getNpsstatus());
				// ver
				webP.setPromocion(p.getDescripcion());
				webP.setDescuentoProm(p.getDescuento().toString());
				
				//datos de la tarjeta
				webP.setApellidoTitular(p.getApellidoTitular());
				webP.setNombreTitular(p.getNombreTitular());
				webP.setNumeroDocumentoTitular(p.getNumeroDocumentoTitular());
				webP.setTipoDocumentoTitular(p.getTipoDocumentoTitular());
				resultWebH.getPagos().add(webP);
			}
			
			//establece el estado en Pendiente o Pendiente_Automatico_OrdenCRM
			this.establecerEstado(resultWebH);
			
			LOG.info("Converti el carrito a WebH. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
			return resultWebH;
		} catch (Exception e) {
			LOG.error("Error convirtiendo el carrito a WebH para guardar. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
			throw new ConvertirCarritoException("Error convirtiendo el carrito a WebH para guardar. NroCarrito: " + carrito.getCarrito().getNumeroCarro());
		}
	}
	
	
	/*
	 * Si el monto de la bonificacion es mayor al del item
	 * se genera otra nueva bonificacion con el siguiente item del combo producto
	 */
	private void subdividirBonificacionComboProducto(Set<WebD> detalles) throws CloneNotSupportedException {
		List<WebD> bonificacionesAdd=new ArrayList<>();
		for (WebD webd : detalles) {
			if (CODIGO_BONIFICACION.equals(webd.getCodigoItemEdsa().toString())) {
				List<WebD> comboProductos = this.findComboProducto(detalles,webd.getCodigoItemCarsa());
				if (comboProductos != null && !comboProductos.isEmpty()) {
					Double importeTotalABonificar=Math.abs(webd.getImporteTotalLinea());
					int i=0;
					do {
						WebD itemCombo=comboProductos.get(i);
						if(importeTotalABonificar < itemCombo.getImporteTotalLinea()){
							webd.setImporteTotalLinea(importeTotalABonificar*-1);
							webd.setCodigoItemCarsa(itemCombo.getCodigoItemCarsa());
							importeTotalABonificar=0D;
						}else{
							WebD bonificacion=webd.clone();
							bonificacion.setImporteTotalLinea(itemCombo.getImporteTotalLinea()*-1);
							bonificacion.setCodigoItemCarsa(itemCombo.getCodigoItemCarsa());
							importeTotalABonificar=importeTotalABonificar-itemCombo.getImporteTotalLinea();
							bonificacion.setNroItem((short) (detalles.size()+1));
							bonificacionesAdd.add(bonificacion);
						}
					    i++;
					} while (importeTotalABonificar>0 && i< comboProductos.size());
				}
			}
		}
		if(!bonificacionesAdd.isEmpty()){
			detalles.addAll(bonificacionesAdd);
		}
	}


	private List<WebD> findComboProducto(Set<WebD> detalles,Integer codigoComboProducto) {
		List<WebD> comboProductos=new ArrayList<WebD>();
		for(WebD webd:detalles){
			if(webd.getCodigoProductoCombo()!=null && webd.getCodigoProductoCombo().equals(codigoComboProducto.toString())){
				comboProductos.add(webd);
			}
		}
		//ordeno la lista por mayor importe
		if (comboProductos != null && !comboProductos.isEmpty()) {
		Collections.sort(comboProductos, new Comparator<WebD>() {
			@Override
			public int compare(WebD w1, WebD w2) {
				return w2.getImporteTotalLinea().compareTo(w1.getImporteTotalLinea());
			}
		});
		}
		return comboProductos;
	}

	private Double setPrecision(double valor,Integer decimal) {
		return BigDecimal.valueOf(valor)
	    .setScale(decimal, RoundingMode.HALF_UP)
	    .doubleValue();
	}
	
	private void establecerEstado(WebH resultWebH) {
		
		if( habilitarFacturacionAutomatica 
				&& resultWebH.getWebCli()!=null 
				&& (QA.equals(enviroment)
					|| (Integer.valueOf(0).equals(resultWebH.getWebCli().getDomicilioEnvioFraudulento()) 
					&& Integer.valueOf(0).equals(resultWebH.getWebCli().getDomicilioFacturacionFraudulento())
					))
				&& resultWebH.esRetiroEnTienda() 
				&& !this.esRetiroEnTiendaOca(resultWebH)
				&& (scoreMayorIgual==null ||  getScoreCarritoWeb(resultWebH) >= this.scoreMayorIgual)
				&& resultWebH.pagosAprobados())
		{			
			if(esConsignacion(resultWebH)){
				resultWebH.setEstado(WebH.EN_REVISION);
			}else{
				resultWebH.setEstado(WebH.ESTADO_PENDIENTE_ORDEN_CRM);
			}
			
	    }else
	    {
	    	//Si es consignacion tengo que ponerlo en EN_REVISION
	    	if (esConsignacion(resultWebH)){
	    		resultWebH.setEstado(WebH.EN_REVISION);
	    	}else{
		    	//si no dejo en pendiente
		    	resultWebH.setEstado(WebH.ESTADO_PENDIENTE);
	    	}
		}
	}
	
	private Boolean esRetiroEnTiendaOca( WebH webH){
		Iterator<WebD> iter = webH.getDetalles().iterator();
		while (iter.hasNext()) {	
			WebD webD=iter.next();
			if (webD.getCodigoRetiroCarsa() != null && !webD.getCodigoRetiroCarsa().trim().isEmpty())
			{
				try{
				int codigoRetiro=Integer.valueOf(webD.getCodigoRetiroCarsa());
				if(codigoRetiro>10000)
				{
					return true;
				}else{
					return false;
				}
				}catch(NumberFormatException exception){
					return false;
				}
			}
		}
		return false;
	}
	
	private Integer getScoreCarritoWeb(WebH resultWebH){
		if(resultWebH.getVerazScore()!=null){
			try {
				return Integer.parseInt(resultWebH.getVerazScore().replaceFirst("%", ""));
			} catch (NumberFormatException e) {
				return 0;
			}
		}
		return 0;
	}

	private MerlinNormalizadoMcom merlinNormalizado(String merlinnormalizado) {
		if (merlinnormalizado != null) {
			if ("SI".equals(merlinnormalizado.toUpperCase())) {
				return MerlinNormalizadoMcom.SI;
			}
			if ("NO".equals(merlinnormalizado.toUpperCase())) {
				return MerlinNormalizadoMcom.NO;
			}
			if ("CONTINGENCIA".equals(merlinnormalizado.toUpperCase())) {
				return MerlinNormalizadoMcom.CONTINGENCIA;
			}
		}
		return null;
	}
	
	public Boolean isPendienteOrdenCrm(String nroCarrito) {
		return this.webHService.isPendienteOrdenCrm(nroCarrito);
	}
	
	public Boolean actualizarEstadoCarritoPendiente(String nroCarrito) {
		return this.webHService.actualizarEstadoCarrito(nroCarrito,WebH.ESTADO_PENDIENTE);
	}
	
	public Boolean actualizarEstadoCarritoPendienteAutomatico(String nroCarrito,String nroOrden) {
		return this.webHService.actualizarCarritoRt(nroCarrito,nroOrden,WebH.ESTADO_PENDIENTE_AUTOMATICO);
	}
	public Boolean actualizarEstadoCarritoPendienteOrdenCrm(String nroCarrito) {
		return this.webHService.actualizarEstadoCarrito(nroCarrito,WebH.ESTADO_PENDIENTE_ORDEN_CRM);
	}
	
	public Boolean actualizarEstadoClienteError(String nroCarrito) {
		return this.webHService.actualizarEstadoCarrito(nroCarrito,WebH.ESTADO_ERROR_CLIENTE);
	}
	
	public Boolean actualizarNroOrdenCrmCarrito(String nroCarritoS, String nroFactura, String nroOrdenCrm) {
		String carrito;
		
		if(!nroCarritoS.isEmpty())
			carrito=nroCarritoS;
		else
		{
			Integer punto = Integer.valueOf( nroFactura.substring(0, 4));
			Integer cdg = Integer.valueOf(nroFactura.substring(6));
			
			carrito = this.webHService.findCarritoByNroFactura(punto.toString()+"-"+cdg.toString()).getNroCarrito();
		}
		
		if(carrito!=null)
			return this.webHService.actualizarNroOrdenCrmCarrito(carrito,nroOrdenCrm);
		else 
			return false;
	}
	
	public String obtenerNroOrdenCrmCarrito(String nroFactura) throws BadRequestException {
		
			WebH carrito = this.webHService.findCarritoByNroFactura(nroFactura);
			
			if(carrito!=null && carrito.getNroOrdenCrm()!=null)
				return carrito.getNroOrdenCrm();
			else
				throw new BadRequestException("No se encontro carrito asociado a la factura: " +nroFactura);
		
	}
	
	public ArrepentidoDTO obtenerArrepentidoDTO(ArrepentidoRequest request) throws BadRequestException{
		//Saco los datos que se pidieron para enviar un mail y para enviar a la novedad a sap
		String nroOrden = "";
		ArrepentidoDTO dto = new ArrepentidoDTO();
		
		if(request !=null){
			if(request.getWebnr() != null && !request.getWebnr().isEmpty()){
				nroOrden = request.getWebnr();
				if(actualizarEstadoArrepentimientoCompra(nroOrden))
					LOG.info("Carrito: " + nroOrden + " Actualizado a ArrepentimientoCompra");
				else
					LOG.error("Carrito: " + nroOrden + " Error Actualizando a ArrepentimientoCompra");
			}else{
				if(request.getWebpto()!= null && !request.getWebpto().isEmpty())
					nroOrden = formatoFactura(request);
			}
		}else{
			throw new BadRequestException("No se encontro carrito asociado a la factura: " +request);
		}
		
		dto.setDni(request.getWebndo());
		dto.setEmail(request.getWebema());
		dto.setCodigoMotivo(request.getWebcom());
		dto.setMotivo(request.getWebmot());
		dto.setNroOrden(nroOrden);
		dto.setObservaciones("");
		return dto;
	}
	
	public Boolean actualizarEstadoArrepentimientoCompra(String nroCarrito) {
		WebH carro = this.webHService.getByNroCarrito(nroCarrito);
		if(carro != null){
			if(carro.getEstado().equals(WebH.ESTADO_PENDIENTE)
					||carro.getEstado().equals(WebH.ESTADO_PENDIENTE_ORDEN_CRM)
					||carro.getEstado().equals(WebH.EN_REVISION)
					||carro.getEstado().equals(WebH.ESTADO_ERROR)
					||carro.getEstado().equals(WebH.ESTADO_ERROR_CLIENTE)
					|| carro.getEstado().equals(WebH.EN_REVISION))
				return this.webHService.actualizarEstadoCarrito(nroCarrito,WebH.ESTADO_ARREPENTIMIENTO);
		}
		return false;
	}
	
	public Integer getPuntoVentaAutomatico() {
		return puntoVentaAutomatico;
	}

	public void setPuntoVentaAutomatico(Integer puntoVentaAutomatico) {
		this.puntoVentaAutomatico = puntoVentaAutomatico;
	}

	public FiltroFacturadorAutomatico crearFiltroAutomatico(String nroCarritoS){

		FiltroFacturadorAutomatico filtroFacturadorAutomatico=null;
		if(nroCarritoS!=null && !nroCarritoS.isEmpty()){
		 filtroFacturadorAutomatico=new FiltroFacturadorAutomatico();
		 filtroFacturadorAutomatico.setNroCarrito(nroCarritoS);		
		 filtroFacturadorAutomatico.setPuntoVenta(puntoVentaAutomatico);
		 filtroFacturadorAutomatico.setIdPedidoVenta(null);
		}
		return filtroFacturadorAutomatico;
	}
	
	public Boolean carritoEnEstado(String nroCarrito,String estado){
		WebH carrito= this.webHService.getByNroCarrito(nroCarrito);
		if(carrito!=null){
			return estado.equals(carrito.getEstado());
		}
		else
			return false;
	}
	
	private static boolean isNumeric(String cadena){
		if (cadena.matches("\\d*")){
			return true;
		}
		else{
			return false;
		}	
	}
	
	/** Para los caso en que la altura es alfanumerica (Ej: KM 15), se setea en vacio, y se 
	 * concatena a la calle
	 * @param altura
	 * @return
	 */
	private String alturaFormateada(String altura){
		
		if((altura != null) && isNumeric(altura)){
			return altura;
		}
		return "0";
	}
	
	/** Para los caso en que la altura es alfanumerica (Ej: KM 15), se setea en vacio, y se 
	 * concatena a la calle
	 * @param altura
	 * @return
	 */
	private String calleFormateada(String altura, String calle){
		if((altura != null) && isNumeric(altura)){
			return calle;
		}
		return calle + " " + altura;
	}
	
	
	public OportunidadCarro getOportunidadRequest(WebH webh)
	{
		OportunidadCarro result=new OportunidadCarro();
		result.setFechaDate(webh.getFechaPedido());
		result.setNumeroCarro(webh.getNroCarrito());
		result.setOperacion(OportunidadCarro.OPERACION_MENOS);
		for(WebD webd:webh.getDetalles()){
			if(webd.getCodigoItemCarsa()!=null
					&& webd.getCodigoItemCarsa()>0 
					&& !converter.esServicio(webd.getCodigoItemEdsa().toString())
					&& webd.getCantidad()>0)
			{
			DetalleItems detalleItem=new DetalleItems();
			detalleItem.setArticulo(webd.getCodigoItemCarsa());
			detalleItem.setCantidad(webd.getCantidad());
			result.getDetalleItems().add(detalleItem);
			}
		}		
		return result;
	}
	
	public WebH updateOportunidad(WebH webh,OportunidadCarro oportunidadCarro)
	{
		for(DetalleItems detalleItem:oportunidadCarro.getDetalleItems()){
			for(WebD webd:webh.getDetalles()){
				if(webd.getCodigoItemCarsa().equals(detalleItem.getArticulo())
						&& webd.getCantidad().equals(detalleItem.getCantidad())
					&& !converter.esServicio(webd.getCodigoItemEdsa().toString()))
				{
						webd.setEsOportunidad(detalleItem.getEsOpoWeb());
						webd.setPrecioVentaInterno(detalleItem.getPrecioVta());
						webd.setPrecioCostoInterno(detalleItem.getCosto());
						webd.setFechaConsultaPrecio(detalleItem.getTimestampDate());					
				}
			}
		}
		return this.webHService.updateCarro(webh);
	}

	public void setEnviroment(String enviroment) {
		this.enviroment = enviroment;
	}

	public void setConverter(Converter converter) {
		this.converter = converter;
	}
	
	private Boolean esConsignacion( WebH webH){
		Iterator<WebD> iter = webH.getDetalles().iterator();
		while (iter.hasNext()) {	
			WebD webD=iter.next();
			if (webD.getCodigoItemCarsa() != null && webD.getCodigoItemCarsa()>0)
			{
				try{
				int codigoProducto=Integer.valueOf(webD.getCodigoItemCarsa());
				if(codigoProducto>=this.rangoProductoVentaConsignacion.get(0) 
						&& codigoProducto<=this.rangoProductoVentaConsignacion.get(1))
				{
					return true;
				}else{
					return false;
				}
				}catch(NumberFormatException exception){
					return false;
				}
			}
		}
		return false;
	}
	
	private String formatoFactura(ArrepentidoRequest obj){
		Formatter fmtNroFac = new Formatter();
		Formatter fmtPtoFac = new Formatter();
		Long nroFac = Long.parseLong(obj.getWebnro());
		Long ptoFac = Long.parseLong(obj.getWebpto());
		fmtNroFac.format("%08d", nroFac);
		fmtPtoFac.format("%04d", ptoFac);
		if(obj.getWebtfa().equals("A"))
			return fmtPtoFac.toString()+"1"+fmtNroFac.toString();
		else
			return fmtPtoFac.toString()+"6"+fmtNroFac.toString();
	} 
}
