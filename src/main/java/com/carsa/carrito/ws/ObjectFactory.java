//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantaci�n de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perder�n si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.16 a las 10:55:15 AM ART 
//


package com.carsa.carrito.ws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.carsa package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.carsa
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CarritoRequest }
     * 
     */
    public CarritoRequest createCarritoRequest() {
        return new CarritoRequest();
    }

    /**
     * Create an instance of {@link CarritoRequest.Pagos }
     * 
     */
    public CarritoRequest.Pagos createCarritoRequestPagos() {
        return new CarritoRequest.Pagos();
    }

    /**
     * Create an instance of {@link CarritoRequest.Articulos }
     * 
     */
    public CarritoRequest.Articulos createCarritoRequestArticulos() {
        return new CarritoRequest.Articulos();
    }

    /**
     * Create an instance of {@link CarritoRequest.Cliente }
     * 
     */
    public CarritoRequest.Cliente createCarritoRequestCliente() {
        return new CarritoRequest.Cliente();
    }

    /**
     * Create an instance of {@link CarritoRequest.Cliente.DatosCliente }
     * 
     */
    public CarritoRequest.Cliente.DatosCliente createCarritoRequestClienteDatosCliente() {
        return new CarritoRequest.Cliente.DatosCliente();
    }

    /**
     * Create an instance of {@link CarritoRequest.Carrito }
     * 
     */
    public CarritoRequest.Carrito createCarritoRequestCarrito() {
        return new CarritoRequest.Carrito();
    }

    /**
     * Create an instance of {@link CarritoRequest.Pagos.Pago }
     * 
     */
    public CarritoRequest.Pagos.Pago createCarritoRequestPagosPago() {
        return new CarritoRequest.Pagos.Pago();
    }

    /**
     * Create an instance of {@link CarritoRequest.Articulos.Articulo }
     * 
     */
    public CarritoRequest.Articulos.Articulo createCarritoRequestArticulosArticulo() {
        return new CarritoRequest.Articulos.Articulo();
    }

    /**
     * Create an instance of {@link CarritoRequest.Cliente.DatosFacturacion }
     * 
     */
    public CarritoRequest.Cliente.DatosFacturacion createCarritoRequestClienteDatosFacturacion() {
        return new CarritoRequest.Cliente.DatosFacturacion();
    }

    /**
     * Create an instance of {@link CarritoRequest.Cliente.DatosEnvio }
     * 
     */
    public CarritoRequest.Cliente.DatosEnvio createCarritoRequestClienteDatosEnvio() {
        return new CarritoRequest.Cliente.DatosEnvio();
    }

    /**
     * Create an instance of {@link CarritoRequest.Cliente.DatosCliente.Domicilio }
     * 
     */
    public CarritoRequest.Cliente.DatosCliente.Domicilio createCarritoRequestClienteDatosClienteDomicilio() {
        return new CarritoRequest.Cliente.DatosCliente.Domicilio();
    }

}
