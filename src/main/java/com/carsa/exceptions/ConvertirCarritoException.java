package com.carsa.exceptions;

public class ConvertirCarritoException extends Exception implements CarritoException {

	private static final long serialVersionUID = 7310377757242418698L;

	public ConvertirCarritoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConvertirCarritoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}