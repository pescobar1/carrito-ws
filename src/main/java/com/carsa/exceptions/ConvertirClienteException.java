package com.carsa.exceptions;

public class ConvertirClienteException extends Exception implements CarritoException {

	private static final long serialVersionUID = 7310377757242418698L;

	public ConvertirClienteException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ConvertirClienteException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}