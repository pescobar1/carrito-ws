package com.carsa.exceptions;

public class ExceptionHandler {

	/**
	 * Extrae el mensaje de error m�s interno.
	 * Sirve para poder 'deswrappear' la excepcion propia lanzada y luego capturada en la Exception Choice Strategy
	 * @param e excepcion recibida
	 * @return String mensaje de error
	 */
	public static String obtenerMensajeError(Exception e){
		Throwable ee=e;
		while (ee.getCause()!=null){
			ee=ee.getCause();
		}
		return new StringBuilder(ee.getClass().getName()).append(": ").append(ee.getMessage()).toString();
	}
	
	public static Throwable obtenerError(Exception e){
		Throwable ee=e;
		while (ee.getCause()!=null){
			ee=ee.getCause();
		}
		return ee;
	}
	
}
