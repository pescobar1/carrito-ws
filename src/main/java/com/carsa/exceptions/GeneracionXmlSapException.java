package com.carsa.exceptions;

public class GeneracionXmlSapException extends Exception implements CarritoException {

	private static final long serialVersionUID = 7310377757242418698L;

	public GeneracionXmlSapException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GeneracionXmlSapException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}