package com.carsa.exceptions;

public class SaveCarritoException extends Exception implements CarritoException{

	private static final long serialVersionUID = 6278497217296713529L;

	public SaveCarritoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SaveCarritoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	

}
