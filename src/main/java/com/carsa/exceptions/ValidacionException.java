package com.carsa.exceptions;

public class ValidacionException extends Exception implements CarritoException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4175414705259322380L;
	
	public ValidacionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ValidacionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
}
